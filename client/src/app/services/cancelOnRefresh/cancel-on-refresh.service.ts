import {isArray, remove} from "lodash";
import { Injectable } from '@angular/core';
import {Task} from "../../models/task";


export interface TaskToCancelOnRefresh {
  taskType: string;
  selectorTaskId: string;
  projectName: string;
  objectId: number;
}

@Injectable({
  providedIn: 'root'
})
export class CancelOnRefreshService {

  constructor() { }

  dontCancelOnRefresh(task: Task) {
    const unparsedTasksToCancelOnRefresh = window.localStorage.getItem(`pending${task.taskType}`),
        tasksToCancelOnRefresh: TaskToCancelOnRefresh[] = unparsedTasksToCancelOnRefresh ?
            JSON.parse(unparsedTasksToCancelOnRefresh) : [];

    if (!isArray(tasksToCancelOnRefresh)) {
      return;
    }

    remove(tasksToCancelOnRefresh, (taskToCancel) => {
      return taskToCancel.projectName === task.projectName &&
          taskToCancel.selectorTaskId === task.selectorTaskId &&
          taskToCancel.taskType === task.taskType;
    });

    window.localStorage.setItem(`pending${task.taskType}`, JSON.stringify(tasksToCancelOnRefresh));
  }

  setTaskToCancelOnRefresh(task: Task) {
    const unparsedTasksToCancelOnRefresh = window.localStorage.getItem(`pending${task.taskType}`),
        tasksToCancelOnRefresh: TaskToCancelOnRefresh[] = unparsedTasksToCancelOnRefresh ?
            JSON.parse(unparsedTasksToCancelOnRefresh) : [];

    if (!isArray(tasksToCancelOnRefresh)) {
      return;
    }

    tasksToCancelOnRefresh.push({
      taskType: task.taskType,
      selectorTaskId: task.selectorTaskId,
      projectName: task.projectName,
      objectId: task.objectId
    });

    window.localStorage.setItem(`pending${task.taskType}`, JSON.stringify(tasksToCancelOnRefresh));
  }
}

