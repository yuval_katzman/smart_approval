import {fakeAsync, TestBed, tick} from '@angular/core/testing';

import { ProjectService } from './project.service';
import {HttpClientModule} from "@angular/common/http";
import {IndexedDbService} from "../indexedDb/indexed-db-service.service";
import {API} from "../API/api.service";
import {Observable} from "rxjs";

describe('ProjectService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule]
  }));

  it('should be created', () => {
    const service: ProjectService = TestBed.get(ProjectService);
    expect(service).toBeTruthy();
  });

  it('get project from indexedDb', fakeAsync(() => {
    const service: ProjectService = TestBed.get(ProjectService);
    const indexedDbService = TestBed.get(IndexedDbService);

    spyOn(indexedDbService, 'checkIfDBExists').and.returnValue(new Promise(resolve => resolve(true)));
    spyOn(indexedDbService, 'read').and.returnValue(new Promise(resolve => resolve({
      project_name: 'test'
    })));

    service.getProjectByName('test').subscribe((res) => {
      expect(res.projectName).toBe('test');
    });
  }));

  it('get project from server', fakeAsync(() => {
    const service: ProjectService = TestBed.get(ProjectService);
    const indexedDbService = TestBed.get(IndexedDbService);
    const apiService = TestBed.get(API);

    spyOn(indexedDbService, 'checkIfDBExists').and.returnValue(new Promise(resolve => resolve(false)));
    spyOn(apiService, 'get').and.returnValue(new Observable(observer => observer.next({project_name: 'test'})));
    spyOn(indexedDbService, 'createDB').and.returnValue(new Promise(resolve => resolve()));
    const addToIndexedDbSpy = spyOn(indexedDbService, 'add').and.returnValue(new Promise(resolve => resolve()));

    service.getProjectByName('test').subscribe((res) => {
      expect(res.projectName).toBe('test');
    });

    tick();

    expect(addToIndexedDbSpy).toHaveBeenCalledWith('projects', 'test', {
      project_name: 'test'
    });
  }));
});
