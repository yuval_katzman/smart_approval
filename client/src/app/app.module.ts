import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { ValidateComponent } from './components/validate/validate.component';
import {CookieService} from "ngx-cookie-service";
import { HeaderTitleComponent } from './components/header-title/header-title.component';
import {MatDialogModule} from "@angular/material";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { LoadingScreenComponent } from './components/loading-screen/loading-screen.component';
import {UnauthenticatedInterceptorService} from "./services/unauthenticatedInterceptor/unauthenticated-interceptor.service";
import { AuthInterceptorService } from "./services/authInterceptor/auth-interceptor.service";
import { BetweenItemsComponent } from './components/between-items/between-items.component';
import {MegazordCanvasDirective} from "./components/megazordCanvas/megazord-canvas.directive";
import {FormsModule} from "@angular/forms";
import {DragDropModule} from "@angular/cdk/drag-drop";
import { RelativeStopwatchComponent } from './components/relative-stopwatch/relative-stopwatch.component';
import { CancelTaskDialogComponent } from './components/cancel-task-dialog/cancel-task-dialog.component';
import { InactivityTimerComponent } from './components/inactivity-timer/inactivity-timer.component';
import {
    TransitionGroupComponent,
    TransitionGroupItemDirective
} from './components/transition-group/transition-group.component';
import { LightboxModule } from 'ngx-lightbox';
import { YesNoCantTellComponent } from './components/validate/yes-no-cant-tell/yes-no-cant-tell.component';
import {MatRadioModule} from "@angular/material/radio";
import {MatInputModule} from "@angular/material/input";
import {HotkeyDirective} from "./components/validate/yes-no-cant-tell/hotkey.directive";
import { YesNoComponent } from './components/validate/yes-no/yes-no.component';


@NgModule({
    declarations: [
        AppComponent,
        ValidateComponent,
        HeaderTitleComponent,
        LoadingScreenComponent,
        BetweenItemsComponent,
        MegazordCanvasDirective,
        RelativeStopwatchComponent,
        CancelTaskDialogComponent,
        InactivityTimerComponent,
        TransitionGroupComponent,
        TransitionGroupItemDirective,
        YesNoCantTellComponent,
        HotkeyDirective,
        YesNoComponent,

    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        BrowserAnimationsModule,
        MatDialogModule,
        FormsModule,
        DragDropModule,
        LightboxModule,
        MatRadioModule,
        MatInputModule
    ],
    entryComponents: [LoadingScreenComponent, BetweenItemsComponent, CancelTaskDialogComponent, InactivityTimerComponent],
    providers: [
        CookieService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: UnauthenticatedInterceptorService,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptorService,
            multi: true
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
