import {forEach, isArray} from "lodash";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {ValidateManagerService} from "../validate-manager/validate-manager.service";
import {TaskToCancelOnRefresh} from "./cancel-on-refresh.service";

@Injectable({
    providedIn: 'root'
})
export class CancelOnRefreshCancellerService {

    private cancelFunctions: {
        [taskType: string]: (projectName: string, selectorTaskId: string, objectId: number, reason: string) => Observable<any>
    } = {};

    constructor(private validateManager: ValidateManagerService) {
        this.cancelFunctions.oos_approval = this.validateManager.cancelTask.bind(this.validateManager);
    }

    cancelTasks() {
        forEach(this.cancelFunctions, (cancelFn, taskType) => {
            const unparsedTasksToCancelOnRefresh = window.localStorage.getItem(`pending${taskType}`),
                tasksToCancelOnRefresh: TaskToCancelOnRefresh[] = unparsedTasksToCancelOnRefresh ?
                    JSON.parse(unparsedTasksToCancelOnRefresh) : [];

            if (!isArray(tasksToCancelOnRefresh)) {
                return;
            }

            forEach(tasksToCancelOnRefresh, (task) => {
                cancelFn(task.projectName, task.selectorTaskId, task.objectId, 'user refreshed the page').subscribe(() => {});
            });

            window.localStorage.setItem(`pending${taskType}`, JSON.stringify([]));
        });
    }
}

