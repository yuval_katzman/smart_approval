SET FOREIGN_KEY_CHECKS = 0;


TRUNCATE TABLE selector.project;
INSERT INTO selector.project (pk, name) VALUES (1, 'dev');

TRUNCATE TABLE selector.queue;
INSERT INTO selector.queue (pk, name, task_type_fk, visibility_timeout, project_fk) VALUES
(1, '{"taskType":"oos_approval","projectName":"dev"}', 19, 900000, 1);

INSERT INTO selector.queue (pk, name, task_type_fk, visibility_timeout, project_fk) VALUES
(2, '{"taskType":"approval_voting","projectName":"dev"}', 23, 900000, 1);

INSERT INTO selector.queue (pk, name, task_type_fk, visibility_timeout, project_fk) VALUES
(3, '{"taskType":"approvals","projectName":"dev","subQueue":"subQueueName"}', 25, 900000, 1);

INSERT INTO selector.queue (pk, name, task_type_fk, visibility_timeout, project_fk) VALUES
(4, '{"taskType":"approvals_voting","projectName":"dev","subQueue":"subQueueName"}', 26, 900000, 1);


TRUNCATE TABLE selector.task_pool;
INSERT INTO  selector.task_pool (uuid, object_id, queue_fk, project_fk, wave_type, wave_uid, insert_date, pull_date,
locked_until, locked_by, is_dirty, valid_until_date, restricted_to_user, number_of_cycles, number_of_tries,
is_ack_needed, additional_info) VALUES
(UUID(), '1', '1', '1', 'waveType', 'waveUid',
'2019-09-01 13:12:37.988', NULL, NULL, NULL, '0', NULL, NULL, '1', '0', '0',
'{
  "approval_id": 1,
  "job_type": "WatchRepair",
  "entity_id": 2187121,
  "entity_type": "probe",
  "wave_uid": "waveUid",
  "scene_uid": "sceneUid",
  "wave_type": "waveType",
  "session_uid": "sessionUid",
  "ttl_in_minutes": 60,
  "questions": [
    {
      "location": {
        "probes": [{
          "pk": 2187121,
          "masks": [
            {
              "h": 400,
              "w": 250,
              "x": 890,
              "y": 1000
            }
          ]
        }],
        "scene": {
          "productId": 1,
          "masks": [
            {
              "h": 200,
              "w": 600,
              "x": 1500,
              "y": 1150
            }
          ]
        }
      },
      "product_fk": 1,
      "product_dvc_fk": 1,
      "question_category": "OOSApproval",
      "question": "Can you find Coco pops on the shelf ?",
      "question_uid": "questionUid",
      "question_type": "YesNo",
      "lastSeenSceneId":421205
    }
  ],
  "project_name": "dev"
}'
),
(UUID(), '2', '1', '1', 'waveType', 'waveUid',
'2019-09-01 13:12:37.988', NULL, NULL, NULL, '0', NULL, NULL, '1', '0', '0',
'{
  "approval_id": 1,
  "job_type": "approvals",
  "scene_id": 1,
  "wave_uid": "waveUid",
  "scene_uid": "sceneUid",
  "wave_type": "waveType",
  "session_uid": "sessionUid",
  "ttl_in_minutes": 60,
  "questions": [
    {
      "location": {
        "probes": [{
          "pk": 2187121,
          "masks": [
            {
              "h": 400,
              "w": 250,
              "x": 890,
              "y": 1000
            }
          ]
        }],
        "scene": {
          "productId": 1,
          "masks": [
            {
              "h": 200,
              "w": 600,
              "x": 1500,
              "y": 1150
            }
          ]
        }
      },
      "product_fk": 1,
      "product_dvc_fk": 1,
      "question_category": "OOSApproval",
      "question": "Can you find Coco pops on the shelf ?",
      "question_uid": "questionUid",
      "question_type": "YesNo",
      "lastSeenSceneId":421205
    },
    {
      "location": {
        "probes": [{
          "pk": 2187121,
          "masks": [
            {
              "h": 400,
              "w": 280,
              "x": 900,
              "y": 1450
            }
          ]
        },{
          "pk": 2187124,
          "masks": [
            {
              "h": 400,
              "w": 280,
              "x": 1150,
              "y": 100
            }
          ]
        }],
        "scene": {
          "productId": 1,
          "masks": [
            {
              "h": 350,
              "w": 300,
              "x": 1550,
              "y": 1600
            }
          ]
        }
      },
      "product_fk": 2,
      "question_category": "OOSApproval",
      "question": "Is Kariot in the scene?",
      "question_uid": "questionUid",
      "question_type": "YesNo",
      "lastSeenSceneId":421205
    },
    {
      "location": {
        "probes": [{
          "pk": 2187121,
          "masks": [
            {
              "h": 400,
              "w": 280,
              "x": 900,
              "y": 1450
            }
          ]
        },{
          "pk": 2187124,
          "masks": [
            {
              "h": 400,
              "w": 280,
              "x": 1150,
              "y": 100
            }
          ]
        }],
        "scene": {
          "productId": 1,
          "masks": [
            {
              "h": 350,
              "w": 300,
              "x": 1550,
              "y": 1600
            }
          ]
        }
      },
      "product_fk": 2,
      "question_category": "OOSApproval",
      "question": "Is Kariot in the scene?",
      "question_uid": "questionUid",
      "question_type": "YesNo",
      "lastSeenSceneId":421205
    },
    {
        "question_type" : "YesNoCantTell",
        "value" : "1.1",
        "question_category" : "PriceApproval",
        "location": {
        "probes": [{
          "pk": 2187121,
          "masks": [
            {
              "h": 400,
              "w": 280,
              "x": 900,
              "y": 1450
            }
          ]
        },{
          "pk": 2187124,
          "masks": [
            {
              "h": 400,
              "w": 280,
              "x": 1150,
              "y": 100
            }
          ]
        }],
        "scene": {
          "productId": 1,
          "masks": [
            {
              "h": 350,
              "w": 300,
              "x": 1550,
              "y": 1600
            }
          ]
        }
        },
        "product_fk": 2,
        "question": "Is Kariot in the scene?",
        "question_uid": "questionUid",
        "lastSeenSceneId":421205
    },
    {
        "question_type" : "YesNoCantTell",
        "value" : "1.1",
        "question_category" : "PriceApproval",
        "return_fixed_value": true,
        "location": {
        "probes": [{
          "pk": 2187121,
          "masks": [
            {
              "h": 400,
              "w": 280,
              "x": 900,
              "y": 1450
            }
          ]
        },{
          "pk": 2187124,
          "masks": [
            {
              "h": 400,
              "w": 280,
              "x": 1150,
              "y": 100
            }
          ]
        }],
        "scene": {
          "productId": 1,
          "masks": [
            {
              "h": 350,
              "w": 300,
              "x": 1550,
              "y": 1600
            }
          ]
        }
        },
        "product_fk": 2,
        "question": "Is Kariot in the scene?",
        "question_uid": "questionUid",
        "lastSeenSceneId":421205
    },
    {
        "question_type" : "YesNo",
        "value" : "1.1",
        "question_category" :"PriceApproval",
        "return_fixed_value": true,
        "location": {
        "probes": [{
        "pk": 2187121,
        "masks": [
        {
          "h": 400,
          "w": 280,
          "x": 900,
          "y": 1450
        }
        ]
        },{
        "pk": 2187124,
        "masks": [
        {
          "h": 400,
          "w": 280,
          "x": 1150,
          "y": 100
        }
        ]
        }],
        "scene": {
        "productId": 1,
        "masks": [
        {
          "h": 350,
          "w": 300,
          "x": 1550,
          "y": 1600
        }
        ]
        }
        },
        "product_fk": 2,
        "question": "Is Kariot in the scene?",
        "question_uid": "questionUid",
        "lastSeenSceneId":421205
    }
  ],
  "project_name": "dev"
}'
),
(UUID(), '3', '1', '1', 'waveType', 'waveUid',
'2019-09-01 13:12:37.988', NULL, NULL, NULL, '0', NULL, NULL, '1', '0', '0',
'{
  "approval_id": 2,
  "job_type": "WatchRepair",
  "scene_id": 2,
  "wave_uid": "waveUid",
  "scene_uid": "sceneUid",
  "wave_type": "waveType",
  "session_uid": "sessionUid",
  "ttl_in_minutes": 60,
  "questions": [
    {
      "location": {
        "probes": [{
          "pk": 2187122,
          "masks": [
            {
              "h": 300,
              "w": 400,
              "x": 100,
              "y": 200
            }
          ]
        }],
        "scene": {
          "productId": 1,
          "masks": [
            {
              "h": 400,
              "w": 300,
              "x": 1500,
              "y": 1150
            }
          ]
        }
      },
      "product_fk": 1,
      "question_category": "OOSApproval",
      "question": "Can you find Coco pops on the shelf ?",
      "question_uid": "questionUid",
      "question_type": "YesNo",
      "lastSeenSceneId":421205
    },
    {
      "location": {
        "probes": [{
          "pk": 2187122,
          "masks": [
            {
              "h": 300,
              "w": 400,
              "x": 100,
              "y": 200
            }
          ]
        }],
        "scene": {
          "productId": 1,
          "masks": [
            {
              "h": 350,
              "w": 300,
              "x": 1550,
              "y": 1600
            }
          ]
        }
      },
      "product_fk": 2,
      "question_category": "OOSApproval",
      "question": "Is Kariot in the scene?",
      "question_uid": "questionUid",
      "question_type": "YesNo",
      "lastSeenSceneId":421205
    }
  ],
  "project_name": "dev"
}'
);



SET FOREIGN_KEY_CHECKS = 1;
