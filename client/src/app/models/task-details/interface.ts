export interface YesNoQuestion {
    readonly questionText: string;
    readonly productName: string;
    readonly value?: string;
    readonly yesAnswer: string;
    readonly noAnswer: string;
    readonly noValueAnswer: string;
    readonly valueType?: string;
    readonly valuePlaceHolder: string;
    readonly returnValue: boolean;
}

export interface YesNoCantTellQuestion  extends YesNoQuestion {
    readonly cantTellAnswer: string;
}
