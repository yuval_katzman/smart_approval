import {Injectable} from '@angular/core';
import {map, isEmpty, intersection} from 'lodash';
import {CookieService} from 'ngx-cookie-service';
import {environment} from '../../../environments/environment';
import {forkJoin, Observable, of} from 'rxjs';
import {User} from '../../models/user';
import {API} from '../API/api.service';
import {IndexedDbService} from '../indexedDb/indexed-db-service.service';
import {catchError} from "rxjs/operators";

@Injectable({
    providedIn: 'root'
})
export class UserService {
    private loggedInUser: User | undefined;

    constructor(private cookieService: CookieService, private api: API, private indexedDbService: IndexedDbService) {
    }

    isAuthorized(projectName?: string, roles?: string[]): Observable<boolean> {
        return new Observable((observer) => {
            if (!this.isAuthenticated()) {
                return observer.next(false);
            }

            this.getLoggedInUser().subscribe((user: User) => {
                const userRoles = map(user.roles, role => role.toLowerCase()),
                    requiredRoles = map(roles, role => role.toLowerCase());

                observer.next(
                    (!projectName || user.projects[projectName]) &&
                    (isEmpty(requiredRoles) || !isEmpty(intersection(userRoles, requiredRoles)))
                );
            });
        });
    }

    private setLoggedInUser(user) {
        this.loggedInUser = user;
    }

    getLoggedInUser(): Observable<User> {
        return new Observable((observer) => {
            if (this.loggedInUser) {
                return observer.next(this.loggedInUser);
            }

            const availableClouds = Object.keys(environment.config.cloudsUrls);
            const cloudsRequests = availableClouds.map(requestedCloud =>
                this.api.get('/api/users/profile', {requestedCloud}).pipe(
                    catchError(err => of(null))));

            forkJoin(cloudsRequests).subscribe((cloudProfiles: any) => {
                // If all clouds return error - logout
                if (!cloudProfiles.some(cloudProfile => cloudProfile != null)) {
                    return this.logout();
                }

                // Merge user projects from all clouds
                const rawUser = cloudProfiles[0];
                cloudProfiles.forEach((cloudProfile) => {
                    if (cloudProfile) {
                        rawUser.projects = Object.assign({}, rawUser.projects, cloudProfile.projects);
                    }
                });

                const user = new User({
                    address: rawUser.address,
                    birthDate: rawUser.birth_date,
                    center: rawUser.center,
                    country: rawUser.country,
                    createDate: rawUser.create_date,
                    currentCenter: rawUser.current_center,
                    deleteDate: rawUser.delete_date,
                    deleted: rawUser.deleted,
                    email: rawUser.email,
                    firstName: rawUser.first_name,
                    gender: rawUser.gender,
                    isActive: rawUser.is_active,
                    isStaff: rawUser.is_staff,
                    isSuperuser: rawUser.is_superuser,
                    joinDate: rawUser.join_date,
                    lastName: rawUser.last_name,
                    lastLogin: rawUser.last_login,
                    mobile: rawUser.mobile,
                    modules: rawUser.modules,
                    parentsPhone: rawUser.parents_phone,
                    projects: rawUser.projects,
                    roles: rawUser.roles,
                    staffId: rawUser.staff_id,
                    stitching: rawUser.stitching,
                    teamMembers: rawUser.teamMembers
                });
                this.setLoggedInUser(user);
                return observer.next(this.loggedInUser);
            });
        });
    }

    isAuthenticated(): boolean {
        return !!this.cookieService.get('connect.sid');
    }

    async logout() {
        this.cookieService.delete('connect.sid');
        await this.indexedDbService.deleteDB();
        window.localStorage.clear();
        window.location.href = `/login?redirectUrl=${window.location.href}`;
    }

    sendToAccessDeniedPage() {
        window.location.href = `${environment.config.baseUrl}/denied`;
    }
}
