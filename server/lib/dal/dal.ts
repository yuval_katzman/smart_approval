import {Db, ObjectID} from 'mongodb';
import moment = require('moment');
import {SmartUser} from '../defs';
import {sum, keyBy} from 'lodash'
import {SaveTask} from "../interfaces";

const nc = require('@trax-retail/node-core')('smart-approval');

export async function documentUserCancel(sessionId: string, projectName: string, jobType: string,
                                         objectId: number, objectName: string, reason: string) {
    const mongo: Db = await nc.db.mongo('users');

    const canceledMission = {
        project: projectName,
        job_type: jobType,
        time: new Date(),
        reason: reason,
        [objectName]: objectId,
    };

    return mongo.collection('login_sessions').update({session_id: sessionId}, {
        $push: {
            cancels: canceledMission,
        },
        $set: {
            last_task_date: new Date(),
        },
    });

}

export async function updateUserLoginInfo(projectName: string, sessionId: string, netDurationInSeconds: number) {
    const mongo: Db = await nc.db.mongo('users');

    return mongo.collection('login_sessions').update({session_id: sessionId}, {
        $inc: {
            [`tasks.${projectName}.approval.count`]: 1,
            [`tasks.${projectName}.approval.total_seconds`]: netDurationInSeconds,
            total_work_time: netDurationInSeconds,
        },
        $set: {
            last_task_date: new Date()
        },
    }, {
        upsert: true,
    });
}

export async function addUserStatistics(user: SmartUser, projectName: string, sessionId: string, task: SaveTask) {
    const mongo: Db = await nc.db.mongo('users');
    return mongo.collection('user_statistics')
        .insertOne({
            project_name: projectName,
            session_id: sessionId,
            user: user.email,
            duration: task.answers && sum(task.answers.map(a => a.duration)) || 0,
            answers: task.answers && task.answers.map(q => ({question_uid: q.question_uid, duration: q.duration})) || []
        });
}

export async function updateProjectStats(projectName: string) {
    const mongo: Db = await nc.db.mongo('projects');

    return mongo.collection('stats').update({
        date: moment.utc().startOf('day').toDate(),
        project: projectName,
    }, {
        $inc: {
            approvals: 1,
        },
    }, {
        upsert: true,
    });
}

export async function saveTaskToApprovalResult(user: SmartUser, task: SaveTask) {
    const mongo: Db = await nc.db.mongo('projects');
    const approvalResultCollection = mongo.collection('approval_results');
    const answers = keyBy(task.answers, answer => answer.product_fk)
    const approvalTaskResult = {
        original_task: task.originalTask.additionalInfo,
        createdAt: new Date(),
        user_name: user.staff_id || user.email,
        answers
    }

    await approvalResultCollection.insertOne(approvalTaskResult);
}

export async function  getTemplate(projectName, templateId: number) {
    const sql = await nc.db.sql(projectName);
    const result = await sql.query(`select name,template_group as  templateGroupName from static.template where pk = ?`, {
        type: 'SELECT',
        replacements: [templateId]
    })
    if (result)
        return result[0];
}

export async function  getTemplateGroupIds(projectName: string, templateGroup: string): Promise<number[] | undefined> {
    const sql = await nc.db.sql(projectName);
    const cache = await nc.cache();
    const cacheKey = `${projectName}-${templateGroup}`;
    let result = await cache.get(cacheKey)

    if (!result) {
        result = await sql.query(`
                SELECT pk 
                FROM static.template 
                WHERE template_group = ? AND delete_date IS NULL AND name NOT LIKE "~%"`, {
            type: 'SELECT',
            replacements: [templateGroup]
        });

        result = result ? result.map(row => {return row.pk}) : undefined;
        cache.set(cacheKey, result, 60 * 1000);
    }

    return result;
}
