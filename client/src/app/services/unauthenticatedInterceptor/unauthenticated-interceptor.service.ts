import { Injectable } from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {tap} from "rxjs/operators";
import {UserService} from "../user/user.service";

@Injectable({
  providedIn: 'root'
})
export class UnauthenticatedInterceptorService implements HttpInterceptor {

  constructor(private userService: UserService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
        tap(
            (response: HttpEvent<any>) => {
              return response;
            },
            (err: any) => {
                if (err instanceof HttpErrorResponse && (err.status === 401 || err.status === 403)) {
                    this.userService.logout();
              }
            }
        )
    );
  }
}
