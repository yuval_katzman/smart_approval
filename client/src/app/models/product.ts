
export class Product {
    att1: string;
    att2: string;
    brandId: number;
    categoryId: number;
    creationDate: string;
    deleteDate: null;
    eanCode: string;
    formFactor: string;
    globalStatusId: number;
    globalCode: string;
    hardMaximalPrice: number;
    hardMinimalPrice: number;
    id: number;
    images: {
        direction: string,
        id: number,
        path: string
    }[];
    isActive: boolean;
    isNew: boolean;
    localName: string;
    maximalPrice: number;
    minimalPrice: number;
    name: string;
    size: number;
    sizeUnit: string;
    smartCaption: string | null;
    smartDisplayGroup: null;
    smartDisplayOrder: null;
    subpackages: number | null;
    type: string;
    units: string | null;
    uuid: string;
    designChanges:
        {
            "id": number,
            "images":
                {
                    "id": number,
                    "path": string
                    "direction": string
                }[]

        }[];
    constructor(rawProduct) {
        this.att1 = rawProduct.att1;
        this.att2 = rawProduct.att2;
        this.brandId = rawProduct.brandId;
        this.categoryId = rawProduct.categoryId;
        this.creationDate = rawProduct.creationDate;
        this.deleteDate = rawProduct.deleteDate;
        this.designChanges = rawProduct.designChanges;
        this.eanCode = rawProduct.eanCode;
        this.formFactor = rawProduct.formFactor;
        this.globalStatusId = rawProduct.globalStatusId;
        this.globalCode = rawProduct.global_code;
        this.hardMaximalPrice = rawProduct.hardMaximalPrice;
        this.hardMinimalPrice = rawProduct.hardMinimalPrice;
        this.id = rawProduct.id;
        this.images = rawProduct.images;
        this.isActive = rawProduct.isActive;
        this.isNew = rawProduct.isNew;
        this.localName = rawProduct.localName;
        this.maximalPrice = rawProduct.maximalPrice;
        this.minimalPrice = rawProduct.minimalPrice;
        this.name = rawProduct.name;
        this.size = rawProduct.size;
        this.sizeUnit = rawProduct.sizeUnit;
        this.smartCaption = rawProduct.smartCaption;
        this.smartDisplayGroup = rawProduct.smartDisplayGroup;
        this.smartDisplayOrder = rawProduct.smartDisplayOrder;
        this.subpackages = rawProduct.subpackages;
        this.type = rawProduct.type;
        this.units = rawProduct.units;
        this.uuid = rawProduct.uuid;
        this.images.forEach(img => img.path);
    }
}

