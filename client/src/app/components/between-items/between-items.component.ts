import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Router} from '@angular/router';
import {environment} from '../../../environments/environment';
import {Subscription} from 'rxjs';
import {API} from "../../services/API/api.service";

@Component({
    selector: 'app-between-items',
    templateUrl: './between-items.component.html',
    styleUrls: ['./between-items.component.scss']
})
export class BetweenItemsComponent implements OnInit {
    weAreWaitingForYou = false;
    qCount: number;
    subscription: Subscription;

    constructor(
        public dialogRef: MatDialogRef<BetweenItemsComponent>,
        private router: Router,
        private api: API,
        @Inject(MAT_DIALOG_DATA) public data: any,
    ) {
    }

    ngOnInit() {
        this.qCount = this.data.qCount;
        this.subscription = this.data.qCountObservable.subscribe((qCount) => {
            this.qCount = qCount;
        });

        setTimeout(() => {
            this.weAreWaitingForYou = true;
        }, 10 * 1000);
    }

    onChangeProject(projectName: string) {
        this.router.navigate(['approvals', projectName, 'validate']);
        this.dialogRef.close();
    }

    onBreak(reason: string, projectName: string) {
        this.api.post('/approvals/api/:projectName/break-task', {reason}, {
            pathParams: { projectName } }).subscribe(_ => {
            window.location.href = environment.config.baseUrl;
        });
    }

    continue() {
        this.subscription.unsubscribe();
        this.dialogRef.close({
            getNextTask: true
        });
    }

}
