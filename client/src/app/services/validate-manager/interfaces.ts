import {Task} from "../../models/task";

export interface SaveTask {
    originalTask: Task;
    netDurationInSeconds: number;
    queueDurationInSeconds: number;
    wave_type: string;
    wave_uid: string;
    project_name: string;
    job_type: string;
    task_name: string;
    session_uid: string;
    scene_uid: string;
    scene_id: number;
    persistence: any;
    approval_id: number;
    answers: {
        question_uid: string,
        product_fk: number,
        question_category: string,
        question_type: string,
        answer: any,
        /// duration in milisecond.
        duration: number
    }[];
}
