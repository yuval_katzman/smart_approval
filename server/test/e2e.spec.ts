import {suite, test} from "mocha-typescript";
import ChaiStatic = Chai.ChaiStatic;
import {originalTask, PN} from './pn.service';
import {SaveTask, TaskType} from "../lib/interfaces";
import {Db} from "mongodb";
import {cloneDeep} from 'lodash';
const chai: ChaiStatic & { request: Function } = require("chai");
const chaiHttp = require('chai-http');
chai.should();
chai.use(chaiHttp);
const assert = require('chai').assert;
const sinon = require('sinon');
let pn: PN;
const wait = async (ms) => {
    await new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}

@suite
class queue {
    static async before() {
        pn = await PN.getInstance();
    }

    async before() {
        try {
            const selectorDB = await pn.db.sql('selector');
            const mongo: Db = await pn.db.mongo('projects');
            await mongo.collection('approval_results').deleteMany({})
            await selectorDB.query('delete from selector.task_pool');
            await wait(1000);
        }
        catch (e) {
            console.log(e)
        }
    }

    @test
    async save_task() {
        const task: any = await this.next_task1()
        const topicPublishStub = pn.cloud.topic.publish;
        const response: SaveTask = {
            originalTask: {
                "taskType": 'approval_voting',
                "objectType": "scene",
                "uuid": "e42a7bdb-980f-11ea-aaaa-02420a030378",
                "objectId": 2,
                "waveType": "waveType",
                "waveUid": "waveUid",
                "lockDurationInSeconds": 900,
                "queueDurationInSeconds": 22356903,
                "additionalInfo": {
                    approval_uid: 'approval_uid',
                    entity_id:"entity_id",
                    entity_type:"entity_type",
                    sub_queue:"sub_queue",
                    "job_type": "WatchRepair",
                    "scene_id": 2,
                    "wave_uid": "waveUid",
                    "questions": [{
                        "location": {
                            "scene": {
                                "masks": [{"h": 400, "w": 300, "x": 1500, "y": 1150}],
                                "productId": 1
                            }, "probes": [{"pk": 2187122, "masks": [{"h": 300, "w": 400, "x": 100, "y": 200}]}]
                        },
                        "question": "Can you find Coco pops on the shelf ?",
                        "product_fk": 1,
                        "question_uid": "questionUid",
                        "question_type": "YesNo",
                        "question_category": "OOSApproval"
                    }, {
                        "location": {
                            "scene": {"masks": [{"h": 350, "w": 300, "x": 1550, "y": 1600}], "productId": 1},
                            "probes": [{"pk": 2187122, "masks": [{"h": 300, "w": 400, "x": 100, "y": 200}]}]
                        },
                        "question": "Is Kariot in the scene?",
                        "product_fk": 2,
                        "question_uid": "questionUid",
                        "question_type": "YesNo",
                        "question_category": "OOSApproval"
                    }],
                    "scene_uid": "sceneUid",
                    "wave_type": "waveType",
                    "approval_id": "approval_id",
                    "session_uid": "sessionUid",
                    "project_name": "test",
                    "ttl_in_minutes": 60
                },
                "projectName": "test"
            },
            approval_id: 1,
            task_name: "oos_approval",
            netDurationInSeconds: 10,
            wave_type: 'waveType',
            wave_uid: 'waveUid',
            project_name: 'test',
            job_type: 'jobType',
            session_uid: 'sessionUid',
            queueDurationInSeconds: 10,
            scene_uid: 'sceneUid',
            scene_id: 1,
            persistence: {
                ma: 'kore',
            },
            answers: [{
                question_uid: 'questionUid',
                product_fk: 1,
                question_category: 'questionCategory',
                question_type: 'questionType',
                answer: true,
                duration: 1
            }],
        };

        const result = await chai.request(pn.express)
            .post(`${pn.config.express.apiPrefix}/test/save-task?selectorTaskId=${task.selectorTaskId}`)
            .set('Cookie', pn.cookie)
            .send(response)
        result.statusCode.should.be.eq(200);
        sinon.assert.callCount(topicPublishStub, 3);
        sinon.assert.calledWith(topicPublishStub.getCall(2),
            {
                approval_uid: 'approval_uid',
                "netDurationInSeconds": 10,
                "processing_time": 10,
                "queue_duration": 10,
                "wave_type": "waveType",
                "wave_uid": "waveUid",
                entity_id:"entity_id",
                entity_type:"entity_type",
                sub_queue:"sub_queue",
                "project_name": "test",
                "persistence": {"ma": "kore"},
                "job_type": "jobType",
                "session_uid": "sessionUid",
                "scene_uid": "sceneUid",
                "scene_id": 1,
                "answers": [{
                    "question_uid": "questionUid",
                    "product_fk": 1,
                    "question_category": "questionCategory",
                    "question_type": "questionType",
                    "answer": true,
                    "duration": 1
                }],
                "event_name": "APPROVAL-TASK_FINISHED",
                "user": "",
                "email": "user@traxretail.com",
                "approval_id": 1,
                "task_name": "oos_approval",
                "number_of_tasks": 1
            });

    }

    @test
    async getTemplate() {
        const result = await chai.request(pn.express)
            .get(`${pn.config.express.apiPrefix}/test/template`)
            .set('Cookie', pn.cookie)
            .query({id: 1});

        JSON.parse(result.res.text).name.should.be.equal("templateName");
    }

    @test
    async count_0() {
        const result = await chai.request(pn.express)
            .get(`${pn.config.express.apiPrefix}/test/queue-count`)
            .set('Cookie', pn.cookie)
        JSON.parse(result.res.text).count.should.be.equal(0);
    }

    @test
    async count_1() {
        await pn.insertTask();
        const result = await chai.request(pn.express)
            .get(`${pn.config.express.apiPrefix}/test/queue-count`)
            .set('Cookie', pn.cookie)
        JSON.parse(result.res.text).count.should.be.equal(1);
    }

    @test("should return the count of approval voting and oos approval tasks - approval voting queue is not exists")
    async count_2() {
        await pn.insertTask();
        const result = await chai.request(pn.express)
            .get(`${pn.config.express.apiPrefix}/test/queue-count`)
            .set('Cookie', pn.cookie)
        JSON.parse(result.res.text).count.should.be.equal(1);
    }

    @test("should return the count of approval voting and oos approval tasks")
    async count_3() {
        await pn.createApprovalVotingQueue();
        await pn.insertTask();
        await pn.insertTask(2);
        const result = await chai.request(pn.express)
            .get(`${pn.config.express.apiPrefix}/test/queue-count`)
            .set('Cookie', pn.cookie)
        JSON.parse(result.res.text).count.should.be.equal(2);
    }

    @test("should return the count of approvals")
    async count_4() {
        await pn.insertTask();
        await pn.insertTask(3);
        const result = await chai.request(pn.express)
            .get(`${pn.config.express.apiPrefix}/test/queue-count?subQueue=subQueueName`)
            .set('Cookie', pn.cookie)
        JSON.parse(result.res.text).count.should.be.equal(1);
    }

    @test("should return the count of approvals voting")
    async count_5() {
        await pn.insertTask();
        await pn.insertTask(4);
        const result = await chai.request(pn.express)
            .get(`${pn.config.express.apiPrefix}/test/queue-count?subQueue=subQueueName`)
            .set('Cookie', pn.cookie)
        JSON.parse(result.res.text).count.should.be.equal(1);
    }


    @test
    async next_task0() {
        const result = await chai.request(pn.express)
            .get(`${pn.config.express.apiPrefix}/test/next-task`)
            .set('Cookie', pn.cookie)
        result.res.text.should.be.equal('');
    }

    @test
    async next_task1() {
        await pn.insertTask();
        const result = await chai.request(pn.express)
            .get(`${pn.config.express.apiPrefix}/test/next-task`)
            .set('Cookie', pn.cookie)
        const resultJson = JSON.parse(result.res.text);
        assert.containsAllDeepKeys(resultJson, {
            'sceneId': 2,
            'scene_id': 2,
            'project': 'test',
            'projectName': 'test',
            'project_name': 'test',
            'taskType': 'oos_approval',
            'waveType': 'waveType',
            'waveUid': 'waveUid',
            'wave_type': 'waveType',
            'wave_uid': 'waveUid',
            'job_type': 'WatchRepair',
            'approval_id': 'approval_id',
            'questions': [
                {
                    'location': {
                        'scene': {
                            'masks': [
                                {
                                    'h': 400,
                                    'w': 300,
                                    'x': 1500,
                                    'y': 1150,
                                },
                            ],
                            'productId': 1,
                        },
                        'probes': [
                            {
                                'pk': 2187122,
                                'masks': [
                                    {
                                        'h': 300,
                                        'w': 400,
                                        'x': 100,
                                        'y': 200,
                                    },
                                ],
                            },
                        ],
                    },
                    'question': 'Can you find Coco pops on the shelf ?',
                    'product_fk': 1,
                    'question_uid': 'questionUid',
                    'question_type': 'YesNo',
                    'question_category': 'OOSApproval',
                },
                {
                    'location': {
                        'scene': {
                            'masks': [
                                {
                                    'h': 350,
                                    'w': 300,
                                    'x': 1550,
                                    'y': 1600,
                                },
                            ],
                            'productId': 1,
                        },
                        'probes': [
                            {
                                'pk': 2187122,
                                'masks': [
                                    {
                                        'h': 300,
                                        'w': 400,
                                        'x': 100,
                                        'y': 200,
                                    },
                                ],
                            },
                        ],
                    },
                    'question': 'Is Kariot in the scene?',
                    'product_fk': 2,
                    'question_uid': 'questionUid',
                    'question_type': 'YesNo',
                    'question_category': 'OOSApproval',
                },
            ],
            'scene_uid': 'sceneUid',
            'session_uid': 'sessionUid',
            'ttl_in_minutes': 60,
            'sceneData': {
                'scene': {
                    'project_name': 'test',
                    'scene_id': 2,
                    'type': 2,
                    'scene_date': '2019-09-04',
                    'scene_creation_time': '2019-09-04 12:09:48',
                    'stitched_image_path': null,
                    'store_id': 28,
                    'store_name': null,
                    'store_type': null,
                    'region': null,
                    'sales_rep_name': null,
                    'sales_rep_id': null,
                    'scene_type': null,
                    'scene_type_id': 60,
                    'number_of_probes': 4,
                    'retailer_name': null,
                    'number_of_ignored_probes': 0,
                    'store_area': null,
                    'actual_number_of_probes': 4,
                    'display_type_name': 'Primary Shelf',
                    'status': 6,
                    'score': null,
                    'comment': null,
                    'user': null,
                    'created_time': null,
                    'visit_id': 'd121f345-c7f3-4de7-a900-be6b1205d745',
                    'tiles_creation_time': '2019-09-04 12:19:24',
                    'flag_time': null,
                    'comments_num': 0,
                    'realogram_html_path': 'https://traxus.s3.amazonaws.com/shufersalil/scene-images/914757/Realogram_Scene914757_2019090412195019S.html',
                    'client_version': null,
                    'user_agent': null,
                    'reasons': null,
                    'reviewers': [],
                    'siblingSceneIds': [
                        2,
                        1,
                    ],
                    'isTiled': false,
                },
                'probeImages': [
                    {
                        'probe_id': 2187125,
                        'original_height': 1944,
                        'original_width': 2592,
                        'status': 3,
                        'store_name': null,
                        'sales_rep_name': null,
                        'scene_fk': 2,
                        'probe_image_path': 'http://traxus.s3.amazonaws.com/shufersalil/Probe_Images/20190904/28/20190904120232-681b9568-bc5c-4729-8a50-0a709874cd77',
                        'commentsNum': 0,
                    },
                    {
                        'probe_id': 2187126,
                        'original_height': 1944,
                        'original_width': 2592,
                        'status': 3,
                        'store_name': null,
                        'sales_rep_name': null,
                        'scene_fk': 2,
                        'probe_image_path': 'http://traxus.s3.amazonaws.com/shufersalil/Probe_Images/20190904/28/20190904120403-32ad5a32-7a28-49da-92f9-2c31a75a343c',
                        'commentsNum': 0,
                    },
                    {
                        'probe_id': 2187127,
                        'original_height': 1944,
                        'original_width': 2592,
                        'status': 3,
                        'store_name': null,
                        'sales_rep_name': null,
                        'scene_fk': 2,
                        'probe_image_path': 'http://traxus.s3.amazonaws.com/shufersalil/Probe_Images/20190904/28/20190904120333-b83bbd5b-8585-4004-a89a-52981b03b3a6',
                        'commentsNum': 0,
                    },
                    {
                        'probe_id': 2187128,
                        'original_height': 1944,
                        'original_width': 2592,
                        'status': 3,
                        'store_name': null,
                        'sales_rep_name': null,
                        'scene_fk': 2,
                        'probe_image_path': 'http://traxus.s3.amazonaws.com/shufersalil/Probe_Images/20190904/28/20190904120306-5139aac9-8d16-49a1-98ee-439fadd128b9',
                        'commentsNum': 0,
                    },
                ],
                'products': [],
                'projectName': 'test',
                'sceneId': '2',
                'stitchingData': {
                    'molecules': [
                        {
                            'data': [
                                {
                                    'transform': [
                                        [
                                            0.5091549277380019,
                                            -0.12541454542758315,
                                            707.7204201318028,
                                        ],
                                        [
                                            0.04636412888493394,
                                            0.3455915782831826,
                                            351.4837065844982,
                                        ],
                                        [
                                            0.00009087432193519817,
                                            -0.0000998993193258421,
                                            1,
                                        ],
                                    ],
                                    'probe_pk': 2187128,
                                },
                                {
                                    'transform': [
                                        [
                                            0.5119771378773853,
                                            0.06465050632929253,
                                            766.2400775131013,
                                        ],
                                        [
                                            0.022263813827079433,
                                            0.4401206285418588,
                                            -224.70131268183695,
                                        ],
                                        [
                                            0.00005137320179831387,
                                            0.00004831432200487323,
                                            1,
                                        ],
                                    ],
                                    'probe_pk': 2187125,
                                },
                                {
                                    'transform': [
                                        [
                                            0.30756612738223577,
                                            0.08156831198306008,
                                            1584.6182880650888,
                                        ],
                                        [
                                            0.003667066395440073,
                                            0.397580567531726,
                                            -226.67798240115604,
                                        ],
                                        [
                                            -0.000043238790527485414,
                                            0.000046632446903496506,
                                            1,
                                        ],
                                    ],
                                    'probe_pk': 2187126,
                                },
                                {
                                    'transform': [
                                        [
                                            0.2826513427710345,
                                            -0.0460582413603336,
                                            1595.7186432627257,
                                        ],
                                        [
                                            -0.01774839456188139,
                                            0.35892594409326906,
                                            215.6920030451096,
                                        ],
                                        [
                                            -0.000043031952781952376,
                                            -0.000023877339303100934,
                                            1,
                                        ],
                                    ],
                                    'probe_pk': 2187127,
                                },
                            ],
                        },
                    ],
                    'liveMolecules': [
                        {
                            'data': [
                                {
                                    'transform': [
                                        [
                                            null,
                                            null,
                                            null,
                                        ],
                                        [
                                            null,
                                            null,
                                            null,
                                        ],
                                        [
                                            null,
                                            null,
                                            null,
                                        ],
                                    ],
                                    'probe_pk': 2187125,
                                },
                                {
                                    'transform': [
                                        [
                                            null,
                                            null,
                                            null,
                                        ],
                                        [
                                            null,
                                            null,
                                            null,
                                        ],
                                        [
                                            null,
                                            null,
                                            null,
                                        ],
                                    ],
                                    'probe_pk': 2187126,
                                },
                                {
                                    'transform': [
                                        [
                                            null,
                                            null,
                                            null,
                                        ],
                                        [
                                            null,
                                            null,
                                            null,
                                        ],
                                        [
                                            null,
                                            null,
                                            null,
                                        ],
                                    ],
                                    'probe_pk': 2187127,
                                },
                                {
                                    'transform': [
                                        [
                                            null,
                                            null,
                                            null,
                                        ],
                                        [
                                            null,
                                            null,
                                            null,
                                        ],
                                        [
                                            null,
                                            null,
                                            null,
                                        ],
                                    ],
                                    'probe_pk': 2187128,
                                },
                            ],
                        },
                    ],
                    'dividers': [],
                    'liveDividers': [],
                    'displayTypeId': 1,
                    'stitchingSceneInfoId': 121683,
                    'probes': [
                        {
                            'probe_url': 'http://traxus.s3.amazonaws.com/shufersalil/Probe_Images/20190904/28/20190904120306-5139aac9-8d16-49a1-98ee-439fadd128b9',
                            'creation_time': '2019-09-04 12:09:51',
                            'local_image_time': '2019-09-04 12:03:06',
                            'probe_pk': 2187128,
                            'original_width': 2592,
                            'original_height': 1944,
                        },
                        {
                            'probe_url': 'http://traxus.s3.amazonaws.com/shufersalil/Probe_Images/20190904/28/20190904120232-681b9568-bc5c-4729-8a50-0a709874cd77',
                            'creation_time': '2019-09-04 12:09:49',
                            'local_image_time': '2019-09-04 12:02:32',
                            'probe_pk': 2187125,
                            'original_width': 2592,
                            'original_height': 1944,
                        },
                        {
                            'probe_url': 'http://traxus.s3.amazonaws.com/shufersalil/Probe_Images/20190904/28/20190904120403-32ad5a32-7a28-49da-92f9-2c31a75a343c',
                            'creation_time': '2019-09-04 12:09:50',
                            'local_image_time': '2019-09-04 12:04:03',
                            'probe_pk': 2187126,
                            'original_width': 2592,
                            'original_height': 1944,
                        },
                        {
                            'probe_url': 'http://traxus.s3.amazonaws.com/shufersalil/Probe_Images/20190904/28/20190904120333-b83bbd5b-8585-4004-a89a-52981b03b3a6',
                            'creation_time': '2019-09-04 12:09:50',
                            'local_image_time': '2019-09-04 12:03:33',
                            'probe_pk': 2187127,
                            'original_width': 2592,
                            'original_height': 1944,
                        },
                    ],
                },
                'isSceneRecognition': false,
                'displayTypes': [],
                'liveProducts': [],
                'isLive': false,
            },
            'productData': [
                {
                    'id': 1,
                    'name': 'Kellogges Coco Pops Cereal Choco Chocolate Cardboard Box 500 g - 5050083559761',
                    'localName': 'קוקו פופס שוקוס קלוג"500 5050083559761',
                    'uuid': '76c0eb90-1e5c-11e9-86dd-dbdadbd45d50',
                    'categoryLocalName': null,
                    'categoryName': null,
                    'type': 'SKU',
                    'isActive': true,
                    'globalStatusId': 4,
                    'formFactor': 'cardboard box',
                    'size': 500,
                    'sizeUnit': 'g',
                    'creationDate': '2019-01-22T15:43:28.000Z',
                    'eanCode': '5050083559761',
                    'subpackages': null,
                    'units': null,
                    'att1': null,
                    'att2': '5050083559761',
                    'isNew': false,
                    'smartCaption': null,
                    'smartDisplayGroup': null,
                    'smartDisplayOrder': null,
                    'deleteDate': null,
                    'minimalPrice': 0.01,
                    'hardMinimalPrice': 0.001,
                    'maximalPrice': 1000000,
                    'hardMaximalPrice': 10000000,
                    'categoryId': 23,
                    'brandId': 395,
                    'global_code': null,
                    'images': [
                        {
                            'id': 1,
                            'path': 'https://traxus.s3.amazonaws.com/shufersalil/Product_Images/8864/Front20190122154328',
                            'direction': 'Front',
                        },
                        {
                            'id': 5,
                            'path': 'https://traxus.s3.amazonaws.com/shufersalil/Product_Images/8864/Side120190422082304',
                            'direction': 'Side1',
                        },
                        {
                            'id': 6,
                            'path': 'https://traxus.s3.amazonaws.com/shufersalil/Product_Images/8864/Side220190422082304',
                            'direction': 'Side2',
                        },
                        {
                            'id': 3,
                            'path': 'https://traxus.s3.amazonaws.com/shufersalil/Product_Images/8864/Top20190425075456',
                            'direction': 'Top',
                        },
                        {
                            'id': 4,
                            'path': 'https://traxus.s3.amazonaws.com/shufersalil/Product_Images/8864/Bottom20190425075456',
                            'direction': 'Bottom',
                        },
                    ],
                    'designChanges': [],
                },
                {
                    'id': 2,
                    'name': 'Thelma Kariot Cereal Chocolada Cardboard Box 750 g - 2741077',
                    'localName': 'כריות צוקולדה 750 גרם 2741077',
                    'uuid': '77bc89a0-1e5c-11e9-86dd-dbdadbd45d50',
                    'categoryLocalName': null,
                    'categoryName': null,
                    'type': 'SKU',
                    'isActive': true,
                    'globalStatusId': 4,
                    'formFactor': 'cardboard box',
                    'size': 750,
                    'sizeUnit': 'g',
                    'creationDate': '2019-01-22T15:43:30.000Z',
                    'eanCode': '2741077',
                    'subpackages': null,
                    'units': null,
                    'att1': null,
                    'att2': '2741077',
                    'isNew': false,
                    'smartCaption': null,
                    'smartDisplayGroup': null,
                    'smartDisplayOrder': null,
                    'deleteDate': null,
                    'minimalPrice': 0.01,
                    'hardMinimalPrice': 0.001,
                    'maximalPrice': 1000000,
                    'hardMaximalPrice': 10000000,
                    'categoryId': 23,
                    'brandId': 394,
                    'global_code': null,
                    'images': [
                        {
                            'id': 7,
                            'path': 'https://traxus.s3.amazonaws.com/shufersalil/Product_Images/8921/Front20190122154330',
                            'direction': 'Front',
                        },
                        {
                            'id': 11,
                            'path': 'https://traxus.s3.amazonaws.com/shufersalil/Product_Images/8921/Side120190422082305',
                            'direction': 'Side1',
                        },
                        {
                            'id': 12,
                            'path': 'https://traxus.s3.amazonaws.com/shufersalil/Product_Images/8921/Side220190422082305',
                            'direction': 'Side2',
                        },
                    ],
                    'designChanges': [],
                },
            ],
        });
        return resultJson;
    }

    @test("should remove invalid products")
    async next_task2() {
        const task = cloneDeep(originalTask);
        task.questions[0].product_fk = 9999;
        await pn.insertTask(1, task);
        const result = await chai.request(pn.express)
            .get(`${pn.config.express.apiPrefix}/test/next-task`)
            .set('Cookie', pn.cookie)
        JSON.parse(result.res.text).questions.length.should.be.equal(1);
    }

    @test("should retry get next task after first will fail")
    async next_task3() {
        const task = cloneDeep(originalTask);
        task.questions.forEach(q => q.product_fk = 99999);
        task.approval_id = "1";
        // insert task with invalid products
        await pn.insertTask(1, task);

        // insert valid task
        await pn.insertTask();
        const result = await chai.request(pn.express)
            .get(`${pn.config.express.apiPrefix}/test/next-task`)
            .set('Cookie', pn.cookie)
        JSON.parse(result.res.text).questions.length.should.be.equal(2);

        // validate that the queue is empty
        await  wait(1000)
        await this.count_0();
    }

    @test("get next task - product in task doesn't exist - should return null if queue is empty")
    async next_task4() {
        const task = cloneDeep(originalTask);
        task.questions.forEach(q => q.product_fk = 99999);
        task.approval_id = "1";
        // insert task with invalid products
        await pn.insertTask(1, task);
        task.approval_id = "2";
        await pn.insertTask(1, task);
        const result = await chai.request(pn.express)
            .get(`${pn.config.express.apiPrefix}/test/next-task`)
            .set('Cookie', pn.cookie);
        result.statusCode.should.be.equal(200);
        result.body.should.be.empty;

        // validate that the queue is empty
        await  wait(1000);
        await this.count_0();
    }

    @test("get next task - product in task doesn't exist - should return next task")
    async next_task7() {
        const task = cloneDeep(originalTask);
        task.questions.forEach(q => q.product_fk = 99999);
        task.approval_id = "1";
        // insert task with invalid products
        await pn.insertTask(1, task);
        task.approval_id = "2";
        task.questions.forEach(q => q.product_fk = 1);
        await pn.insertTask(1, task);
        const result = await chai.request(pn.express)
            .get(`${pn.config.express.apiPrefix}/test/next-task`)
            .set('Cookie', pn.cookie);
        result.statusCode.should.be.equal(200);
        result.body.should.have.property('approval_id').that.equals('2');

        // validate that the queue is empty
        await  wait(1000);
        await this.count_0();
    }


    @test("get next task - scene in task doesn't exist - should return null if queue is empty")
    async next_task5() {
        const task = cloneDeep(originalTask);
        task.scene_id=99999999;
        await pn.insertTask(1, task);
        const result = await chai.request(pn.express)
            .get(`${pn.config.express.apiPrefix}/test/next-task`)
            .set('Cookie', pn.cookie);
        result.statusCode.should.be.equal(200);
        result.body.should.be.empty;

        // validate that the queue is empty
        await  wait(1000);
        await this.count_0();
    }

    @test("get next task - scene in task doesn't exist - should return next task")
    async next_task8() {
        const task = cloneDeep(originalTask);
        task.scene_id=99999999;
        await pn.insertTask(1, task);
        task.approval_id = "2";
        task.scene_id = 2;
        await pn.insertTask(1, task);
        const result = await chai.request(pn.express)
            .get(`${pn.config.express.apiPrefix}/test/next-task`)
            .set('Cookie', pn.cookie);
        result.statusCode.should.be.equal(200);
        result.body.should.have.property('approval_id').that.equals('2');

        // validate that the queue is empty
        await  wait(1000);
        await this.count_0();
    }


    @test("should return probe data when it is probe task")
    async next_task6() {
        const task:  any = cloneDeep(originalTask);
        task.scene_id = undefined;
        task.entity_id = 2187122;
        task.entity_type = 'probe';

        await pn.insertTask(1, task);
        const result = await chai.request(pn.express)
            .get(`${pn.config.express.apiPrefix}/test/next-task`)
            .set('Cookie', pn.cookie)
        JSON.parse(result.res.text).probeData.probeUrl.should.be.eq("http://traxus.s3.amazonaws.com/shufersalil/Probe_Images/20190904/28/20190904120403-32ad5a32-7a28-49da-92f9-2c31a75a343b");
    }
    @test
    async next_voting_task() {
        await pn.insertTask(2);
        const result = await chai.request(pn.express)
            .get(`${pn.config.express.apiPrefix}/test/next-task`)
            .set('Cookie', pn.cookie)
        const resultJson = JSON.parse(result.res.text);
        assert.equal(resultJson.taskType, TaskType.approvalVoting)
    }

    @test
    async next_approvals_task() {
        const task = cloneDeep(originalTask);
        task.job_type = 'approvals';
        await pn.insertTask(3, task);
        const result = await chai.request(pn.express)
            .get(`${pn.config.express.apiPrefix}/test/next-task?subQueue=subQueueName`)
            .set('Cookie', pn.cookie)
        const resultJson = JSON.parse(result.res.text);
        assert.equal(resultJson.taskType, TaskType.approvals)
    }

    @test
    async next_approvals_voting_task() {
        const task = cloneDeep(originalTask);
        task.job_type = 'approvals_voting';
        await pn.insertTask(4, task);
        const result = await chai.request(pn.express)
            .get(`${pn.config.express.apiPrefix}/test/next-task?subQueue=subQueueName`)
            .set('Cookie', pn.cookie)
        const resultJson = JSON.parse(result.res.text);
        assert.equal(resultJson.taskType, TaskType.approvalsVoting)
    }

    @test
    async cancel_task() {
        const task: any = await this.next_task1()
        const result = await chai.request(pn.express)
            .put(`${pn.config.express.apiPrefix}/test/cancel-task?selectorTaskId=${task.selectorTaskId}`)
            .set('Cookie', pn.cookie)
            .send({sceneId: task.sceneId, reason: 'reason'});
        result.statusCode.should.be.eq(200);
    }

    @test
    async break_task() {
        const result = await chai.request(pn.express)
            .post(`${pn.config.express.apiPrefix}/test/break-task`)
            .set('Cookie', pn.cookie)
            .send({ reason: 'reason'});
        result.statusCode.should.be.eq(200);
    }

    @test
    async extend_lock() {
        const task: any = await this.next_task1()
        const result = await chai.request(pn.express)
            .put(`${pn.config.express.apiPrefix}/test/extend-lock?selectorTaskId=${task.selectorTaskId}`)
            .set('Cookie', pn.cookie)
        result.statusCode.should.be.eq(200);
    }

    @test
    async save_task_to_approval_result() {
        const task: any = await this.next_task1()
        const response: SaveTask = {
            originalTask: {
                "taskType": "approval_voting",
                "objectType": "scene",
                "uuid": "e42a7bdb-980f-11ea-aaaa-02420a030378",
                "objectId": 2,
                "waveType": "waveType",
                "waveUid": "waveUid",
                "lockDurationInSeconds": 900,
                "queueDurationInSeconds": 22356903,
                "additionalInfo": {
                    "job_type": "Ace",
                    "scene_id": 2,
                    "wave_uid": "waveUid",
                    "questions": [{
                        "location": {
                            "scene": {
                                "masks": [{"h": 400, "w": 300, "x": 1500, "y": 1150}],
                                "productId": 1
                            }, "probes": [{"pk": 2187122, "masks": [{"h": 300, "w": 400, "x": 100, "y": 200}]}]
                        },
                        "question": "Can you find Coco pops on the shelf ?",
                        "product_fk": 1,
                        "question_uid": "questionUid",
                        "question_type": "YesNo",
                        "question_category": "OOSApproval"
                    }, {
                        "location": {
                            "scene": {"masks": [{"h": 350, "w": 300, "x": 1550, "y": 1600}], "productId": 1},
                            "probes": [{"pk": 2187122, "masks": [{"h": 300, "w": 400, "x": 100, "y": 200}]}]
                        },
                        "question": "Is Kariot in the scene?",
                        "product_fk": 2,
                        "question_uid": "questionUid",
                        "question_type": "YesNo",
                        "question_category": "OOSApproval"
                    }],
                    "scene_uid": "sceneUid",
                    "wave_type": "waveType",
                    "approval_id": "approval_id",
                    "session_uid": "sessionUid",
                    "project_name": "test",
                    "ttl_in_minutes": 60
                },
                "projectName": "test"
            },
            approval_id: 1,
            task_name: "approval_voting",
            netDurationInSeconds: 10,
            wave_type: 'waveType',
            wave_uid: 'waveUid',
            project_name: 'test',
            job_type: 'jobType',
            session_uid: 'sessionUid',
            queueDurationInSeconds: 10,
            scene_uid: 'sceneUid',
            scene_id: 1,
            persistence: {
                ma: 'kore',
            },
            answers: [{
                question_uid: 'questionUid',
                product_fk: 1,
                question_category: 'questionCategory',
                question_type: 'questionType',
                answer: true,
                duration: 1
            }],
        };

        await chai.request(pn.express)
            .post(`${pn.config.express.apiPrefix}/test/save-task?selectorTaskId=${task.selectorTaskId}`)
            .set('Cookie', pn.cookie)
            .send(response)

        const mongo: Db = await pn.db.mongo('projects');
        let approvalResults: any = await mongo.collection('approval_results').find({}).toArray();
        assert.equal(approvalResults.length, 1)

        await chai.request(pn.express)
            .post(`${pn.config.express.apiPrefix}/test/save-task?selectorTaskId=${task.selectorTaskId}`)
            .set('Cookie', pn.cookie)
            .send(response)
        approvalResults = await mongo.collection('approval_results').find({}).toArray();
        assert.equal(approvalResults.length, 2)
    }

    @test
    async count_by_template_id() {
        const selectorDB = await pn.db.sql('selector');
        await selectorDB.query(`INSERT INTO selector.queue (pk, name, task_type_fk, visibility_timeout, project_fk) VALUES
            (22, '{"taskType":"oos_approval","projectName":"test","templateId":1}', 19, 900000, 1);`);
        await pn.insertTask(22);
        const result = await chai.request(pn.express)
            .get(`${pn.config.express.apiPrefix}/test/queue-count?templateId=1`)
            .set('Cookie', pn.cookie)
        JSON.parse(result.res.text).count.should.be.equal(1);
    }

    @test
    async next_task_by_template_id() {
        await pn.insertTask(22);
        const result = await chai.request(pn.express)
            .get(`${pn.config.express.apiPrefix}/test/next-task?templateId=1`)
            .set('Cookie', pn.cookie)
        JSON.parse(result.res.text).sceneId.should.be.equal(2);
    }

    @test
    async count_by_template_group() {
        const selectorDB = await pn.db.sql('selector');
        await selectorDB.query(`INSERT INTO selector.queue (pk, name, task_type_fk, visibility_timeout, project_fk) VALUES
            (33, '{"taskType":"oos_approval","projectName":"test","templateGroup":"saba"}', 19, 900000, 1);`);
        await pn.insertTask(33);
        const result = await chai.request(pn.express)
            .get(`${pn.config.express.apiPrefix}/test/queue-count?templateGroup=saba`)
            .set('Cookie', pn.cookie)
        JSON.parse(result.res.text).count.should.be.equal(1);
    }

    @test
    async next_task_by_template_group() {
        await pn.insertTask(33);
        const result = await chai.request(pn.express)
            .get(`${pn.config.express.apiPrefix}/test/next-task?templateGroup=saba`)
            .set('Cookie', pn.cookie)
        JSON.parse(result.res.text).sceneId.should.be.equal(2);
    }

    @test
    async count_by_category_id() {
        await pn.insertTask(5);
        const result = await pn.query('queue-count?traxCategoryUuid=UUID&subQueue=subQueueName2')
        result.count.should.be.equal(1);
    }

    @test
    async next_task_by_category_id() {
        await pn.insertTask(5);
        const result = await pn.query('next-task?traxCategoryUuid=UUID&subQueue=subQueueName2')
        result.sceneId.should.be.equal(2);
    }
}
