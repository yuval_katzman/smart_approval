SET FOREIGN_KEY_CHECKS = 0;

TRUNCATE TABLE user_management.projects;
INSERT INTO user_management.projects (pk, name, description, domain, creation_date, is_active) VALUES
('1', 'Backend-Service', 'Backend-Service', NULL, '2018-02-04 08:30:02', '1');

TRUNCATE TABLE user_management.applications;
INSERT INTO user_management.applications (pk, name, description, creation_time, application_code, is_internal) VALUES
('1', 'Explorer', '[{\"entity\": \"Scene\", \"permissions\": [\"view\"]}, {\"entity\": \"Visit\", \"permissions\": [\"view\"]}]', '2017-01-08 16:19:37', 'EXPLORER', '0');

TRUNCATE TABLE user_management.users;
INSERT INTO user_management.users (pk, user_name, password, status, email, creation_time, first_name, last_name, is_internal, mobile_number) VALUES
('1', 'retailwatch@traxretail.com', NULL, 'ACTIVE', 'retailwatch@traxretail.com', '2018-05-15 13:41:38', NULL, NULL, '1', NULL);

TRUNCATE TABLE user_management.user_projects;
INSERT INTO user_management.user_projects (pk, user_fk, project_fk, user_type) VALUES
('1', '1', '1', 'USER');

TRUNCATE TABLE user_management.roles;
INSERT INTO user_management.roles (pk, project_fk, name, creation_time, description, is_internal) VALUES
('1', '1', 'smart', '2018-05-16 13:50:19', 'backend', '0'),
('2', '1', 'explorer',  '2018-05-16 13:50:19', 'explorer', '0');

TRUNCATE TABLE user_management.role_applications;
INSERT INTO user_management.role_applications (pk, role_fk, application_fk) VALUES
(1, 2, 1);

TRUNCATE TABLE user_management.user_roles;
INSERT INTO user_management.user_roles (pk, role_fk, user_fk, creation_time) VALUES
('1', '1', '1', '2018-05-16 13:55:10'),
('2', '2', '1', '2018-05-16 13:55:10');

SET FOREIGN_KEY_CHECKS = 1;
