import {YesNoCantTellQuestion, YesNoQuestion} from "./interface";

export class PriceApprovalYesNo implements YesNoQuestion {
    constructor(public productName: string, public value: string, public returnValue = false) {}
    readonly questionText = `Is the price for product ${this.productName} is ${this.value}?`;
    readonly valuePlaceHolder = '00.0';
    readonly valueType = 'number';
    readonly noValueAnswer: string = 'No, the price is';
    readonly noAnswer: string = 'No, the price is incorrect';
    readonly yesAnswer: string = `Yes, the price is $ ${this.value}`;
}

export class PriceApprovalYesNoCantTell extends PriceApprovalYesNo implements YesNoCantTellQuestion {
    constructor(public productName: string, public value: string, public returnValue = false) {
        super(productName, value);
    }

    readonly cantTellAnswer: string = 'There is no price tag';
}


