import {
  Directive,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import {
  debounce,
  forEach,
  find,
  findIndex,
  cloneDeep,
  isFunction,
  filter,
  get
} from "lodash";
import Mask from "../../models/mask.interface";
import {SceneParams} from "../../models/scene-params.interface";
import * as jquery from "jquery";
import {Compound} from "../../models/compound";

interface RenderHelper {
  firstBuffer: ImageBuffer;
  secondBuffer: ImageBuffer;
  isFirstBufferActive: boolean;
  completed: boolean;
  activeBuffer: ImageBuffer;
  backBuffer: ImageBuffer;
  whichBuffer: () => void;
  swapBuffers: () => void;
  changeImageLevel: (newLevel: number) => void;
}

interface ImageBuffer {
  image: HTMLImageElement;
  level: number;
}

interface Decorator {
  renderingParams: { [position: string]: number };
  x: number;
  y: number;
  tag?: any;
  isActivePredicate?: (d: Decorator) => boolean;
  onHover: () => void;
  onClick: () => void;
  render: () => void;
  isActive?: () => boolean;
}

interface MegazordMask extends Mask {
  vectors: {}[];
}

interface MegazordContext extends CanvasRenderingContext2D {
  inverseTransformedPoint: (x: number, y: number) => any;
  transformedPoint: (x: number, y: number) => any;
  getEffectiveScale: () => any;
  getScaleX: () => number;
  getScaleY: () => number;
}

@Directive({
  selector: '[appMegazordCanvas]'
})
export class MegazordCanvasDirective implements OnInit, OnDestroy, OnChanges {
  @Input() sceneParamsInput: SceneParams;
  @Input() apiInput: {};
  @Input() tags: any[];
  @Input() compound: Compound;
  @Input() masks: MegazordMask[];
  @Input() lineVectors: any;
  @Input() pluginDataCallback: () => {};
  @Input() readOnly: boolean;
  @Input() zoomEvent: EventEmitter<number>;
  @Input() fitToScreen: EventEmitter<{}>;

  @Output() renderBeganEvent = new EventEmitter();
  @Output() moveBeganEvent = new EventEmitter();
  @Output() moveFinishedEvent = new EventEmitter();
  @Output() renderFinishedEvent = new EventEmitter();
  @Output() imageLoadedEvent = new EventEmitter();
  @Output() tagClickedEvent = new EventEmitter();
  @Output() hoveringOverTag = new EventEmitter<{}>();
  @Output() annotationButtonClicked = new EventEmitter<{}>();
  @Output() tagAdded = new EventEmitter();
  @Output() maskDoneEvent = new EventEmitter();
  @Output() scaleChange = new EventEmitter();

  public api: { [method: string]: () => void };

  private canvas: HTMLCanvasElement;
  private context: MegazordContext;
  private scaleFactorPerClick = 1.03;
  private currentImgLevel = 6;
  private canvasParent: HTMLElement;
  private frustum: { [direction: string]: number } = {
    top: null,
    bottom: null,
    left: null,
    right: null
  };
  private lineWidth = 30;
  private previousDistBetweenTouches: number;
  private touchPointsCenter: { x: number, y: number } = {
    x: null,
    y: null
  };
  private sceneParams: {
    sceneHeight: number,
    sceneWidth: number,
    numOfTilesX: number,
    numOfTilesY: number,
    tileWidth: number,
    tileHeight: number
  };
  private lineRenderOffset: number = this.lineWidth / 2;
  private tilesToRender: any[] = [];
  private calculatedSceneBounds: { [position: string]: number };
  private displayBaysShelves = true;
  private tagImageSrc = `/assets/img/tag.png`;
  private redTagImageSrc = `/assets/img/redTag.png`;
  private greenTagImageSrc = `/assets/img/greenTag.png`;
  private pricedTagImageSrc = `/assets/img/tag-price.png`;
  private highlightedTagImageSrc = `/assets/img/selected-pin.png`;
  private highlightedPriceDecoratorImageSrc = `/assets/img/label-partially-filled-selected.png`;
  private displayTagImageSrc = `/assets/img/displayTypeTag.png`;
  private xDecoratorImageSrc = `/assets/img/delete.png`;
  private pricingDecoratorImageSrc = `/assets/img/label-partially-filled-hover.png`;
  private tagColorsImg: { [color: string]: HTMLImageElement } = {
    red: null,
    yellow: null,
    green: null,
    grey: null
  };
  private tagIconImg: HTMLImageElement;
  private highlightedTagIconImg: HTMLImageElement;
  private pricedTagIconImg: HTMLImageElement;
  private highlightedPriceTagIconImg: HTMLImageElement;
  private highlightedPriceDecoratorIconImg: HTMLImageElement;
  private displayTypeIconImg: HTMLImageElement;
  private xDecoratorIconImg: HTMLImageElement;
  private pricingDecoratorIconImg: HTMLImageElement;
  private initialMaxTagSize: number;
  private initialMaxXDecoratorSize: number;
  private initialMaxAnnotationDecoratorSize: number;
  private maxTagSize: number;
  private maxXDecoratorSize: number;
  private maxAnnotationDecoratorSize: number;
  private zoomingBounds: { [scale: string]: number } = {
    minScale: null,
    maxScale: null
  };
  private debouncedNotifyMoveBegan: () => void;
  private debouncedNotifyMoveFinished: () => void;
  private numOfTilesToRenderNow: number;
  private tilesFinishedRenderInThisCycle: number;
  private numOfTilesToRenderInThisCycle: number;
  private megazordCanvasInitiated: boolean;
  private isMaskingModeToggled: boolean;
  private resizeCanvasDebounced: () => void;
  private selectedMask: MegazordMask;
  private selectedMaskDeleteButtonDecorator: Decorator;
  private selectedMaskAnnotationButtonDecorator: Decorator;
  public renderingConfig = {
    isDynamicTagSizing: true,
    tagSizingFactor: 1,
    decoratorSizingFactor: 1
  };
  private onClick: (e: Event) => void;
  private defaultLineDrawingStyle: { color: string, lineDash: any[], lineWidth: number } = {
    color: 'blue',
    lineDash: [],
    lineWidth: null
  };
  private maskLineDrawingStyle = {
    color: 'yellow',
    lineDash: [30, 20],
    lineWidth: 30
  };
  private canvasActualWidth: number;
  private canvasActualHeight: number;

  private firstTimeLoadingCallback: () => void;

  private getMaskLineDrawingStyle = (() => {
    return this.maskLineDrawingStyle;
  });
  private boundFunctions: { boundElement: any, eventType: string, originalFn: (e: Event) => void, boundFn: () => void }[] = [];
  public apiMethods = {
    fitToScreen: () => {
      let topLeft;
      let bottomRight;
      let transformedBottomRight;
      let offsetFromTop;

      if (this.calculatedSceneBounds) {
        topLeft = {
          x: this.calculatedSceneBounds.minX,
          y: this.calculatedSceneBounds.minY
        };
        bottomRight = {
          x: this.calculatedSceneBounds.maxX,
          y: this.calculatedSceneBounds.maxY
        };
      } else {
        topLeft = {
          x: 0,
          y: 0// sceneParams.sceneHeight / 4
        };
        bottomRight = {
          x: this.sceneParams.sceneWidth,
          y: this.sceneParams.sceneHeight
        };
      }

      this.moveFrustumTo(topLeft, bottomRight);

      transformedBottomRight = this.context.transformedPoint(this.canvasActualWidth, this.canvasActualHeight);
      offsetFromTop = (transformedBottomRight.y - this.calculatedSceneBounds.maxY) / 2;
      this.context.translate((transformedBottomRight.x - this.calculatedSceneBounds.maxX) / 2, offsetFromTop);

      this.checkIfNewLevelIsNeeded();

      this.draw();
      this.scaleChange.emit(this.context.getEffectiveScale());
    }
  };

  constructor(private elementRef: ElementRef) {
  }

  ngOnInit() {
    this.canvas = this.elementRef.nativeElement;
    this.context = this.canvas.getContext('2d') as MegazordContext;

    if (!this.sceneParamsInput) {
      return;
    }

    this.sceneParams = {
      sceneHeight: this.sceneParamsInput.sceneHeight,
      sceneWidth: this.sceneParamsInput.sceneWidth,
      numOfTilesX: this.sceneParamsInput.numOfTilesX,
      numOfTilesY: this.sceneParamsInput.numOfTilesY,
      tileWidth: this.sceneParamsInput.tileWidth,
      tileHeight: this.sceneParamsInput.tileHeight
    };

    this.initialMaxTagSize = this.sceneParams.sceneHeight / 20;
    this.maxTagSize = this.initialMaxTagSize * this.renderingConfig.tagSizingFactor * 2;
    this.initialMaxXDecoratorSize = this.sceneParams.sceneHeight / 21;
    this.maxXDecoratorSize = this.initialMaxXDecoratorSize * this.renderingConfig.tagSizingFactor * 2;
    this.initialMaxAnnotationDecoratorSize = this.sceneParams.sceneHeight / 20;
    this.maxAnnotationDecoratorSize = this.initialMaxAnnotationDecoratorSize * this.renderingConfig.tagSizingFactor * 2;

    this.api = this.apiInput || {};

    this.fitToScreen.subscribe(this.apiMethods.fitToScreen.bind(this));

    this.zoomEvent.subscribe((zoomScale) => {
      this.zoomOnCenter(zoomScale);
    });

    forEach(this.apiMethods, (method: () => void, methodName: string) => {
      this.api[methodName] = method;
    });

    this.fitToContainer(this.canvas);

    // Firefox
    if (/Firefox/i.test(navigator.userAgent)) {
      this.bindFunction(this.canvas, 'DOMMouseScroll', this.handleScroll);
    } else if (navigator.appName === 'Netscape') { // IE 11, Edge
      this.bindFunction(this.canvas, 'mousewheel', this.handleScroll);
    } else { // Chrome, Safari
      this.bindFunction(this.canvas, 'wheel', this.handleScroll);
    }

    this.canvasParent = document.getElementById('canvasParent');
    this.canvas.width = this.canvasParent.offsetWidth;
    this.canvas.height = this.canvasParent.offsetHeight;
    this.resizeCanvasDebounced = debounce(this.resizeCanvas);
    this.bindFunction(window, 'resize', this.resizeCanvasDebounced);

    this.canvasActualWidth = this.canvasParent.offsetWidth;
    this.canvasActualHeight = this.canvasParent.offsetHeight;

    this.context.setTransform(1, 0, 0, 1, 0, 0);

    this.initImageBuffers();

    this.initDraggableMechanism(this.canvasMove);

    this.debouncedNotifyMoveBegan = debounce(this.notifyMoveBegan, 300);

    this.debouncedNotifyMoveFinished = debounce(this.notifyMoveFinished, 300);

    this.trackTransforms(this.context);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.masks || !this.sceneParams) {
      return;
    }
    this.transformMasksToLineVectors();
    this.assignVectorsToAtoms(this.sceneParams.tileWidth, this.sceneParams.tileHeight,
        this.sceneParams.numOfTilesX, this.sceneParams.numOfTilesY);
    this.draw();
  }

  ngOnDestroy() {
    this.unbindFunction(this.canvas, 'wheel', this.handleScroll);
    this.unbindFunction(window, 'resize', this.resizeCanvasDebounced);
  }

  private bindFunction(elementToBindTo: any, eventType: string, fn: (e: Event) => void): EventListener {
    const boundFn = fn.bind(this);
    this.boundFunctions.push({
      boundElement: elementToBindTo,
      eventType,
      originalFn: fn,
      boundFn
    });
    elementToBindTo.addEventListener(eventType, boundFn);
    return boundFn;
  }

  private unbindFunction(boundElement: any, eventType: string, fn: (e: Event) => void) {
    const boundFunctionObj = find(this.boundFunctions, {
      originalFn: fn,
      boundElement,
      eventType
    });
    if (!boundFunctionObj) {
      return;
    }
    this.boundFunctions.splice(findIndex(this.boundFunctions, boundFunctionObj), 1);
    boundElement.removeEventListener(eventType, boundFunctionObj.boundFn);
    return boundFunctionObj.boundFn;
  }

  // Creates a double buffer for an atom. This means we manage two images for every atom
  // in order to able to display one and load the other at the same time. If we don't do this
  // (or another solution, there are several) we'll encounter flickers when changing a tile's
  // level.
  private createDoubleBufferImage(atom: any) {
    const renderHelper: RenderHelper = {
      firstBuffer: null,
      secondBuffer: null,
      isFirstBufferActive: null,
      completed: null,
      activeBuffer: null,
      backBuffer: null,
      whichBuffer: null,
      swapBuffers: null,
      changeImageLevel: null
    };

    renderHelper.firstBuffer = {image: new Image(), level: null};
    renderHelper.secondBuffer = {image: new Image(), level: null};
    renderHelper.isFirstBufferActive = true;
    renderHelper.completed = true;

    renderHelper.activeBuffer = renderHelper.firstBuffer;
    renderHelper.backBuffer = renderHelper.secondBuffer;

    renderHelper.swapBuffers = () => {
      atom.rendering.isFirstBufferActive = !atom.rendering.isFirstBufferActive;

      if (renderHelper.isFirstBufferActive) {
        renderHelper.activeBuffer = renderHelper.secondBuffer;
        renderHelper.backBuffer = renderHelper.firstBuffer;
      } else {
        renderHelper.activeBuffer = renderHelper.firstBuffer;
        renderHelper.backBuffer = renderHelper.secondBuffer;
      }
    };

    renderHelper.changeImageLevel = debounce((newLevel: number) => {
      renderHelper.completed = false;
      renderHelper.backBuffer.image.src = atom.probe.availableLevels[newLevel].s3ImagePath;
      renderHelper.backBuffer.level = newLevel;
    }, 0);

    atom.rendering = renderHelper;
  }

  private fitToContainer(canvas: HTMLCanvasElement) {
    // Make it visually fill the positioned parent
    canvas.style.width = '100%';
    canvas.style.height = '100%';

    // Then set the internal size to match
    canvas.width = canvas.offsetWidth;
    canvas.height = canvas.offsetHeight;
  }

  private canvasMove(event: any) {
    event.preventDefault();
    event.stopPropagation();

    this.debouncedNotifyMoveBegan();

    let xDelta = 0;
    let yDelta = 0;

    if (event.dragilble.singleMoveEventDeltaX) {
      xDelta = event.dragilble.singleMoveEventDeltaX;
    }

    if (event.dragilble.singleMoveEventDeltaY) {
      // yDelta = event.dragilble.movementDeltaY;
      yDelta = event.dragilble.singleMoveEventDeltaY;
    }

    if (xDelta === 0 && yDelta === 0) {
      return;
    }


    const curScaleX = this.context.getScaleX();
    const curScaleY = this.context.getScaleY();
    const translationDistX = xDelta / curScaleX;
    const translationDistY = yDelta / curScaleY;

    this.context.translate(translationDistX, translationDistY);

    this.draw();
  }

  private getModifiedDefaultDrawingStyle(color: string) {
    const modifiedDrawingStyle = cloneDeep(this.defaultLineDrawingStyle);
    modifiedDrawingStyle.color = color;

    return modifiedDrawingStyle;
  }

  private scaleUniformAroundTransformedPoint(scaleFactor: number, point: { [position: string]: number }) {
    const transformedPoint = this.context.transformedPoint(point.x, point.y);
    this.scaleAroundPoint(scaleFactor, scaleFactor, transformedPoint);
  }

  private scaleAroundPoint(scaleFactorX: number, scaleFactorY: number, point: { x: number, y: number }) {
    this.context.translate(point.x, point.y);
    this.context.scale(scaleFactorX, scaleFactorY);
    this.context.translate(-point.x, -point.y);
  }

  private validateScale(scale: number) {
    const curScale = this.context.getEffectiveScale();
    let scaleResult = scale;

    if (curScale * scale > this.zoomingBounds.maxScale) {
      scaleResult = this.zoomingBounds.maxScale / curScale;
    } else if (curScale * scale < this.zoomingBounds.minScale) {
      scaleResult = this.zoomingBounds.minScale / curScale;
    }

    return scaleResult;
  }

  private zoomOnCenter(zoomingScale: number) {
    const wrapperBounds = this.canvasParent.getBoundingClientRect(),
        wrapperWidth = wrapperBounds.width,
        wrapperHeight = wrapperBounds.height,
        wrapperCenterX = wrapperWidth / 2,
        wrapperCenterY = wrapperHeight / 2 + wrapperBounds.top;

    this.scaleUniformAroundTransformedPoint(zoomingScale, {x: wrapperCenterX, y: wrapperCenterY});

    this.finalizeZoom();
  }

  private zoom(clicks: number, zoomPoint: { x: number, y: number }) {
    let scaleFactor;

    scaleFactor = Math.pow(this.scaleFactorPerClick, clicks);
    scaleFactor = this.validateScale(scaleFactor);

    this.scaleUniformAroundTransformedPoint(scaleFactor, zoomPoint);

    this.finalizeZoom();
  }

  private finalizeZoom() {
    this.debouncedNotifyMoveFinished();
    this.checkIfNewLevelIsNeeded();
    this.scaleChange.emit(this.context.getEffectiveScale());
    this.draw();
  }

  private checkIfNewLevelIsNeeded() {
    const requiredLevel = this.getRequiredImageLevel(
        this.compound.molecules[0].atoms[0].probe.availableLevels, this.context.getEffectiveScale());

    if (this.currentImgLevel !== requiredLevel) {
      this.currentImgLevel = requiredLevel;
    }
  }

  private calcFrustum() {
    let transformedTopLeft;
    let transformedBottomRight;
    transformedTopLeft = this.context.transformedPoint(0, 0);
    transformedBottomRight = this.context.transformedPoint(this.canvas.offsetWidth, this.canvas.offsetHeight);

    this.frustum.left = transformedTopLeft.x;
    this.frustum.top = transformedTopLeft.y;

    this.frustum.right = transformedBottomRight.x;
    this.frustum.bottom = transformedBottomRight.y;
  }

  private handleScroll(event: WheelEvent) {
    this.debouncedNotifyMoveBegan();
    let delta = 0;
    let zoomX;
    let zoomY;

    zoomX = event.offsetX || (event.pageX - this.canvas.offsetLeft);
    zoomY = event.offsetY || (event.pageY - this.canvas.offsetTop);

    if (event.deltaY) {
      delta = -event.deltaY / 40;
    } else if (event.detail) {
      delta = -event.detail;
    }

    if (delta) {
      this.zoom(delta, {x: zoomX, y: zoomY});
    }

    event.preventDefault();
  }

  private touchZoom(event: TouchEvent) {
    let curDistBetweenTouches;
    let scaleFactor;
    let xDist;
    let yDist;
    const curCanvasOffset = (jquery(this.canvas)).offset();
    xDist = event.touches[0].pageX - event.touches[1].pageX;
    yDist = event.touches[0].pageY - event.touches[1].pageY;

    // Adding the canvas location in the screen to the calculation of the zooming point
    curCanvasOffset.top = curCanvasOffset.top || 0;
    curCanvasOffset.left = curCanvasOffset.left || 0;

    this.touchPointsCenter.x =
        (event.touches[0].pageX + event.touches[1].pageX) / 2 - curCanvasOffset.left;
    this.touchPointsCenter.y =
        (event.touches[0].pageY + event.touches[1].pageY) / 2 - curCanvasOffset.top;

    curDistBetweenTouches = Math.sqrt(Math.pow(xDist, 2) + Math.pow(yDist, 2));

    scaleFactor = curDistBetweenTouches / this.previousDistBetweenTouches;
    scaleFactor = this.validateScale(scaleFactor);

    this.scaleUniformAroundTransformedPoint(scaleFactor, this.touchPointsCenter);
    this.previousDistBetweenTouches = curDistBetweenTouches;

    this.finalizeZoom();
  }

  private initDraggableMechanism(onMove: (e: Event) => void, onDragEnd?: () => void) {
    let draggable = this.canvas;
    let x;
    let y;
    let oldX: number;
    let oldY: number;
    let lastMoveDeltaX = 0;
    let lastMoveDeltaY = 0;
    const unit = 'px';
    const events: { [functionName: string]: (e: Event) => void } = {
      dragEnd: null,
      dragMove: null,
      dragStart: null
    };
    let accumulatedScale;
    let movementDeltaX: number;
    let movementDeltaY: number;
    let doubleClickPrevent = false;
    let doubleClickTimer: number;

    const stay = false;

    const move = (event: any) => {

      if (event.touches && event.touches.length === 2) {
        this.touchZoom(event);
        return;
      }

      x = event.pageX || event.touches[0].pageX;
      y = event.pageY || event.touches[0].pageY;

      movementDeltaX = (x - oldX);
      movementDeltaY = (y - oldY);

      if (!stay) {
        draggable.style.left = movementDeltaX + unit;
        draggable.style.top = movementDeltaY + unit;
      }

      event.dragilble = {};
      event.dragilble.accumulatedMovementDeltaX = movementDeltaX;
      event.dragilble.accumulatedMovementDeltaY = movementDeltaY;

      event.dragilble.singleMoveEventDeltaX = movementDeltaX - lastMoveDeltaX;
      event.dragilble.singleMoveEventDeltaY = movementDeltaY - lastMoveDeltaY;

      lastMoveDeltaX = movementDeltaX;
      lastMoveDeltaY = movementDeltaY;

      events.dragMove(event);
    };

    const hasEffectivelyMovedTouch = () => {
      return Math.pow(movementDeltaX, 2) + Math.pow(movementDeltaY, 2) >
          5 / this.context.getEffectiveScale();
    };

    const hasEffectivelyMovedMouse = () => {
      return !(movementDeltaX < 2 && movementDeltaY < 2);
    };

    const drop = (event: any) => {
      this.unbindFunction(document, 'mousemove', move);
      this.unbindFunction(document, 'mouseup', drop);

      this.unbindFunction(document, 'touchmove', move);
      this.unbindFunction(document, 'touchend', drop);

      draggable = null;

      event.dragilble = {};
      event.dragilble.movementDeltaX = movementDeltaX;
      event.dragilble.movementDeltaY = movementDeltaY;

      events.dragEnd(event);

      movementDeltaX = 0;
      movementDeltaY = 0;

      lastMoveDeltaX = 0;
      lastMoveDeltaY = 0;
    };

    events.dragStart = (event: MouseEvent) => {
    };

    events.dragMove = ((event: MouseEvent) => {
      onMove.bind(this)(event);
    }).bind(this);

    events.dragEnd = ((event: MouseEvent) => {
      if (!isFunction(onDragEnd)) {
        return;
      }

      onDragEnd(event);
    }).bind(this);

    const determineTouchZoomOrDrag = (event: TouchEvent) => {
      if (event.type !== 'touchstart' || !event.touches) {
        return;
      }

      if (event.touches.length === 1) {
        event.stopPropagation();
        event.preventDefault();

        draggable = this.canvas;
        accumulatedScale = 1; // totalScale//megazordUtils.getScale(draggable, true);
        oldX = event.touches[0].pageX - draggable.offsetLeft;
        oldY = event.touches[0].pageY - draggable.offsetTop;
        // megazlog('x: ' + oldX + ', y: ' + oldY);
        this.bindFunction(document, 'touchmove', move);
        this.bindFunction(document, 'touchend', drop);

        events.dragStart(event);
      } else if (event.touches.length === 2) {

        const xDist = event.touches[0].pageX - event.touches[1].pageX;

        const yDist = event.touches[0].pageY - event.touches[1].pageY;

        this.touchPointsCenter.x = Math.abs(xDist);
        this.touchPointsCenter.y = Math.abs(yDist);
        this.previousDistBetweenTouches =
            Math.sqrt(Math.pow(xDist, 2) +
                Math.pow(yDist, 2));

        event.preventDefault();
      }

    };

    const drag = (event: any) => {
      if (this.isMaskingModeToggled) {
        return;
      }
      if (event.which === 1) {
        event.stopPropagation();
        draggable = this.canvas;
        accumulatedScale = 1; // totalScale//megazordUtils.getScale(draggable, true);
        oldX = event.pageX - 0;
        oldY = event.pageY - draggable.offsetTop;
        this.bindFunction(document, 'mousemove', move);
        this.bindFunction(document, 'mouseup', drop);

        events.dragStart(event);
      } else if (event.type === 'touchstart') {
        determineTouchZoomOrDrag(event);
      }
    };

    this.onClick = (e: MouseEvent) => {
      doubleClickTimer = window.setTimeout(() => {
        oldX = null;
        oldY = null;
        doubleClickPrevent = false;
      }, 200);
    };
    this.bindFunction(this.canvas, 'click', this.onClick);
    this.bindFunction(this.canvas, 'mousedown', drag);
    this.bindFunction(this.canvas, 'touchstart', drag);
  }


  private isLevelGoodEnough(optionalScale: number, currentScale: number) {
    const isScaleGoodEnough = currentScale <= optionalScale;
    return isScaleGoodEnough;
  }

  private getRequiredImageLevel(availableLevels: any[], currentScale: number) {

    let // By default set the worst level
        resultLevelIndex = availableLevels.length - 1;

    let foundSufficientLvl = false;

    // This code is based on the assumption that the levels are ordered in increasing quality.
    for (let levelIndex: number = availableLevels.length - 1; levelIndex >= 0; levelIndex--) {
      if (this.isLevelGoodEnough(availableLevels[levelIndex].scale, currentScale)) {
        resultLevelIndex = levelIndex;
        foundSufficientLvl = true;
        break;
      }
    }

    // If no good enough lvl was found, give the best we have (zero)
    if (!foundSufficientLvl) {
      resultLevelIndex = 0;
    }

    return resultLevelIndex;
  }

  private resizeCanvas() {
    let frustumTopLeft;

    let frustumBottomRight;

    this.canvasActualWidth = this.canvasParent.offsetWidth;
    this.canvasActualHeight = this.canvasParent.offsetHeight;

    frustumTopLeft = {
      x: -this.frustum.left,
      y: -this.frustum.top,
    };

    frustumBottomRight = {
      x: -this.frustum.right,
      y: -this.frustum.bottom,
    };

    this.canvas.width = this.canvasActualWidth;
    this.canvas.height = this.canvasActualHeight;

    this.moveFrustumTo(frustumTopLeft, frustumBottomRight);

    this.draw();
  }

  private draw() {
    if (!this.megazordCanvasInitiated) {
      return;
    }

    this.calcFrustum();
    this.context.save();

    // Use the identity matrix while clearing the canvas
    this.context.setTransform(1, 0, 0, 1, 0, 0);
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);

    // Restore the transform
    this.context.restore();

    this.renderImages();
  }

  private findPostClipSceneBounds() {
    const maxXPrefix = this.sceneParams.sceneWidth - this.sceneParams.tileWidth;
    const maxYPrefix = this.sceneParams.sceneHeight - this.sceneParams.tileHeight;
    const minXPrefix = this.sceneParams.tileWidth;
    const minYPrefix = this.sceneParams.tileHeight;
    let maxX;
    let maxY;
    let minX;
    let minY;
    let hasClippedAnyBounds = false;

    maxX = maxXPrefix;
    maxY = maxYPrefix;
    minX = minXPrefix;
    minY = minYPrefix;

    // Run on min X and min Y tiles, and on max X and max Y tiles and find the actual minX,
    // minY, maxX and maxY of the scene. We're checking inwards - we're looking for
    // places where the scene is smaller than what the sceneParams suggest - those are cases
    // of clipped black areas.
    for (let tileIndexX = 0; tileIndexX < this.sceneParams.numOfTilesX; tileIndexX++) {
      for (let tileIndexY = 0; tileIndexY < this.sceneParams.numOfTilesY; tileIndexY++) {
        const curAtom = this.findAtomByTileId(tileIndexX, tileIndexY);
        let alternativeMinX;
        let alternativeMinY;
        let alternativeMaxX;
        let alternativeMaxY;

        if (!curAtom || !curAtom.probe || !curAtom.probe.clipImagedPath ||
            curAtom.probe.clipImagedPath.length === 0) {
          continue;
        }

        // If we ever get here, mark that we're clipping parts of the scene
        hasClippedAnyBounds = true;

        if (tileIndexX === 0) {
          alternativeMinX = this.findMinInPath(curAtom.probe.clipImagedPath, 'x');

          if (alternativeMinX < minX) {
            minX = alternativeMinX;
          }
        }

        if (tileIndexY === 0) {
          alternativeMinY = this.findMinInPath(curAtom.probe.clipImagedPath, 'y');

          if (alternativeMinY < minY) {
            minY = alternativeMinY;
          }
        }

        if (tileIndexX === this.sceneParams.numOfTilesX - 1) {
          alternativeMaxX =
              this.findMaxInPath(curAtom.probe.clipImagedPath, 'x') + maxXPrefix;

          if (alternativeMaxX > maxX) {
            maxX = alternativeMaxX;
          }
        }

        if (tileIndexY === this.sceneParams.numOfTilesY - 1) {
          alternativeMaxY = this.findMaxInPath(curAtom.probe.clipImagedPath, 'y') +
              maxYPrefix;

          if (alternativeMaxY > maxY) {
            maxY = alternativeMaxY;
          }
        }
      }
    }

    // Reassigning the scene size parameters. If we're not clipping bounds, we have to calculate the size of
    // the scene differently.
    if (!hasClippedAnyBounds) {
      let helpVarX,
          helpVarY;

      helpVarX = maxX;
      helpVarY = maxY;

      maxX = minX;
      minX = helpVarX;
      maxY = minY;
      minY = helpVarY;
    }

    this.calculatedSceneBounds = {
      maxX,
      maxY,
      minX,
      minY
    };
  }

  private findMaxInPath(path: any, fieldName: string) {
    let max = path[0][fieldName];
    for (let pointIndex = 1; pointIndex < path.length; pointIndex++) {
      if (max < path[pointIndex][fieldName]) {
        max = path[pointIndex][fieldName];
      }
    }

    return max;
  }

  private findMinInPath(path: any, fieldName: string) {
    let min = path[0][fieldName];
    for (let pointIndex = 1; pointIndex < path.length; pointIndex++) {
      if (min > path[pointIndex][fieldName]) {
        min = path[pointIndex][fieldName];
      }
    }

    return min;
  }

  private isInFrustum(atom: any) {
    let isVisibleResult;

    isVisibleResult = !(atom.location.left > this.frustum.right ||
        atom.location.left + this.sceneParams.tileWidth < this.frustum.left ||
        atom.location.top > this.frustum.bottom ||
        atom.location.top + this.sceneParams.tileHeight < this.frustum.top);

    return isVisibleResult;
  }

  private clipPath(path: {x: number, y: number}[], offsetLeft: number, offsetTop: number) {
    if (!path) {
       return;
    }

    this.context.beginPath();
    this.context.moveTo(0, 0);
    forEach(path, (point) => {
      this.context.lineTo(offsetLeft + point.x, offsetTop + point.y);
    });
    this.context.closePath();
    this.context.clip();
  }

  private renderAtomLineVectors(atom: any) {
    const vectorsToRender = atom.rendering.lineVectors;

    if (vectorsToRender && this.displayBaysShelves) {
      for (let vectorIndex = 0; vectorIndex < vectorsToRender.length; vectorIndex++) {
        const vector = vectorsToRender[vectorIndex];
        let vectorDrawingStyle;

        const startPoint = {
          x: vector.vertices[0].x - this.lineWidth / 2,
          y: vector.vertices[0].y - this.lineWidth / 2
        };

        const endPoint = {
          x: vector.vertices[1].x - this.lineWidth / 2,
          y: vector.vertices[1].y - this.lineWidth / 2
        };

        this.context.save();

        vectorDrawingStyle = this.getVectorDrawingStyle(vector);

        this.context.beginPath();
        this.context.shadowColor = 'black';
        this.context.shadowBlur = 2;
        this.context.shadowOffsetX = 2;
        this.context.shadowOffsetY = 2;

        this.context.moveTo(startPoint.x - this.lineRenderOffset, startPoint.y - this.lineRenderOffset);
        this.context.lineTo(endPoint.x - this.lineRenderOffset, endPoint.y - this.lineRenderOffset);
        this.context.lineWidth = vectorDrawingStyle.lineWidth || this.lineWidth;

        this.context.strokeStyle = vectorDrawingStyle.color;
        this.context.setLineDash(vectorDrawingStyle.lineDash);

        this.context.stroke();
        this.context.moveTo(0, 0);

        this.context.restore();
      }
    }
  }

  private getVectorDrawingStyle(vector: any) {
    let lineDrawingStyle = this.defaultLineDrawingStyle;

    if (isFunction(vector.getDrawingStyle)) {
      lineDrawingStyle = vector.getDrawingStyle();
    }

    return lineDrawingStyle;
  }

  private renderSelectedMask() {
    if (!this.selectedMask) {
      return;
    }

    this.selectedMaskDeleteButtonDecorator.render();

    this.selectedMaskAnnotationButtonDecorator.render();

  }

  private renderImageInner(atom: any) {
    if (atom.rendering.activeBuffer &&
        atom.rendering.activeBuffer.image &&
        atom.rendering.activeBuffer.image.src) {
      this.context.save();

      this.clipPath(atom.probe.clipImagedPath, atom.location.left, atom.location.top);

      this.context.drawImage(atom.rendering.activeBuffer.image,
          atom.location.left,
          atom.location.top,
          this.sceneParams.tileWidth,
          this.sceneParams.tileHeight);
      this.context.restore();
    }

    this.renderAtomLineVectors(atom);
    this.renderSelectedMask();
  }

  private isFinishedRendering() {
    let curTile;
    let isFinishedRendering = true;

    for (let tileIndex = 0; tileIndex < this.tilesToRender.length; tileIndex++) {
      curTile = this.tilesToRender[tileIndex];

      if (!curTile.rendering.completed) {
        isFinishedRendering = false;
        break;
      }
    }

    return isFinishedRendering;
  }

  private renderImage(atom: any) {
    const shouldRender = this.isInFrustum(atom);

    if (shouldRender) {
      this.numOfTilesToRenderInThisCycle++;

      if (atom.rendering.activeBuffer &&
          atom.rendering.activeBuffer.level !== this.currentImgLevel) {
        this.updateImageLevel(atom, this.currentImgLevel);
      }

      this.renderImageInner(atom);
    }
  }

  private updateAtomImage(atom: any, newLevel: number) {
    atom.rendering.changeImageLevel(newLevel);
    atom.rendering.backBuffer.image.src = atom.probe.availableLevels[newLevel].s3ImagePath;
    atom.rendering.backBuffer.level = newLevel;
  }

  private updateImageLevel(atom: any, newLevel: number) {

    if (!get(atom, 'rendering.backBuffer.level')) {
      this.updateAtomImage(atom, newLevel);
    }

    // Check if the backbuffer already has the required level
    if (newLevel === atom.rendering.backBuffer.level) {
      atom.rendering.backBuffer.onload = () => {
        this.renderImageInner(atom);
      }
      atom.rendering.swapBuffers();
    } else {
      this.updateAtomImage(atom, newLevel);
    }

  }

  private findAtomByTileId(tileX: number, tileY: number) {
    const tileAtom = find(this.compound.molecules[0].atoms, (atom) => {
      return atom.probe.tileId.x === tileX && atom.probe.tileId.y === tileY;
    });

    return tileAtom;
  }

  private createRenderTiles() {
    // Create a matrix of tiles surrounded by one extra tile in every direction
    for (let tilesXIndex = -1; tilesXIndex < this.sceneParams.numOfTilesX + 1; tilesXIndex++) {
      for (let tilesYIndex = -1; tilesYIndex < this.sceneParams.numOfTilesY + 1; tilesYIndex++) {
        let tileAtom;
        // If a real tile exists in those coordinates
        if (tilesXIndex >= 0 && tilesXIndex < this.sceneParams.numOfTilesX &&
            tilesYIndex >= 0 && tilesYIndex < this.sceneParams.numOfTilesY) {

          tileAtom = this.findAtomByTileId(tilesXIndex, tilesYIndex);
        } else {
          tileAtom = {
            location: {
              left: tilesXIndex * this.sceneParams.tileWidth,
              top: tilesYIndex * this.sceneParams.tileHeight
            },
            probe: {
              tileId: {
                x: tilesXIndex,
                y: tilesYIndex
              }
            },
            rendering: {
              completed: true
            }
          };
        }

        this.tilesToRender.push(tileAtom);
      }
    }
  }

  private renderImages() {

    this.tilesFinishedRenderInThisCycle = 0;
    this.numOfTilesToRenderInThisCycle = 0;
    for (let tileIndex = 0; tileIndex < this.tilesToRender.length; tileIndex++) {
      this.renderImage(this.tilesToRender[tileIndex]);
    }

    this.renderSelectedMask();

    if (!this.isFinishedRendering()) {
      this.notifyRenderBegan();
    } else {
      this.scaleChange.emit(this.context.getEffectiveScale());
    }

    this.numOfTilesToRenderNow = this.numOfTilesToRenderInThisCycle;
  }

  private getScaleToFitToScreen() {
    let maxX = 0;
    let maxY = 0;
    let yScale;
    let xScale;
    let fitToScreenScale;

    forEach(this.compound.molecules, (molecule) => {
      forEach(molecule.atoms, (atom) => {
        const curAtomRight = atom.location.left + this.sceneParams.tileWidth;
        const curAtomBottom = atom.location.top + this.sceneParams.tileHeight;

        if (curAtomRight > maxX) {
          maxX = curAtomRight;
        }

        if (curAtomBottom > maxY) {
          maxY = curAtomBottom;
        }
      });
    });

    yScale = this.canvas.offsetHeight / maxY;
    xScale = this.canvas.offsetWidth / maxX;

    fitToScreenScale = yScale > xScale ? xScale : yScale;
    return fitToScreenScale;
  }

  private isDefined(field: number) {
    return field || field === 0;
  }

  private transformMasksToLineVectors() {
    if (!this.masks) {
      return;
    }

    if (!this.lineVectors) {
      this.lineVectors = [];
    }

    this.lineVectors = filter(this.lineVectors, (lineVector) => {
      return !lineVector.maskVector;
    });

    for (let maskIndex = 0; maskIndex < this.masks.length; maskIndex++) {
      const curMask = this.masks[maskIndex];
      if (curMask && curMask.topLeft && curMask.bottomRight &&
          this.isDefined(this.masks[maskIndex].topLeft.x) &&
          this.isDefined(this.masks[maskIndex].topLeft.y) &&
          this.isDefined(this.masks[maskIndex].bottomRight.x) &&
          this.isDefined(this.masks[maskIndex].bottomRight.y)) {
        curMask.vectors = [];

        curMask.vectors.push(
            {
              maskVector: true,
              getDrawingStyle: this.getMaskLineDrawingStyle,
              vertices: [
                {x: curMask.topLeft.x, y: curMask.topLeft.y},
                {x: curMask.bottomRight.x, y: curMask.topLeft.y}
              ]
            });

        curMask.vectors.push(
            {
              maskVector: true,
              getDrawingStyle: this.getMaskLineDrawingStyle,
              vertices: [
                {x: curMask.topLeft.x, y: curMask.bottomRight.y},
                {x: curMask.bottomRight.x, y: curMask.bottomRight.y}
              ]
            });

        curMask.vectors.push(
            {
              maskVector: true,
              getDrawingStyle: this.getMaskLineDrawingStyle,
              vertices: [
                {x: curMask.topLeft.x, y: curMask.topLeft.y},
                {x: curMask.topLeft.x, y: curMask.bottomRight.y}
              ]
            });

        curMask.vectors.push(
            {
              maskVector: true,
              getDrawingStyle: this.getMaskLineDrawingStyle,
              vertices: [
                {x: curMask.bottomRight.x, y: curMask.topLeft.y},
                {x: curMask.bottomRight.x, y: curMask.bottomRight.y}
              ]
            });

        for (let i = 0; i < curMask.vectors.length; i++) {
          this.lineVectors.push(curMask.vectors[i]);
        }
      }
    }
  }

  private assignVectorsToAtoms(tileWidth: number, tileHeight: number, numOfTilesX: number, numOfTilesY: number) {
    const tileBordersLineEquations: {m: number, n: number, mode: string}[] = [];
    let m: number;
    let n: number;

    if (!this.lineVectors) {
      return;
    }

    forEach(this.tilesToRender, (atom) => {
      atom.rendering.lineVectors = [];
    });

    for (let tileXIndex = -1; tileXIndex < numOfTilesX + 1; tileXIndex++) {
      m = 1;
      n = tileXIndex * tileWidth;
      tileBordersLineEquations.push({m, n, mode: 'vertical'});
    }

    for (let tileYIndex = -1; tileYIndex < numOfTilesY + 1; tileYIndex++) {
      m = 0;
      n = tileYIndex * tileHeight;
      tileBordersLineEquations.push({m, n, mode: 'horizontal'});
    }

    forEach(this.lineVectors, (vector: any) => {
      const intersections = [];
      let isHorizontal;
      let isVertical;
      let vertices;
      let higherX;
      let lowerX;
      let higherY;
      let lowerY;
      const drawingStyle = this.getAppropriateDrawingStyle(vector);

      vertices = vector.vertices;
      higherX = vertices[0].x;
      lowerX = vertices[1].x;
      higherY = vertices[0].y;
      lowerY = vertices[1].y;

      if (vertices[1].x > vertices[0].x) {
        higherX = vertices[1].x;
        lowerX = vertices[0].x;
      }

      if (vertices[1].y > vertices[0].y) {
        higherY = vertices[1].y;
        lowerY = vertices[0].y;
      }

      if (vertices[1].x === vertices[0].x) {
        isVertical = true;
        n = vertices[0].x;
      } else if (vertices[1].y === vertices[0].y) {
        isHorizontal = true;
        n = vertices[0].y;
      } else {
        m = (vertices[1].y - vertices[0].y) / (vertices[1].x - vertices[0].x);
        n = vertices[0].y - (m * vertices[0].x);
      }


      for (let equationIndex = 0; equationIndex < tileBordersLineEquations.length;
           equationIndex++) {
        let curEquation;
        let xResult;
        let yResult;
        curEquation = tileBordersLineEquations[equationIndex];

        if (isHorizontal && curEquation.mode === 'horizontal' ||
            isVertical && curEquation.mode === 'vertical') {
          continue;
        }

        if (isHorizontal && curEquation.mode === 'vertical') {
          xResult = curEquation.n;
          yResult = n;
        } else if (isVertical && curEquation.mode === 'horizontal') {
          xResult = n;
          yResult = curEquation.n;
        } else {
          if (curEquation.mode === 'vertical') {
            yResult = m * curEquation.n + n;
            xResult = (yResult - n) / m;
          } else { // Means the equation is horizontal
            xResult = (curEquation.n - n) / m;
            yResult = m * xResult + n;
          }

        }

        if (xResult > higherX ||
            yResult > higherY ||
            xResult < lowerX ||
            yResult < lowerY) {
          continue;
        }

        let existsAlready = false;

        // let existsAlready = _.find(intersections, function (point) {
        //     return point.x === Math.round(xResult) && point.y === Math.round(yResult);
        // });

        // This if check is only necessary because the previous code is flawed. Will
        // require improvement.
        for (let pointCheckIndex = 0; pointCheckIndex < intersections.length;
             pointCheckIndex++) {
          const curPoint = intersections[pointCheckIndex];

          if (curPoint.x === Math.round(xResult) &&
              curPoint.y === Math.round(yResult)) {
            existsAlready = true;
            break;
          }
        }

        if (!existsAlready) {
          intersections.push({x: Math.round(xResult), y: Math.round(yResult)});
        }

        // // If the two lines intersect within the scene, add them to the list
        // if (xResult >= 0 && xResult <= tileWidth * numOfTilesX &&
        //     yResult >= 0 && yResult <= numOfTilesY * tileHeight) {
        //     intersections.push({ x: xResult, y: yResult });
        //
        // }

        // intersections.sort(sortByDistance);
      }

      // Add the beginning and end points of the vector to the intersections list
      intersections.push({x: Math.round(vertices[0].x), y: Math.round(vertices[0].y)});
      intersections.push({x: Math.round(vertices[1].x), y: Math.round(vertices[1].y)});

      intersections.sort(this.sortByDistance);

      // So far, for every line vector we've found its every intersection with the tile
      // boundaries and sorted the intersections according to distance from the beginning
      // of the line. Now we're running on those points according to the order we now have
      // and from each two followings points, create a sub-line. After that we'll be able
      // to assign each such sub-line to a tile (we basically cut the line vector using the
      // tile boundaries and receive a bunch of clipped lines so we'll be able to render
      // them tile by tile.
      let curStartPoint;

      let curEndPoint;
      for (let intersectionIndex = 1; intersectionIndex < intersections.length;
           intersectionIndex++) {
        curStartPoint = intersections[intersectionIndex - 1];
        curEndPoint = intersections[intersectionIndex];
        const tileX = Math.floor(((curStartPoint.x + curEndPoint.x) / 2) / tileWidth);
        const tileY = Math.floor(((curStartPoint.y + curEndPoint.y) / 2) / tileHeight);

        for (let atomIndex = 0; atomIndex < this.tilesToRender.length;
             atomIndex++) {
          const curAtom = this.tilesToRender[atomIndex]; // scope.compound.molecules[0].atoms[atomIndex];
          if (curAtom.probe.tileId.x === tileX &&
              curAtom.probe.tileId.y === tileY) {

            if (!curAtom.rendering.lineVectors) {
              curAtom.rendering.lineVectors = [];
            }

            curAtom.rendering.lineVectors
                .push({
                  vertices: [{x: curStartPoint.x, y: curStartPoint.y},
                    {x: curEndPoint.x, y: curEndPoint.y}],
                  getDrawingStyle: drawingStyle
                });
            break;
          }
        }
      }

      // Line Equation: y = mx + n
      // vectorLineEquations.push({ m : m, n: n });
    });
  }

  private getAppropriateDrawingStyle(vector: any) {
    let drawingStyle = null;
    if (vector.getDrawingStyle) {
      drawingStyle = vector.getDrawingStyle;
    } else if (vector.color) {
      drawingStyle = this.getModifiedDefaultDrawingStyle(vector.color);
    }

    return drawingStyle;
  }

  private sortByDistance(firstItem: { x: number, y: number }, secondItem: { x: number, y: number }) {
    let firstItemDistToOirginSqaured;
    let secondItemDistToOirginSquared;

    firstItemDistToOirginSqaured = Math.pow(firstItem.x, 2) + Math.pow(firstItem.y, 2);
    secondItemDistToOirginSquared = Math.pow(secondItem.x, 2) + Math.pow(secondItem.y, 2);

    // gil's comment: maybe I mixed the order
    return secondItemDistToOirginSquared - firstItemDistToOirginSqaured;
  }

  // Receives two points in the image coordinate system and moves the frustum there.
  public moveFrustumTo(topLeft: { x: number, y: number }, bottomRight: { x: number, y: number }) {
    let yDiff;

    let xDiff;
    let scaleFactor;

    yDiff = Math.abs(bottomRight.y - topLeft.y);
    xDiff = Math.abs(bottomRight.x - topLeft.x);
    if (yDiff > xDiff) {
      scaleFactor = this.canvasActualHeight / yDiff;
    } else {
      scaleFactor = this.canvasActualWidth / xDiff;
    }
    // Reset current transformation matrix to the identity matrix
    this.context.setTransform(1, 0, 0, 1, 0, 0);
    this.context.scale(scaleFactor, scaleFactor);
    // moving the canvas and its origin x units horizontally and y units vertically
    // x: Positive values are to the right, and negative to the left.
    // y:  Positive values are down, and negative are up.
    this.context.translate(topLeft.x, topLeft.y);
    this.finalizeZoom();
  }

  private notifyRenderBegan() {
    this.renderBeganEvent.emit();
  }

  private notifyMoveBegan() {
    this.moveBeganEvent.emit();
  }

  private notifyMoveFinished() {
    this.moveFinishedEvent.emit();
  }

  private notifyRenderFinished() {
    this.renderFinishedEvent.emit();
  }

  private updateLoadingCounter() {
    const finishedRendering = this.isFinishedRendering();

    if (finishedRendering) {
      this.notifyRenderFinished();
    }
  }

  private initImageBuffers() {
    let initialScale;

    this.tilesFinishedRenderInThisCycle = 0;
    this.numOfTilesToRenderNow = this.sceneParams.numOfTilesX * this.sceneParams.numOfTilesY;

    this.notifyRenderBegan();

    this.displayTypeIconImg = new Image();
    this.tagIconImg = new Image();
    this.pricedTagIconImg = new Image();
    this.highlightedTagIconImg = new Image();
    this.highlightedPriceTagIconImg = new Image();
    this.highlightedPriceDecoratorIconImg = new Image();
    this.xDecoratorIconImg = new Image();
    this.pricingDecoratorIconImg = new Image();

    this.tagColorsImg = {
      red: new Image(),
      yellow: new Image(),
      green: new Image(),
      grey: new Image()
    };

    this.tagColorsImg.red.src = this.redTagImageSrc;
    this.tagColorsImg.green.src = this.greenTagImageSrc;
    this.displayTypeIconImg.src = this.displayTagImageSrc;
    this.xDecoratorIconImg.src = this.xDecoratorImageSrc;
    this.pricingDecoratorIconImg.src = this.pricingDecoratorImageSrc;
    this.tagIconImg.src = this.tagImageSrc;
    this.pricedTagIconImg.src = this.pricedTagImageSrc;
    this.highlightedTagIconImg.src = this.highlightedTagImageSrc;
    this.highlightedPriceDecoratorIconImg.src = this.highlightedPriceDecoratorImageSrc;

    this.createRenderTiles();
    this.findPostClipSceneBounds();

    initialScale = this.getScaleToFitToScreen();

    this.zoomingBounds.minScale = initialScale / 8;
    this.zoomingBounds.maxScale = 8;

    this.currentImgLevel = this.getRequiredImageLevel(
        this.compound.molecules[0].atoms[0].probe.availableLevels, initialScale);


    forEach(this.compound.molecules, (molecule) => {
      forEach(molecule.atoms, (atom) => {
        this.createDoubleBufferImage(atom);
        this.renderImageInner(atom);

        atom.rendering.activeBuffer.image.onload = () => {

          this.imageLoadedEvent.emit();
          this.updateLoadingCounter();

          atom.rendering.activeBuffer.image.onload = null;

          atom.rendering.activeBuffer.image.onload = () => {
            atom.rendering.completed = true;

            atom.rendering.swapBuffers();
            this.renderImageInner(atom);

            this.updateLoadingCounter();
          };
          atom.rendering.backBuffer.image.onload = () => {
            atom.rendering.completed = true;

            atom.rendering.swapBuffers();
            this.renderImageInner(atom);

            this.updateLoadingCounter();
          };

          const onTagImgLoad = () => {
            this.api.fitToScreen();

            if (this.firstTimeLoadingCallback &&
                typeof this.firstTimeLoadingCallback === 'function') {
              this.firstTimeLoadingCallback();
            }

          };

          this.tilesFinishedRenderInThisCycle++;
          if (this.numOfTilesToRenderNow && this.tilesFinishedRenderInThisCycle === this.numOfTilesToRenderNow) {
            this.megazordCanvasInitiated = true;
            this.tagIconImg.onload = onTagImgLoad;
            if (this.tagIconImg.complete) {
              onTagImgLoad();
            }
          }
        };

        atom.rendering.activeBuffer.image.src =
            atom.probe.availableLevels[this.currentImgLevel].s3ImagePath;
        atom.rendering.activeBuffer.level = this.currentImgLevel;
      });
    });

    this.transformMasksToLineVectors();
    // assignVectorsToAtoms(4096, 4096, 8, 2);
    this.assignVectorsToAtoms(this.sceneParams.tileWidth, this.sceneParams.tileHeight,
        this.sceneParams.numOfTilesX, this.sceneParams.numOfTilesY);

  }

  // Stole this function from the internet. It allows folowing the state of the canvas
  // transform. We can accomplish this by using the megazord as well, but it appears canvas only
  // uses Affine Transforms (3x3, only 6 coords of which differ from thw identity matrix.
  // Morpheus uses 4x4 which is redundant here and adds room for bugs because it differs further
  // from the canvas state we're trying to replicate).
  private trackTransforms(ctx: any) {
    const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    let xform = svg.createSVGMatrix();
    ctx.getTransform = ()  => {
      return xform;
    };

    ctx.getScaleX = () => {
      return xform.a;
    };

    ctx.getScaleY = () => {
      return xform.d;
    };

    ctx.getEffectiveScale = () => {
      const xScaleToOneRatioAbs = xform.a > 0 ? xform.a : 1 / xform.a;
      const yScaleToOneRatioAbs = xform.d > 0 ? xform.d : 1 / xform.d;
      let effectiveScale = xform.a;

      if (yScaleToOneRatioAbs > xScaleToOneRatioAbs) {
        effectiveScale = xform.d;
      }

      return effectiveScale;
    };

    const savedTransforms: SVGMatrix[] = [];
    const save = ctx.save;
    ctx.save = () => {
      savedTransforms.push(xform.translate(0, 0));
      return save.call(ctx);
    };
    const restore = ctx.restore;
    ctx.restore = () => {
      xform = savedTransforms.pop();
      return restore.call(ctx);
    };

    const scale = ctx.scale;
    ctx.scale = (sx: number, sy: number) => {
      xform = xform.scaleNonUniform(sx, sy);
      return scale.call(ctx, sx, sy);
    };
    const rotate = ctx.rotate;
    ctx.rotate = (radians: number) => {
      xform = xform.rotate(radians * 180 / Math.PI);
      return rotate.call(ctx, radians);
    };
    const translate = ctx.translate;
    ctx.translate = (dx: number, dy: number) => {
      xform = xform.translate(dx, dy);
      return translate.call(ctx, dx, dy);
    };
    const transform = ctx.transform;
    ctx.transform = (a: number, b: number, c: number, d: number, e: number, f: number) => {
      const m2 = svg.createSVGMatrix();
      m2.a = a;
      m2.b = b;
      m2.c = c;
      m2.d = d;
      m2.e = e;
      m2.f = f;
      xform = xform.multiply(m2);
      return transform.call(ctx, a, b, c, d, e, f);
    };
    const setTransform = ctx.setTransform;
    ctx.setTransform = (a: number, b: number, c: number, d: number, e: number, f: number) => {
      xform.a = a;
      xform.b = b;
      xform.c = c;
      xform.d = d;
      xform.e = e;
      xform.f = f;
      return setTransform.call(ctx, a, b, c, d, e, f);
    };
    const pt = svg.createSVGPoint();
    ctx.transformedPoint = (x: number, y: number) => {
      pt.x = x;
      pt.y = y;
      return pt.matrixTransform(xform.inverse());
    };

    const inversePt = svg.createSVGPoint();
    ctx.inverseTransformedPoint = (x: number, y: number) => {
      inversePt.x = x;
      inversePt.y = y;
      return inversePt.matrixTransform(xform);
    };
  }
}
