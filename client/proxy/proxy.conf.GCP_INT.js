const debugEnv = require('../src/environments/environment.GCP_INT.DEBUG');

const proxyConfig = [
  {
    "context": ["/approvals/api/**"],
    "target": "http://localhost:8989",
    "secure": false,
    "logLevel": "debug",
    "changeOrigin": true
  },
  {
    "context": ["/api/**"],
    "target": "http://localhost:8081",
    "secure": true,
    "logLevel": "debug",
    "changeOrigin": false
  },
  {
    "context": ["/login", "/styles/styles.css", "/scripts/bundle.js", "/scripts/templates.js", "/scripts/templatesShared.js",
      "/manifest/manifest.html", "/manifest/manifest.appcache", "/languages/en_US.json"],
    "target": "http://localhost:8081",
    "secure": true,
    "logLevel": "debug",
    "changeOrigin": true
  },
  {
    "context": ["/approvals/assets/environments/environment.json"],
    bypass: (req, res, proxyOptions) => {
      console.log(req.url);
      if (req.url !== '/approvals/assets/environments/environment.json') {
        // I don't know why those requests get here..
        return;
      }
      console.log('here');
      res.end(JSON.stringify(debugEnv));
      return true;
    },
    "secure": true,
    "logLevel": "debug",
    "changeOrigin": true
  }
];

module.exports = proxyConfig;