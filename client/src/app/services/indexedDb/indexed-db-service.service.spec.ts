import {TestBed} from '@angular/core/testing';

import {IndexedDbService} from './indexed-db-service.service';

describe('IndexedDbServiceService', () => {
    let service;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 20000;
        service = TestBed.get(IndexedDbService);
    });

    afterEach(() => {
        service.deleteDB();
    });

    it('should createDB', async () => {
        await service.createDB(['projects']);
        await service.add('projects', 'projectName', {o: 1});
        const res = await service.read('projects', 'projectName');
        expect(res).toEqual({o: 1});
        const isExists = await service.checkIfDBExists();
        expect(isExists).toBe(true);
    });
});
