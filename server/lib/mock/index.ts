import * as path from 'path';
const nc = require('@trax-retail/node-core')('smart-approval');
import { mergeWith } from 'lodash';
const env = process.env.TRAX_ENV || 'DEV';
const config = mergeWith(require(`../../config/base.json`), require(`../../config/${env}.json`));
import {SmartAuthService} from '@trax-retail/smart-auth';




SmartAuthService.init({mongoConfig: config.mongo.users, corsAllowedOrigins: config.corsAllowedOrigins}).then( _ => {
    nc.init(path.resolve('./'), process.env.TRAX_ENV || 'DEV');
    require('./prepare').run().then(() => process.exit())
});
