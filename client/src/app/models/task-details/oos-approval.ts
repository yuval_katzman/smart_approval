import {YesNoQuestion} from "./interface";

export class OOSApprovalYesNo implements YesNoQuestion {
    constructor(public productName: string) {}
    readonly questionText = `Can you see ${this.productName} on shelf?`;
    // It is must, but not implemented in oos
    readonly noAnswer: string;
    readonly noValueAnswer: string;
    readonly returnValue: boolean;
    readonly valuePlaceHolder: string;
    readonly yesAnswer: string;
}
