import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {YesNo} from "../../../models/task";
import {fromEvent, Subscription} from "rxjs";

@Component({
  selector: 'app-yes-no',
  templateUrl: './yes-no.component.html',
  styleUrls: ['./yes-no.component.scss']
})
export class YesNoComponent implements OnInit, OnDestroy {
  subscription: Subscription;
  @Output() onAnswer = new EventEmitter<YesNo>();
  @Input() answer: YesNo;
  YesNo = YesNo;
  constructor() { }
  ngOnInit() {
    this.subscription = fromEvent(document, 'keypress').subscribe((e: KeyboardEvent) => {
      if (e.key === "y") {
        this.answer = YesNo.YES;
      }

      if (e.key === "n") {
        this.answer = YesNo.NO;
      }

      this.onAnswer.emit(this.answer);
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
