
export interface SaveTask {
    templateId?:number,
    templateGroup?:string,
    traxCategoryUuid?:string,
    originalTask:any,
    netDurationInSeconds: number,
    queueDurationInSeconds: number,
    wave_type: string,
    wave_uid: string,
    project_name: string,
    job_type: string,
    session_uid: string,
    scene_uid: string,
    scene_id: number,
    persistence: any,
    approval_id:number,
    task_name:string,
    answers: {
        question_uid: string,
        product_fk: number,
        question_category: string,
        question_type: string,
        answer: any,
        /// duration in millisecond.
        duration:number
    }[]
}

export enum TaskType {
    oosApproval = "oos_approval",
    approvalVoting = "approval_voting",
    approvalsVoting = "approvals_voting",
    approvals = "approvals"
}

export enum EntityType{
    scene = 'scene',
    probe = 'probe'
}
