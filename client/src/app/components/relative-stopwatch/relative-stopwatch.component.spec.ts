import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {RelativeStopwatchComponent} from './relative-stopwatch.component';
import set = Reflect.set;
import {Component} from '@angular/core';

@Component({
    selector: 'test-component-wrapper',
    template: '<app-relative-stopwatch [restartOn]="restartOn" [stopper]="stopper"></app-relative-stopwatch>'
})
class TestComponentWrapper {
    restartOn = false;
    stopper = false;
}

describe('RelativeStopwatchComponent', () => {
    let wrapperComponent: TestComponentWrapper;
    let component: RelativeStopwatchComponent;
    let fixture: ComponentFixture<TestComponentWrapper>;

    beforeEach(async(() => {

        TestBed.configureTestingModule({
            declarations: [
                TestComponentWrapper,
                RelativeStopwatchComponent
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TestComponentWrapper);
        wrapperComponent = fixture.componentInstance;
        component = fixture.debugElement.children[0].componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    describe('restart', () => {
        it('should be called when restartOn input changes to a true', async () => {
            // @ts-ignore
            const watch = component.watch;
            wrapperComponent.restartOn = false;
            fixture.detectChanges();
            const restartSpy = spyOn<any>(watch, 'restart').and.callThrough();

            wrapperComponent.restartOn = true;
            fixture.detectChanges();

            expect(restartSpy).toHaveBeenCalled();
        });

        it('should not be called when restartOn input changes to a false', async () => {
            // @ts-ignore
            const watch = component.watch;
            wrapperComponent.restartOn = true;
            fixture.detectChanges();
            const restartSpy = spyOn<any>(watch, 'restart').and.callThrough();

            wrapperComponent.restartOn = false;
            fixture.detectChanges();

            expect(restartSpy).not.toHaveBeenCalled();
        });

        it('should not be called when restartOn input is true and does not change', async () => {
            // @ts-ignore
            const watch = component.watch;
            wrapperComponent.restartOn = true;
            fixture.detectChanges();
            const restartSpy = spyOn<any>(watch, 'restart').and.callThrough();

            fixture.detectChanges();

            expect(restartSpy).not.toHaveBeenCalled();
        });

        it('should not be called when restartOn input is false and does not change', async () => {
            // @ts-ignore
            const watch = component.watch;
            wrapperComponent.restartOn = false;
            fixture.detectChanges();
            const restartSpy = spyOn<any>(watch, 'restart').and.callThrough();

            fixture.detectChanges();

            expect(restartSpy).not.toHaveBeenCalled();
        });
    });

    describe('stop', () => {
        it('should be called when stopper input changes to a false', async () => {
            // @ts-ignore
            const watch = component.watch;
            wrapperComponent.stopper = false;
            fixture.detectChanges();
            const stopSpy = spyOn<any>(watch, 'stop').and.callThrough();

            wrapperComponent.stopper = true;
            fixture.detectChanges();

            expect(stopSpy).not.toHaveBeenCalled();
        });

        it('should not be called when stopper input changes to a true', async () => {
            // @ts-ignore
            const watch = component.watch;
            wrapperComponent.stopper = true;
            fixture.detectChanges();
            const stopSpy = spyOn<any>(watch, 'stop').and.callThrough();

            wrapperComponent.stopper = false;
            fixture.detectChanges();

            expect(stopSpy).toHaveBeenCalled();
        });

        it('should not be called when stopper input is true and does not change', async () => {
            // @ts-ignore
            const watch = component.watch;
            wrapperComponent.stopper = true;
            fixture.detectChanges();
            const stopSpy = spyOn<any>(watch, 'stop').and.callThrough();

            fixture.detectChanges();

            expect(stopSpy).not.toHaveBeenCalled();
        });

        it('should not be called when stopper input is false and does not change', async () => {
            // @ts-ignore
            const watch = component.watch;
            wrapperComponent.stopper = false;
            fixture.detectChanges();
            const stopSpy = spyOn<any>(watch, 'stop').and.callThrough();

            fixture.detectChanges();

            expect(stopSpy).not.toHaveBeenCalled();
        });
    });

    describe('start', () => {
        it('should be called when stopper input changes to a true', async () => {
            // @ts-ignore
            const watch = component.watch;
            wrapperComponent.stopper = false;
            fixture.detectChanges();
            const startSpy = spyOn<any>(watch, 'start').and.callThrough();

            wrapperComponent.stopper = true;
            fixture.detectChanges();

            expect(startSpy).toHaveBeenCalled();
        });

        it('should not be called when stopper input changes to a false', async () => {
            // @ts-ignore
            const watch = component.watch;
            wrapperComponent.stopper = true;
            fixture.detectChanges();
            const startSpy = spyOn<any>(watch, 'start').and.callThrough();

            wrapperComponent.stopper = false;
            fixture.detectChanges();

            expect(startSpy).not.toHaveBeenCalled();
        });

        it('should not be called when stopper input is true and does not change', async () => {
            // @ts-ignore
            const watch = component.watch;
            wrapperComponent.stopper = true;
            fixture.detectChanges();
            const startSpy = spyOn<any>(watch, 'start').and.callThrough();

            fixture.detectChanges();

            expect(startSpy).not.toHaveBeenCalled();
        });

        it('should not be called when stopper input is false and does not change', async () => {
            // @ts-ignore
            const watch = component.watch;
            wrapperComponent.stopper = false;
            fixture.detectChanges();
            const startSpy = spyOn<any>(watch, 'start').and.callThrough();

            fixture.detectChanges();

            expect(startSpy).not.toHaveBeenCalled();
        });
    });
});
