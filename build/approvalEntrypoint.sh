#!/usr/bin/env bash

TRAX_ENV=$TRAX_ENV
TRAX_DEBUG="false"

echo "environment: $TRAX_ENV"
echo "debug: $TRAX_DEBUG"

cp /usr/src/app/client/dist/approvals/assets/environments/environment.${TRAX_ENV}.json /usr/src/app/client/dist/approvals/assets/environments/environment.json

sed -i -e "s/<meta name=\"environment\" content=\"[^>]\+\">/<meta name=\"environment\" content=\"${TRAX_ENV}\">/g" /usr/src/app/client/dist/index.html
sed -i -e "s/<meta name=\"debug\" content=\"[^>]\+\">/<meta name=\"debug\" content=\"${TRAX_DEBUG}\">/g" /usr/src/app/client/dist/index.html

cd /usr/src/app/server && npm run start-deployed
