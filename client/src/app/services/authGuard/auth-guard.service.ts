import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {UserService} from '../user/user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private userService: UserService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
      Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if (!this.userService.isAuthenticated()) {
      this.userService.logout();
      return false;
    }

    return new Observable((observer) => {
      this.userService.isAuthorized(route.params.projectName, route.data.requiredPermissions).subscribe((authorized) => {
        if (authorized) {
          observer.next(true);
        } else {
          this.userService.sendToAccessDeniedPage();
          observer.next(false);
        }
      });
    });
  }
}
