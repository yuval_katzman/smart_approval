import {forEach} from "lodash";
import {Atom} from "./atom";
import {Morpheus} from "./morpheus";
import {mat4} from "gl-matrix";

// tslint:disable-next-line:class-name
class MOLECULE_MODEL_CONSTS {
    static defaultSpaceBetweenProbes = 50;
}

export class Molecule {
    atoms: Atom[];
    index: number;
    addAtom: (atom: Atom) => void;
    removeAtom: (atomIndex: number) => void;
    transformation: mat4;

    constructor(moleculeData, compoundData) {

        this.atoms = [];
        this.index = moleculeData.index;

        this.addAtom = atom => {
            this.atoms.push(atom);
        };

        this.removeAtom = atomIndex => {
            this.atoms.splice(atomIndex, 1);
        };

        this.transformation = Morpheus.unit();

        forEach(moleculeData.data, atomData => {
            const newAtom = new Atom(atomData, compoundData, compoundData.isTiled);
            this.addAtom(newAtom);
        });

        /**
         * Fixes bug - the scene viewer gets non transformed images so they get a unit matrix and
         * they hide behind each other
         * In case it is an untouched scene than move the images from left to right
         */
        if (this.isSceneNotTransformed(this)) {
            let currentImageX = 0;
            forEach(this.atoms, (atom, index) => {
                atom.transformation = Morpheus.moveTo(atom.transformation, currentImageX, 0);
                currentImageX += moleculeData.data[index].width + MOLECULE_MODEL_CONSTS.defaultSpaceBetweenProbes;
            });
        }
    }

    private isSceneNotTransformed(molecule) {
        let isMoleculeNotTransformed = true;

        if (molecule.atoms.length <= 1) {
            return false;
        }
        forEach(molecule.atoms, atom => {
            if (!Morpheus.isUnit(atom.transformation)) {
                isMoleculeNotTransformed = false;
                return;
            }
        });
        return isMoleculeNotTransformed;
    }
}

