import {NextFunction, Response} from 'express';
import {AuthenticatedRequest} from '../defs';
import {SmartAuthService} from '@trax-retail/smart-auth';
import {Logic} from '../logic/logic';
import {ROLES_PER_PROJECT} from "@trax-retail/smart-auth/dist/interfaces";

const logic = new Logic();
const nc = require('@trax-retail/node-core')('smart-approval');
nc.use(SmartAuthService.initAuthRoutes());

const ensureAuthenticated = SmartAuthService.ensureAuthenticated({roles: [ROLES_PER_PROJECT.APPROVER]});

nc.get(
    `${nc.config.express.apiPrefix}/:projectName/queue-count`,
    ensureAuthenticated,
    async (req: AuthenticatedRequest, res: Response, next: NextFunction) => {
        const qCount = await logic.getQueueCount(req.user,
            req.params.projectName,
            req.query.templateId ? +req.query.templateId : undefined,
            req.query.templateGroup as string,
            req.query.subQueue as string,
            req.query.traxCategoryUuid as string,
        );
        res.send({count:qCount});
    },
);


nc.get(
    `${nc.config.express.apiPrefix}/:projectName/template`,
    ensureAuthenticated,
    async (req: AuthenticatedRequest, res: Response, next: NextFunction) => {
        const template = await logic.getTemplateById(req.params.projectName, +req.query.id);
        res.send(template);
    },
);

nc.get(
    `${nc.config.express.apiPrefix}/:projectName/next-task`,
    ensureAuthenticated,
    logic.resetCookie,
    async (req: AuthenticatedRequest, res: Response, next: NextFunction) => {
        const task = await logic.getNextTask(req.user,
            req.params.projectName,
            req.query.templateId ? +req.query.templateId : undefined,
            req.query.templateGroup as string,
            req.query.subQueue as string,
            req.query.traxCategoryUuid as string);
        res.send(task);
    },
);

nc.put(
    `${nc.config.express.apiPrefix}/:projectName/cancel-task`,
    ensureAuthenticated,
    logic.resetCookie,
    async (req: AuthenticatedRequest, res: Response, next: NextFunction) => {
        const response = await logic.cancelTaskAndLog(
            req.user,
            req.sessionID,
            req.params.projectName,
            req.query.selectorTaskId as string,
            req.body.sceneId,
            req.body.reason,
            req.body.taskType
        );
        res.send(response);
    },
);

nc.put(
    `${nc.config.express.apiPrefix}/:projectName/extend-lock`,
    ensureAuthenticated,
    logic.resetCookie,
    async (req: AuthenticatedRequest, res: Response, next: NextFunction) => {
        const response = await logic.extendLockAndStuff(req.user, req.params.projectName, req.query.selectorTaskId as string);
        res.send(response);
    },
);

nc.post(
    `${nc.config.express.apiPrefix}/:projectName/save-task`,
    ensureAuthenticated,
    logic.resetCookie,
    async (req: AuthenticatedRequest, res: Response, next: NextFunction) => {
        const response = await logic.saveTask(req.user, req.sessionID, req.params.projectName, req.query.selectorTaskId as string, req.body);
        res.send(response);
    },
);

nc.post(
    `${nc.config.express.apiPrefix}/:projectName/break-task`,
    ensureAuthenticated,
    logic.resetCookie,
    async (req: AuthenticatedRequest, res: Response, next: NextFunction) => {
        logic.breakTask(req.user, req.body.reason).then()
        res.send({});
    },
);

