import { Injectable } from '@angular/core';
import {forEach} from "lodash";
import {MatDialog} from "@angular/material";
import {InactivityTimerComponent} from "../../components/inactivity-timer/inactivity-timer.component";

@Injectable({
  providedIn: 'root'
})
export class InactivityTimerService {
  private running: boolean;
  private timeout: number | null;
  private TIMEOUT = 90 * 1000;
  private numberOfTries;
  private cancelTaskFn: (reason) => any;
  private USER_INTERACTIONS_EVENTS = [
      'onclick',
      'onscroll',
      'onkeydown',
      'onmousemove'
  ];

  constructor(private dialog: MatDialog) {
    this.running = false;
    this.timeout = null;
  }

  start(cancelTaskFn: () => any) {
    if (this.running) {
      return;
    }
    console.log('start');
    this.running = true;
    this.numberOfTries = 2;
    this.timeout = window.setTimeout(this.onTimeout.bind(this), this.TIMEOUT);
    this.listenToUserInteraction();
    this.cancelTaskFn = cancelTaskFn;
  }

  stop() {
    console.log('stop');
    this.running = false;
    clearTimeout(this.timeout);
    this.stopListeningToUserInteraction();
  }

  private onTimeout() {
    this.stop();

    const dialog = this.dialog.open(InactivityTimerComponent, {
      width: '600px',
      panelClass: 'inactivity-timer-dialog',
      disableClose: true,
      data: {
        numberOfTries: this.numberOfTries
      }
    }).afterClosed().subscribe((res) => {
      if (res === 'continue') {
        this.numberOfTries--;
        this.timeout = window.setTimeout(this.onTimeout.bind(this), this.TIMEOUT);
        this.running = true;
      }

      if (res === 'timeout') {
        this.cancelTaskFn('timeout');
      }
    });
  }

  private listenToUserInteraction() {
    forEach(this.USER_INTERACTIONS_EVENTS, (event) => {
      document[event] = this.onUserInteraction.bind(this);
    });
  }

  private stopListeningToUserInteraction() {
    forEach(this.USER_INTERACTIONS_EVENTS, (event) => {
      document[event] = null;
    });
  }

  private onUserInteraction(e: Event) {
    clearTimeout(this.timeout);
    this.timeout = window.setTimeout(this.onTimeout.bind(this), this.TIMEOUT);
  }
}
