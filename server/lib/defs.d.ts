import {Request} from "express";

 export type AuthenticatedRequest  =  Request & {
    sessionID: string;
    session: {
        touch: () => any,
        rolling: any
    }
    staff_id?: string;
    email: string;
    user: SmartUser;
}

interface SmartUser {
    staff_id?: string;
    email: string;
    projects: any;
    password: string;
    stitching: number;
    _id: any
}
