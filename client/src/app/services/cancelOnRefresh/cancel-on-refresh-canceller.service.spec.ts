import {TestBed} from '@angular/core/testing';

import {HttpClientModule} from '@angular/common/http';
import {CancelOnRefreshCancellerService} from './cancel-on-refresh-canceller-service';
import {ValidateManagerService} from '../validate-manager/validate-manager.service';
import {Observable} from 'rxjs';
import {ActivatedRoute} from "@angular/router";

describe('CancelOnRefreshCancellerService', () => {

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientModule],
            providers: [{
                provide: ActivatedRoute,
                useValue: {snapshot: {queryParams: {}}}
            }]
        });

        window.localStorage.clear();
    });

    it('should be created', () => {
        const service: CancelOnRefreshCancellerService = TestBed.get(CancelOnRefreshCancellerService);
        expect(service).toBeTruthy();
    });

    it('should cancel tasks', () => {
        const validateManager = TestBed.get(ValidateManagerService);
        const cancelFunctionSpy = spyOn(validateManager, 'cancelTask').and.returnValue(new Observable(observer => observer.next()));

        const service: CancelOnRefreshCancellerService = TestBed.get(CancelOnRefreshCancellerService);

        window.localStorage.setItem('pendingoos_approval', JSON.stringify([
            {
                taskType: 'oos_approval',
                selectorTaskId: '1111-1111',
                projectName: 'test',
                objectId: 1
            }
        ]));

        service.cancelTasks();

        expect(cancelFunctionSpy).toHaveBeenCalled();
    });

    it('should not cancel tasks when item in localstorage is not an array', () => {
        const validateManager = TestBed.get(ValidateManagerService);
        const cancelFunctionSpy = spyOn(validateManager, 'cancelTask').and.returnValue(new Observable(observer => observer.next()));

        const service: CancelOnRefreshCancellerService = TestBed.get(CancelOnRefreshCancellerService);

        window.localStorage.setItem('pendingoos_approval', JSON.stringify(
            {
                taskType: 'oos_approval',
                selectorTaskId: '1111-1111',
                projectName: 'test',
                objectId: 1
            }
        ));

        service.cancelTasks();

        expect(cancelFunctionSpy).not.toHaveBeenCalled();
    });
});
