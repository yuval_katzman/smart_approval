import { NgModule } from '@angular/core';
import {Routes, RouterModule, UrlSegment, UrlSegmentGroup, Route, UrlMatchResult} from '@angular/router';
import {ValidateComponent} from './components/validate/validate.component';
import {AuthGuardService} from './services/authGuard/auth-guard.service';

export function someMatcher(segments: UrlSegment[], group: UrlSegmentGroup, route: Route): UrlMatchResult {
  return {
    consumed: [new UrlSegment('', {})]
  };
}

const routes: Routes = [
  {
    path: 'approvals/:projectName/validate',
    component: ValidateComponent,
    canActivate: [AuthGuardService],
    resolve: {
    }
  },
  {
    matcher: someMatcher,
    component: ValidateComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: '**',
    redirectTo: 'approvals/dev/validate'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
