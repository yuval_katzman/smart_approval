import {keyBy, map} from 'lodash';
import { SceneParams } from './scene-params.interface';
import { Megascene } from './megascene';
import { Compound } from './compound';
import { Product } from './product';
import {YesNoQuestion, YesNoCantTellQuestion} from "./task-details/interface";
import * as QuestionDetailsFactory from "./task-details";
export enum AnswerOptions {
    NO = "no",
    YES = "yes",
    CANT_TELL = "cant_tell"
}

export enum YesNo {
    NO = "no",
    YES = "yes"
}
export interface Probe {
    pk: number;
    masks: {
        topLeft: {
            x: number,
            y: number
        },
        bottomRight: {
            x: number,
            y: number
        }
    }[];
}

export  interface ProbeStitchingData {
    creationTime: string;
    localImageTime: string;
    originalHeight: number;
    originalWidth: number;
    probeId: number;
    probeUrl: string;
}
export interface Answer {
    value?: string;
    option?: AnswerOptions;
}
export class Question {
    location: {
        probes: Probe[];
        scene: {
            pk: number;
            masks: {
                h: number;
                w: number;
                x: number;
                y: number;
            }[]
        };
    };
    productId: number;
    productDVCfk: number;
    questionDetails: YesNoQuestion | YesNoCantTellQuestion;
    questionType: 'YesNo' | 'YesNoCantTell';
    questionCategory: string;
    questionUid: string;
    answer: string | Answer;
    get isValidAnswer(): boolean {
        if ((this.questionType === "YesNo" && !this.questionDetails.returnValue) || !this.answer) {return !!this.answer; }
        const answer: Answer = this.answer as Answer ;
        if ((answer.option !== AnswerOptions.NO && this.questionDetails.returnValue) || !this.questionDetails.returnValue) {
            return !!answer.option;
        }

        return answer.value && answer.value !== this.questionDetails.value;
    }
    duration: number;
    lastSeenSceneId?: number;
}

class ProbeMegazordData {
    probeId: number;
    compound: {
        molecule: number,
        molecules: {
            atoms: {
                location: { top: number, left: number },
                probe: {
                    tileId: { x: number, y: number },
                    probeOriginalHeight: number,
                    probeOriginalWidth: number,
                    availableLevels: {
                        height: number,
                        imageStorePath: string,
                        s3ImagePath: string,
                        scale: number,
                        width: number
                    }[]
                }
            }[],
            index: number,
            transformation: [number, number, number, number, number, number, number, number, number,
                number, number, number, number, number, number, number],
            length: number,
        }[],
        numberOfProbes: number,
        numOfLevels: number,
        sceneHeightInTiles: number,
        sceneWidthInTiles: number,
        tileHeight: number,
        tileWidth: number
    };
    sceneParams: SceneParams;
    constructor(probe: ProbeStitchingData) {
        Object.assign(this, {
            probeId: probe.probeId,
            compound: {
                molecule: 1,
                molecules: [{
                    atoms: [{
                        location: { top: 1, left: 1 },
                        probe: {
                            tileId: { x: 0, y: 0 },
                            probeOriginalHeight: probe.originalHeight,
                            probeOriginalWidth: probe.originalWidth,
                            availableLevels: [
                                {
                                    height: probe.originalHeight,
                                    imageStorePath: probe.probeUrl + '/original',
                                    s3ImagePath: probe.probeUrl + '/original',
                                    scale: 1,
                                    width: probe.originalWidth,
                                },
                            ],
                        },
                    }],
                    index: 0,
                    transformation: [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1],
                    length: 1,
                }],
                numberOfProbes: 1,
                numOfLevels: 1,
                sceneHeightInTiles: 1,
                sceneWidthInTiles: 1,
                tileHeight: probe.originalHeight,
                tileWidth: probe.originalWidth,
            },
            sceneParams: {
                numOfTilesX: 1,
                numOfTilesY: 1,
                sceneHeight: probe.originalHeight,
                sceneWidth: probe.originalWidth,
                tileHeight: probe.originalHeight,
                tileWidth: probe.originalWidth,
            },
        });
    }
}

export class Task {
    taskType: string;
    selectorTaskId: string;
    projectName: string;
    objectId: number;
    persistence: any;
    queueDurationInSeconds: number;
    jobType?: string;
    questions: Question[];
    selectorTask: any;
    productData: Product[];
    sceneId: number;
    sceneUid: string;
    sessionUid: string;
    waveType: string;
    waveUid: string;
    approvalId: number;
    probeData?: ProbeStitchingData;
    sceneData?: {
        displayTypes: [],
        isLive: boolean;
        isSceneRecognition: boolean;
        liveProducts: [],
        probeImages: {
            commentsNum: number;
            originalHeight: number;
            originalWidth: number;
            probeId: number;
            probeImagePath: string;
            salesRepName: string;
            sceneId: number;
            status: number;
            storeName: string;
        }[];
        products: [];
        projectName: string;
        scene: {
            actualNumberOfProbes: number;
            clientVersion: string;
            comment: string;
            commentsNum: number;
            createdTime: string;
            displayTypeName: string;
            flagTime: string;
            isTiled: boolean;
            numberOfIgnoredProbes: number;
            numberOfProbes: number;
            projectName: string;
            realogramHtmlPath: string;
            reasons: null;
            region: null;
            retailerName: null;
            reviewers: [];
            salesRepId: null;
            salesRepName: null;
            sceneCreationTime: string;
            sceneDate: string;
            sceneId: number;
            sceneType: string;
            sceneTypeId: number;
            score: null;
            siblingSceneIds: number[];
            status: number;
            stitchedImagePath: string;
            storeArea: null;
            storeId: number;
            storeName: null;
            storeType: null;
            tiledSceneJson: {
                boundingBox: {
                    bottom: number,
                    left: number,
                    right: number,
                    top: number
                };
                creationTime: string;
                fullScene: {
                    x: number[];
                    y: number[];
                };
                hShift: number;
                numberOfLevels: number
                numberOfTilesX: number;
                numberOfTilesY: number
                projectName: string;
                scale: number;
                sceneId: number;
                stitchingSceneInfoId: number;
                tile_0_0: {
                    x: number[];
                    y: number[];
                };
                tile_1_0: {
                    x: number[];
                    y: number[];
                };
                tileHeight: number;
                tileWidth: number;
                vShift: number;
            };
            tilesDirectory: string;
            tilesCreationTime: string;
            type: number;
            user: null
            userAgent: null
            visitId: string;
        };
        sceneId: number;
        stitchingData: {
            displayTypeId: number;
            dividers: [];
            liveDividers: [];
            liveMolecules: {
                data: {
                    probeId: number;
                    transform: [[number, number, number], [number, number, number], [number, number, number]]
                }[];
            }[];
            molecules: {
                data: {
                    probeId: number;
                    transform: [[number, number, number], [number, number, number], [number, number, number]]
                }[];
            }[];
            probes: ProbeStitchingData[];
            stitchingSceneInfoId: number;
        };
    };
    megazordData: {
        compound: Compound;
        sceneParams: SceneParams;
    };
    probesMegazordData: ProbeMegazordData[];

    constructor(rawTask) {
        this.taskType = rawTask.taskType;
        this.selectorTaskId = rawTask.selectorTaskId;
        this.projectName = rawTask.projectName;
        this.objectId = rawTask.sceneId;
        this.persistence = rawTask.persistence;
        this.jobType = rawTask.job_type;
        this.queueDurationInSeconds = rawTask.queueDurationInSeconds;
        this.selectorTask = rawTask.selectorTask;
        this.productData = map(rawTask.productData, (rawProduct) => {
            return new Product(rawProduct);
        });
        const productsById = keyBy(this.productData, p => p.id);
        this.questions = map(rawTask.questions, (question, index) => {
            const className = question.question_category + question.question_type;
            const questionDetailsFactory = QuestionDetailsFactory[className] || QuestionDetailsFactory.OOSApprovalYesNo;

            return Object.assign(new Question(), {
                location: {
                    scene: {
                        pk: question.location.scene.pk,
                        masks: !rawTask.sceneData ? [{}] :  map(question.location.scene.masks, (mask) => {
                            const scale = rawTask.sceneData.scene.tiledSceneJson.scale;
                            const h_shift = rawTask.sceneData.scene.tiledSceneJson.h_shift;
                            const v_shift = rawTask.sceneData.scene.tiledSceneJson.v_shift;
                            return {
                                x: (mask.x * scale) - h_shift,
                                y: (mask.y * scale) - v_shift,
                                w: mask.w * scale,
                                h: mask.h * scale,
                            };
                        }),
                    },
                    probes: map(question.location.probes, (probe) => {
                        return {
                            pk: probe.pk,
                            masks: map(probe.masks, (mask) => {
                                return {
                                    topLeft: {
                                        x: mask.x - mask.w,
                                        y: mask.y - mask.h,
                                    },
                                    bottomRight: {
                                        x: mask.x + 2 * mask.w,
                                        y: mask.y + 2 * mask.h,
                                    },
                                };
                            }),
                        };
                    }),
                },
                productId: question.product_fk,
                productDVCfk: question.dvc_fk,
                question: question.question,
                questionType: question.question_type,
                questionCategory: question.question_category,
                questionUid: question.question_uid,
                answer: null,
                generatedId: index,
                duration: question.duration,
                lastSeenSceneId: question.last_seen_scene_fk,
                questionDetails:
                    new questionDetailsFactory(productsById[question.product_fk].name, question.value, question.return_fixed_value)
            });
        });
        this.sceneId = rawTask.sceneId;
        this.sceneUid = rawTask.scene_uid;
        this.sessionUid = rawTask.session_uid;
        this.waveUid = rawTask.waveUid;
        this.waveType = rawTask.waveType;
        this.approvalId = rawTask.approval_id;
        if (rawTask.sceneData) {
            const sceneData = rawTask.sceneData;
            const tiledSceneJson = sceneData.scene.tiledSceneJson;
            const stitchingData = sceneData.stitchingData;
            this.sceneData = {
                displayTypes: sceneData.displayTypes,
                isLive: sceneData.isLive,
                isSceneRecognition: sceneData.isSceneRecognition,
                liveProducts: sceneData.liveProducts,
                probeImages: map(sceneData.probeImages, (probeImage) => {
                    return {
                        commentsNum: probeImage.commentsNum,
                        originalHeight: probeImage.original_height,
                        originalWidth: probeImage.original_width,
                        probeId: probeImage.probe_id,
                        probeImagePath: probeImage.probe_image_path,
                        salesRepName: probeImage.sales_rep_name,
                        sceneId: probeImage.scene_fk,
                        status: probeImage.status,
                        storeName: probeImage.storeName,
                    };
                }),
                products: sceneData.products,
                projectName: sceneData.projectName,
                scene: {
                    actualNumberOfProbes: sceneData.scene.actual_number_of_probes,
                    clientVersion: sceneData.scene.client_version,
                    comment: sceneData.scene.comment,
                    commentsNum: sceneData.scene.comments_num,
                    createdTime: sceneData.scene.created_time,
                    displayTypeName: sceneData.scene.display_type_name,
                    flagTime: sceneData.scene.flag_time,
                    isTiled: sceneData.scene.isTiled,
                    numberOfIgnoredProbes: sceneData.scene.number_of_ignored_probes,
                    numberOfProbes: sceneData.scene.number_of_probes,
                    projectName: sceneData.scene.project_name,
                    realogramHtmlPath: sceneData.scene.realogram_html_path,
                    reasons: sceneData.scene.reasons,
                    region: sceneData.scene.region,
                    retailerName: sceneData.scene.retailer_name,
                    reviewers: sceneData.scene.reviewers,
                    salesRepId: sceneData.scene.sales_rep_id,
                    salesRepName: sceneData.scene.sales_rep_name,
                    sceneCreationTime: sceneData.scene.scene_creation_time,
                    sceneDate: sceneData.scene.scene_date,
                    sceneId: sceneData.scene.scene_id,
                    sceneType: sceneData.scene.scene_type,
                    sceneTypeId: sceneData.scene.scene_type_id,
                    score: sceneData.scene.score,
                    siblingSceneIds: sceneData.scene.siblingSceneIds,
                    status: sceneData.scene.status,
                    stitchedImagePath: sceneData.scene.stitched_image_path,
                    storeArea: sceneData.scene.store_area,
                    storeId: sceneData.scene.store_id,
                    storeName: sceneData.scene.store_name,
                    storeType: sceneData.scene.store_type,
                    tiledSceneJson: {
                        boundingBox: tiledSceneJson.boundingBox,
                        creationTime: tiledSceneJson.creation_time,
                        fullScene: tiledSceneJson.full_scene,
                        hShift: tiledSceneJson.h_shift,
                        numberOfLevels: tiledSceneJson.number_of_levels,
                        numberOfTilesX: tiledSceneJson.number_of_tiles_x,
                        numberOfTilesY: tiledSceneJson.number_of_tiles_y,
                        projectName: tiledSceneJson.project_name,
                        scale: tiledSceneJson.scale,
                        sceneId: tiledSceneJson.scene_id,
                        stitchingSceneInfoId: tiledSceneJson.stitching_scene_info_id,
                        tile_0_0: tiledSceneJson.tile_0_0,
                        tile_1_0: tiledSceneJson.tile_1_0,
                        tileHeight: tiledSceneJson.tile_height,
                        tileWidth: tiledSceneJson.tile_width,
                        vShift: tiledSceneJson.v_shift,
                    },
                    tilesDirectory: sceneData.scene.tilesDirectory,
                    tilesCreationTime: sceneData.scene.tiles_creation_time,
                    type: sceneData.scene.type,
                    user: sceneData.scene.user,
                    userAgent: sceneData.scene.user_agent,
                    visitId: sceneData.scene.visit_id,
                },
                sceneId: sceneData.sceneId,
                stitchingData: {
                    displayTypeId: stitchingData.displayTypeId,
                    dividers: stitchingData.dividers,
                    liveDividers: stitchingData.liveDividers,
                    liveMolecules: map(stitchingData.liveMolecules, (liveMolecule) => {
                        return {
                            data: map(liveMolecule.data, (probe) => {
                                return {
                                    probeId: probe.probe_pk,
                                    transform: probe.transform,
                                };
                            }),
                        };
                    }),
                    molecules: map(stitchingData.molecules, (molecule) => {
                        return {
                            data: map(molecule.data, (probe) => {
                                return {
                                    probeId: probe.probe_pk,
                                    transform: probe.transform,
                                };
                            }),
                        };
                    }),
                    probes: map(stitchingData.probes, (probe) => {
                        return {
                            creationTime: probe.creation_time,
                            localImageTime: probe.local_image_time,
                            originalHeight: probe.original_height,
                            originalWidth: probe.original_width,
                            probeId: probe.probe_pk,
                            probeUrl: probe.probe_url,
                        };
                    }),
                    stitchingSceneInfoId: stitchingData.stitchingSceneInfoId,
                },
            };
            const megaScene = new Megascene(rawTask.sceneData);
            this.megazordData = {
                compound: new Compound(megaScene, rawTask.sceneData.scene.tiledSceneJson, rawTask.sceneData.scene.tilesDirectory),
                sceneParams: {
                    numOfTilesX: this.sceneData.scene.tiledSceneJson.numberOfTilesX,
                    numOfTilesY: this.sceneData.scene.tiledSceneJson.numberOfTilesY,
                    sceneHeight: this.sceneData.scene.tiledSceneJson.numberOfTilesY * this.sceneData.scene.tiledSceneJson.tileHeight,
                    sceneWidth: this.sceneData.scene.tiledSceneJson.numberOfTilesX * this.sceneData.scene.tiledSceneJson.tileWidth,
                    tileHeight: this.sceneData.scene.tiledSceneJson.tileHeight,
                    tileWidth: this.sceneData.scene.tiledSceneJson.tileWidth,
                },
            };
            this.probesMegazordData = map(this.sceneData.stitchingData.probes, probe => new ProbeMegazordData(probe));
        } else {
            this.probesMegazordData = [new ProbeMegazordData(rawTask.probeData)];
        }
    }
}

