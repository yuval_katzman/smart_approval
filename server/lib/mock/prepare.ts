import {promisify} from "util";
const nc = require('@trax-retail/node-core')('smart-approval');
const msg = require('./msg.json')
import {map, mergeWith, uniq} from "lodash";
import {writeFileSync} from 'fs';

function createTaskObject(response) {
    const votingProcessId = (response && response.votingProcessId) ||
        (response && response.params && response.params.voting_process_fk);

    const obj = {
        [`${response.objectType}Id`]: response.objectId,
        [`${response.objectType}_id`]: response.objectId,
        project: response.projectName,
        projectName: response.projectName,
        project_name: response.projectName,
        taskType: response.taskType,
        selectorTaskId: response.uuid,
        waveType: response.waveType,
        waveUid: response.waveUid,
        wave_type: response.waveType,
        wave_uid: response.waveUid,
        queueDurationInSeconds: response.queueDurationInSeconds,
        voting_process_fk: votingProcessId,
        selectorTask: response
    };

    return mergeWith(obj, response);
}

async function getProductData(projectName: string, productIds: number[]) {
    const reqSettings = {
        method: 'GET',
        url: `${nc.config.pnbUrl}/pnb/api/projects/${projectName}/products?filter[id]=${uniq(productIds).join(',')}`,
        headers: {
            'Content-Type': 'application/json'
        },
        json: true
    };

    let res;
    try {
        res = await promisify(nc.utils.serverToServerHttpRequest)('smart', projectName, reqSettings);
    } catch (err) {
        nc.logger.error({
            err
        }, 'Error getting data from P&B');

        throw err;
    }
    const productData = JSON.parse(res.httpRequest.body);
    return productData.data;
}

async function getSceneData(projectName: string, sceneId: number) {
    const reqSettings = {
        method: 'GET',
        url: `${nc.config.xSuiteUrl}/trax-one/api/projects/${projectName}/scene/${sceneId}`,
        headers: {
            'Content-Type': 'application/json'
        },
        json: true
    };

    let res;
    try {
        res = await promisify(nc.utils.serverToServerHttpRequest)('smart', projectName, reqSettings);
    } catch (err) {
        nc.logger.error({
            err
        }, 'Error getting data from Explorer');

        throw err;
    }
    const sceneData = JSON.parse(res.httpRequest.body);
    return sceneData;
}

export async function run() {
    try {
        const task = createTaskObject(msg);
        task.sceneId = task.sceneId || task.scene_id;
        const productIds = map(task.questions, 'product_fk');
        const [sceneData, productData] = await Promise.all([
            getSceneData(task.project_name, task.sceneId),
            getProductData(task.project_name, productIds)
        ]);

        task.sceneData = sceneData;
        task.productData = productData;
        writeFileSync(__dirname + 'task.json', JSON.stringify(task));
    } catch (e) {
        throw e;
    }
}
