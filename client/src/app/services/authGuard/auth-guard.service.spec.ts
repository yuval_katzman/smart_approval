import {fakeAsync, TestBed, tick} from '@angular/core/testing';

import { AuthGuardService } from './auth-guard.service';
import {CookieService} from "ngx-cookie-service";
import {HttpClientModule} from "@angular/common/http";
import {ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {UserService} from "../user/user.service";
import {Observable} from "rxjs";

describe('AuthGuardService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule],
    providers: [
        CookieService
    ]
  }));

  it('should be created', () => {
    const service: AuthGuardService = TestBed.get(AuthGuardService);
    expect(service).toBeTruthy();
  });

  it('user is not authenticated - should logout', fakeAsync(() => {
    const service: AuthGuardService = TestBed.get(AuthGuardService);
    const userService = TestBed.get(UserService);

    spyOn(userService, 'isAuthenticated').and.returnValue(false);

    const logoutSpy = spyOn(userService, 'logout');

    service.canActivate(new ActivatedRouteSnapshot(), {url: 'testUrl'} as RouterStateSnapshot);

    tick();

    expect(logoutSpy).toHaveBeenCalled();
  }));

  it('user is authenticated but not authorized - should be redirected to /denied', fakeAsync(() => {
    const service: AuthGuardService = TestBed.get(AuthGuardService);
    const userService = TestBed.get(UserService);

    spyOn(userService, 'isAuthenticated').and.returnValue(true);

    spyOn(userService, 'isAuthorized').and.returnValue(new Observable(observer => observer.next(false)));

    const accessDeniedSpy = spyOn(userService, 'sendToAccessDeniedPage');

    const activatedRouteSnapshot = new ActivatedRouteSnapshot();

    activatedRouteSnapshot.params = {
      projectName: 'test'
    };
    activatedRouteSnapshot.data = {
      requiredPermissions: []
    };

    const canActivate  = service.canActivate(activatedRouteSnapshot, {url: 'testUrl'} as RouterStateSnapshot) as Observable<boolean>;

    canActivate.subscribe();
    tick();

    expect(accessDeniedSpy).toHaveBeenCalled();

  }));

  it('user is authenticated and authorized - should let user into the route', () => {
    const service: AuthGuardService = TestBed.get(AuthGuardService);
    const userService = TestBed.get(UserService);

    spyOn(userService, 'isAuthenticated').and.returnValue(true);

    spyOn(userService, 'isAuthorized').and.returnValue(new Observable(observer => observer.next(true)));

    const activatedRouteSnapshot = new ActivatedRouteSnapshot();

    activatedRouteSnapshot.params = {
      projectName: 'test'
    };
    activatedRouteSnapshot.data = {
      requiredPermissions: []
    };

    const canActivate  = service.canActivate(activatedRouteSnapshot, {url: 'testUrl'} as RouterStateSnapshot) as Observable<boolean>;

    canActivate.subscribe((res) => {
      expect(res).toBe(true);
    });

  });
});
