import {
    async,
    ComponentFixture,
    discardPeriodicTasks,
    fakeAsync,
    TestBed,
    tick
} from '@angular/core/testing';
import {ValidateComponent} from './validate.component';
import {HeaderTitleComponent} from '../header-title/header-title.component';
import {TransitionGroupComponent} from '../transition-group/transition-group.component';
import {MegazordCanvasDirective} from '../megazordCanvas/megazord-canvas.directive';
import {RelativeStopwatchComponent} from '../relative-stopwatch/relative-stopwatch.component';
import {CookieService} from 'ngx-cookie-service';
import {HttpClientModule, HttpParams} from '@angular/common/http';
import {MatDialog, MatDialogModule} from '@angular/material';
import {Observable, of} from 'rxjs';
import {UserService} from '../../services/user/user.service';
import {User} from '../../models/user';
import {ActivatedRoute} from '@angular/router';
import {ValidateManagerService} from '../../services/validate-manager/validate-manager.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Task, Question} from '../../models/task';
import {Lightbox, LightboxModule} from 'ngx-lightbox';
import {Key} from 'ts-keycode-enum';
import {BrowserModule, By} from '@angular/platform-browser';
import {wait} from '../../../utils';
import {InactivityTimerService} from '../../services/inactivityTimer/inactivity-timer.service';
import {environment} from '../../../environments/environment';
import { ElementRef} from '@angular/core';
import { cloneDeep } from 'lodash';
import {YesNoCantTellComponent} from "./yes-no-cant-tell/yes-no-cant-tell.component";
import {MatRadioButton, MatRadioModule} from "@angular/material/radio";
import { MatInputModule} from "@angular/material/input";
import {AppRoutingModule} from "../../app-routing.module";
import {FormsModule} from "@angular/forms";
import {DragDropModule} from "@angular/cdk/drag-drop";
import {YesNoComponent} from "./yes-no/yes-no.component";
import {HotkeyDirective} from "./yes-no-cant-tell/hotkey.directive";
import {LowFacingApprovalYesNo, LowFacingApprovalYesNoCantTell} from "../../models/task-details/low-facing-approval";
const ROWTASK  = require('./test/rawTask.json');
const task = new Task(ROWTASK);
const user = new User({
    address: '',
    birthDate: '',
    center: '',
    country: '',
    createDate: '',
    currentCenter: '',
    deleteDate: null,
    deleted: false,
    email: 'user@traxretail.com',
    firstName: 'Test',
    gender: 'Female',
    isActive: true,
    isStaff: false,
    isSuperuser: false,
    joinDate: '',
    lastName: 'User',
    lastLogin: null,
    mobile: '',
    modules: [],
    parentsPhone: '',
    projects: {
        test: {
            validate: 5
        }
    },
    roles: ['QAT'],
    staffId: null,
    stitching: 5,
    teamMembers: []
});
const triggerKeyPress = (key: string) => {
    const event: any = document.createEvent('CustomEvent');
    event.key = key;
    event.initEvent(key.startsWith("Arrow") ?  'keydown' : 'keypress', true, true);
    document.dispatchEvent(event);
};

class MockElementRef extends ElementRef {
    constructor() { super(null); }
}

class UserServiceMock {
    getLoggedInUser() {
        return new Observable((observer) => {
            return observer.next(user);
        });
    }
}

class ActivatedRouteMock {
    params;
    snapshot = {queryParams: {}};
    constructor() {
        this.params = new Observable((observer) => {
            observer.next({
                projectName: 'test'
            });
        });
    }
}

class ValidateManagerMock {
    getQueueCount(projectName: string) {
        return new Observable((observer) => {
            setTimeout(() => {
                return observer.next({
                    count: 1
                });
            }, 100);
        });
    }


    getTemplate(projectName: string) {
        return new Observable((observer) => {
            return observer.next({
                name: "templateName"
            });
        });
    }

    getNextTask(projectName: string) {
        return new Observable((observer) => {
            observer.next(task);
        });
    }

    cancelTask(projectName: string, selectorTaskId: string, sceneId: number, reason: string) {
        return of({});
    }

    saveTask() {
        return of({}).subscribe(() => {
        });
    }


    getProductImages() {
        return [{}];
    }
}

class MatDialogMock {
    open(cls: any, options: any) {
        return {
            afterClosed() {
                return new Observable((observer) => {
                    observer.next({
                        getNextTask: true
                    });
                });
            }
        };
    }

    close() {
    }
}

describe('ValidateComponent', () => {
    let component: ValidateComponent;
    let fixture: ComponentFixture<ValidateComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ValidateComponent,
                HeaderTitleComponent,
                TransitionGroupComponent,
                MegazordCanvasDirective,
                RelativeStopwatchComponent,
                YesNoCantTellComponent,
                YesNoComponent,
                HotkeyDirective
            ],
            imports: [
                BrowserModule,
                AppRoutingModule,
                HttpClientModule,
                BrowserAnimationsModule,
                MatDialogModule,
                FormsModule,
                DragDropModule,
                LightboxModule,
                MatRadioModule,
                MatInputModule
            ],
            providers: [CookieService,
                MegazordCanvasDirective,
                {
                    provide: ElementRef,
                    useClass: MockElementRef
                },
                {
                    provide: UserService,
                    useClass: UserServiceMock
                },
                {
                    provide: ActivatedRoute,
                    useClass: ActivatedRouteMock
                },
                {
                    provide: ValidateManagerService,
                    useClass: ValidateManagerMock
                },
                {
                    provide: MatDialog,
                    useClass: MatDialogMock
                }
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ValidateComponent);
        component = fixture.componentInstance;
        component.task = task;
        fixture.detectChanges();
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 40000;
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('`should show task`', async () => {
        const items = fixture.debugElement.queryAll(By.css('.question-summary'));

        expect(items.length).toBeGreaterThan(0);
    });

    it('should call ValidateManagerService getQueueCount every 10 seconds', fakeAsync(() => {
        const service: ValidateManagerService = TestBed.get(ValidateManagerService);
        const getQueueCountSpy = spyOn<any>(service, 'getQueueCount').and.callThrough();
        fixture = TestBed.createComponent(ValidateComponent);
        component = fixture.componentInstance;
        component.task = task;
        fixture.detectChanges();

        expect(getQueueCountSpy).toHaveBeenCalledTimes(1);
        tick(10000);
        expect(getQueueCountSpy).toHaveBeenCalledTimes(2);
        tick(20000);
        expect(getQueueCountSpy).toHaveBeenCalledTimes(4);
        tick(100000);
        expect(getQueueCountSpy).toHaveBeenCalledTimes(14);

        // need this to clear the setInterval tasks
        discardPeriodicTasks();
        tick(1000);
        discardPeriodicTasks();
    }));

    it('selectedLastSeenUrl should return the correct url', async () => {
        const items = fixture.debugElement.queryAll(By.css('.question-summary'));
        items[1].triggerEventHandler('click', null);
        const wantedUrl =
            `${environment.config.xsuite}${component.task.projectName}/explore/scene/${component.selectedQuestion.lastSeenSceneId}?product=${component.selectedQuestion.productId}`;

        const actualUrl = component.selectedLastSeenUrl;

        expect(actualUrl).toEqual(wantedUrl);
    });

    describe('Pressing hotkey', () => {
        describe(`'y'`, () => {
            it(`should trigger onAnswer 'yes'`, async () => {
                const spy = spyOn<any>(component, 'onAnswer').and.callThrough();
                const wantedArg = 'yes';
                const items = fixture.debugElement.queryAll(By.css('.question-summary'));
                items[1].triggerEventHandler('click', null);
                fixture.detectChanges();
                triggerKeyPress('y');
                fixture.detectChanges();

                expect(spy).toHaveBeenCalledWith(wantedArg);
            });
        });

        describe(`'n'`, () => {
            it(`should trigger onAnswer 'no'`, async () => {
                const spy = spyOn<any>(component, 'onAnswer').and.callThrough();
                const wantedArg = 'no';
                const items = fixture.debugElement.queryAll(By.css('.question-summary'));
                items[1].triggerEventHandler('click', null);
                fixture.detectChanges();
                triggerKeyPress('n');
                fixture.detectChanges();

                expect(spy).toHaveBeenCalledWith(wantedArg);
            });
        });

        describe(`'s'`, () => {
            it(`should trigger onChangeProbeSceneMode 'scene'`, async () => {
                const spy = spyOn<any>(component, 'onChangeProbeSceneMode').and.callThrough();
                const wantedArg = 'scene';
                const items = fixture.debugElement.queryAll(By.css('.question-summary'));
                items[1].triggerEventHandler('click', null);
                fixture.detectChanges();

                triggerKeyPress('s');
                fixture.detectChanges();

                expect(spy).toHaveBeenCalledWith(wantedArg);
            });
        });

        describe(`'p'`, () => {
            it(`should trigger onChangeProbeSceneMode 'probes'`, async () => {
                const spy = spyOn<any>(component, 'onChangeProbeSceneMode').and.callThrough();
                const wantedArg = 'probes';
                const items = fixture.debugElement.queryAll(By.css('.question-summary'));
                items[0].triggerEventHandler('click', null);
                fixture.detectChanges();

                triggerKeyPress('p');
                fixture.detectChanges();

                expect(spy).toHaveBeenCalledWith(wantedArg);
            });
        });

        describe(`'leftArrow'`, () => {
            it(`should change 'arrowKeysAffectedList' from 'probes' to 'questions'`, async () => {
                component.arrowKeysAffectedList = 'probes';
                const wantedValue = 'questions';
                const items = fixture.debugElement.queryAll(By.css('.question-summary'));
                items[1].triggerEventHandler('click', null);
                fixture.detectChanges();
                triggerKeyPress('ArrowLeft');
                fixture.detectChanges();

                expect(component.arrowKeysAffectedList).toEqual(wantedValue);
            });

            it(`should not change 'arrowKeysAffectedList' if it was already set to 'questions'`, async () => {
                component.arrowKeysAffectedList = 'questions';
                const wantedValue = 'questions';
                const items = fixture.debugElement.queryAll(By.css('.question-summary'));
                items[1].triggerEventHandler('click', null);
                fixture.detectChanges();
                triggerKeyPress('ArrowLeft');
                fixture.detectChanges();

                expect(component.arrowKeysAffectedList).toEqual(wantedValue);
            });
        });

        describe(`'rightArrow'`, () => {
            it(`should change 'arrowKeysAffectedList' from 'questions' to 'probes'`, async () => {
                component.arrowKeysAffectedList = 'questions';
                const wantedValue = 'probes';
                const items = fixture.debugElement.queryAll(By.css('.question-summary'));
                items[1].triggerEventHandler('click', null);
                fixture.detectChanges();
                triggerKeyPress('ArrowRight');
                fixture.detectChanges();

                expect(component.arrowKeysAffectedList).toEqual(wantedValue);
            });

            it(`should not change 'arrowKeysAffectedList' if it was already set to 'probes'`, async () => {
                component.arrowKeysAffectedList = 'probes';
                const wantedValue = 'probes';
                const items = fixture.debugElement.queryAll(By.css('.question-summary'));
                items[1].triggerEventHandler('click', null);
                fixture.detectChanges();
                triggerKeyPress('ArrowRight');

                fixture.detectChanges();

                expect(component.arrowKeysAffectedList).toEqual(wantedValue);
            });
        });

        describe(`'upArrow'`, () => {
            it(`should not call 'questionOnClick' nor 'onChangeSelectedProbe' when there is no selected question`, async () => {
                const questionOnClickSpy = spyOn<any>(component, 'questionOnClick').and.callThrough();
                const onChangeSelectedProbeSpy = spyOn<any>(component, 'onChangeSelectedProbe').and.callThrough();
                component.clearSelectedQuestion();
                fixture.detectChanges();
                triggerKeyPress('ArrowUp');
                fixture.detectChanges();

                expect(questionOnClickSpy).not.toHaveBeenCalled();
                expect(onChangeSelectedProbeSpy).not.toHaveBeenCalled();
            });

            it(`should call 'questionOnClick' but not 'onChangeSelectedProbe' when 'arrowKeysAffectedList' is set to 'questions'`, async () => {
                triggerKeyPress('s');
                await wait(100);
                fixture.detectChanges();
                const items = fixture.debugElement.queryAll(By.css('.question-summary'));
                items[1].triggerEventHandler('click', null);
                fixture.detectChanges();
                const questionOnClickSpy = spyOn<any>(component, 'questionOnClick').and.callThrough();
                const onChangeSelectedProbeSpy = spyOn<any>(component, 'onChangeSelectedProbe').and.callThrough();
                triggerKeyPress('ArrowUp');
                fixture.detectChanges();

                expect(questionOnClickSpy).toHaveBeenCalledTimes(1);
                expect(onChangeSelectedProbeSpy).not.toHaveBeenCalled();
            });

            it(`should not call 'questionOnClick' nor 'onChangeSelectedProbe' when the first question is selected and 'arrowKeysAffectedList' is set to 'questions'`, async () => {
                triggerKeyPress('s');
                await wait(100);
                fixture.detectChanges();
                const items = fixture.debugElement.queryAll(By.css('.question-summary'));
                items[0].triggerEventHandler('click', null);
                fixture.detectChanges();
                const questionOnClickSpy = spyOn<any>(component, 'questionOnClick').and.callThrough();
                const onChangeSelectedProbeSpy = spyOn<any>(component, 'onChangeSelectedProbe').and.callThrough();
                triggerKeyPress('ArrowUp');
                fixture.detectChanges();

                expect(questionOnClickSpy).not.toHaveBeenCalled();
                expect(onChangeSelectedProbeSpy).not.toHaveBeenCalled();
            });

            it(`should call 'onChangeSelectedProbe' but not 'questionOnClick' when 'arrowKeysAffectedList' is set to 'probes'`, async () => {
                const items = fixture.debugElement.queryAll(By.css('.question-summary'));
                items[1].triggerEventHandler('click', null);
                fixture.detectChanges();
                triggerKeyPress('p');
                fixture.detectChanges();
                triggerKeyPress('ArrowDown');
                fixture.detectChanges();
                await wait(100);
                const questionOnClickSpy = spyOn<any>(component, 'questionOnClick').and.callThrough();
                const onChangeSelectedProbeSpy = spyOn<any>(component, 'onChangeSelectedProbe').and.callThrough();
                triggerKeyPress('ArrowUp');
                fixture.detectChanges();

                expect(questionOnClickSpy).not.toHaveBeenCalled();
                expect(onChangeSelectedProbeSpy).toHaveBeenCalledTimes(1);
            });

            it(`should not call 'questionOnClick' nor 'onChangeSelectedProbe' when the first probe is selected and 'arrowKeysAffectedList' is set to 'probes'`, async () => {
                const items = fixture.debugElement.queryAll(By.css('.question-summary'));
                items[1].triggerEventHandler('click', null);
                fixture.detectChanges();
                triggerKeyPress('p');
                await wait(100);
                fixture.detectChanges();
                const questionOnClickSpy = spyOn<any>(component, 'questionOnClick').and.callThrough();
                const onChangeSelectedProbeSpy = spyOn<any>(component, 'onChangeSelectedProbe').and.callThrough();
                triggerKeyPress('ArrowUp');
                fixture.detectChanges();

                expect(questionOnClickSpy).not.toHaveBeenCalled();
                expect(onChangeSelectedProbeSpy).not.toHaveBeenCalled();
            });
        });

        describe(`'downArrow'`, () => {
            it(`should not call 'questionOnClick' nor 'onChangeSelectedProbe' when there is no selected question`, async () => {
                const questionOnClickSpy = spyOn<any>(component, 'questionOnClick').and.callThrough();
                const onChangeSelectedProbeSpy = spyOn<any>(component, 'onChangeSelectedProbe').and.callThrough();
                component.clearSelectedQuestion();
                fixture.detectChanges();
                triggerKeyPress('ArrowDown');
                fixture.detectChanges();

                expect(questionOnClickSpy).not.toHaveBeenCalled();
                expect(onChangeSelectedProbeSpy).not.toHaveBeenCalled();
            });

            it(`should call 'questionOnClick' but not 'onChangeSelectedProbe' when 'arrowKeysAffectedList' is set to 'questions'`, async () => {
                triggerKeyPress('s');
                await wait(100);
                fixture.detectChanges();
                const items = fixture.debugElement.queryAll(By.css('.question-summary'));
                items[1].triggerEventHandler('click', null);
                fixture.detectChanges();
                const questionOnClickSpy = spyOn<any>(component, 'questionOnClick').and.callThrough();
                const onChangeSelectedProbeSpy = spyOn<any>(component, 'onChangeSelectedProbe').and.callThrough();

                triggerKeyPress('ArrowDown');
                fixture.detectChanges();

                expect(questionOnClickSpy).toHaveBeenCalledTimes(1);
                expect(onChangeSelectedProbeSpy).not.toHaveBeenCalled();
            });

            it(`should not call 'questionOnClick' nor 'onChangeSelectedProbe' when the last question is selected and 'arrowKeysAffectedList' is set to 'questions'`, async () => {
                triggerKeyPress('s');
                await wait(100);
                fixture.detectChanges();
                const items = fixture.debugElement.queryAll(By.css('.question-summary'));
                items[items.length - 1].triggerEventHandler('click', null);
                fixture.detectChanges();
                const questionOnClickSpy = spyOn<any>(component, 'questionOnClick').and.callThrough();
                const onChangeSelectedProbeSpy = spyOn<any>(component, 'onChangeSelectedProbe').and.callThrough();
                triggerKeyPress('ArrowDown');
                fixture.detectChanges();

                expect(questionOnClickSpy).not.toHaveBeenCalled();
                expect(onChangeSelectedProbeSpy).not.toHaveBeenCalled();
            });

            it(`should call 'onChangeSelectedProbe' but not 'questionOnClick' when 'arrowKeysAffectedList' is set to 'probes'`, async () => {
                const items = fixture.debugElement.queryAll(By.css('.question-summary'));
                items[1].triggerEventHandler('click', null);
                fixture.detectChanges();
                triggerKeyPress('p');
                fixture.detectChanges();
                const questionOnClickSpy = spyOn<any>(component, 'questionOnClick').and.callThrough();
                const onChangeSelectedProbeSpy = spyOn<any>(component, 'onChangeSelectedProbe').and.callThrough();
                triggerKeyPress('ArrowDown');
                fixture.detectChanges();

                expect(questionOnClickSpy).not.toHaveBeenCalled();
                expect(onChangeSelectedProbeSpy).toHaveBeenCalledTimes(1);
            });

            it(`should not call 'questionOnClick' nor 'onChangeSelectedProbe' when the last probe is selected and 'arrowKeysAffectedList' is set to 'probes'`, async () => {
                const items = fixture.debugElement.queryAll(By.css('.question-summary'));
                items[1].triggerEventHandler('click', null);
                fixture.detectChanges();
                triggerKeyPress('p');
                await wait(100);
                fixture.detectChanges();
                component.onChangeSelectedProbe(component.task.probesMegazordData[component.task.probesMegazordData.length - 1]);
                await wait(100);
                fixture.detectChanges();
                const questionOnClickSpy = spyOn<any>(component, 'questionOnClick').and.callThrough();
                const onChangeSelectedProbeSpy = spyOn<any>(component, 'onChangeSelectedProbe').and.callThrough();
                triggerKeyPress('ArrowDown');
                fixture.detectChanges();
                expect(questionOnClickSpy).not.toHaveBeenCalled();
                expect(onChangeSelectedProbeSpy).not.toHaveBeenCalled();
            });
        });
    });

    describe(`'finishTask'`, () => {
        it(`should clear data`, async () => {
            const clearSelectedQuestionSpy = spyOn<any>(component, 'clearSelectedQuestion').and.callThrough();
            const openBetweenItemsDialogSpy = spyOn<any>(component, 'openBetweenItemsDialog').and.stub();

            component.finishTask();

            expect(component.task).toBe(null);
            expect(component.selectedProbe).toBe(null);
            expect(component.initialScale).toBe(null);
            expect(component.selectedProbeMasks.length).toBe(0);
            expect(clearSelectedQuestionSpy).toHaveBeenCalled();
        });

        it(`should stop inactivity timer`, async () => {
            const service: InactivityTimerService = TestBed.get(InactivityTimerService);
            const inactivityTimerStopSpy = spyOn<any>(service, 'stop').and.stub();
            const openBetweenItemsDialogSpy = spyOn<any>(component, 'openBetweenItemsDialog').and.stub();

            component.finishTask();

            expect(inactivityTimerStopSpy).toHaveBeenCalled();
        });

        it(`should open the next item dialog after`, async () => {
            const openBetweenItemsDialogSpy = spyOn<any>(component, 'openBetweenItemsDialog').and.stub();

            component.finishTask();

            expect(openBetweenItemsDialogSpy).toHaveBeenCalled();
        });
    });

    describe(`'cancelTask'`, () => {
        it(`should call 'cancelTask' from ValidateManagerService`, async () => {
            const service: ValidateManagerService = TestBed.get(ValidateManagerService);
            const cancelTaskSpy = spyOn<any>(service, 'cancelTask').and.callThrough();
            const openBetweenItemsDialogSpy = spyOn<any>(component, 'openBetweenItemsDialog').and.stub();

            component.cancelTask('test');

            expect(cancelTaskSpy).toHaveBeenCalled();
        });

        it(`should call 'finishTask'`, async () => {
            const finishTaskSpy = spyOn<any>(component, 'finishTask').and.callThrough();
            const openBetweenItemsDialogSpy = spyOn<any>(component, 'openBetweenItemsDialog').and.stub();

            component.cancelTask('test');

            expect(finishTaskSpy).toHaveBeenCalled();
        });
    });

    describe(`'saveTask'`, () => {
        it(`should call 'saveTask' from ValidateManagerService`, async () => {
            const service: ValidateManagerService = TestBed.get(ValidateManagerService);
            const saveTaskSpy = spyOn<any>(service, 'saveTask').and.callThrough();
            const openBetweenItemsDialogSpy = spyOn<any>(component, 'openBetweenItemsDialog').and.stub();

            component.saveTask();

            expect(saveTaskSpy).toHaveBeenCalled();
        });

        it(`should call 'finishTask'`, async () => {
            const finishTaskSpy = spyOn<any>(component, 'finishTask').and.callThrough();
            const openBetweenItemsDialogSpy = spyOn<any>(component, 'openBetweenItemsDialog').and.stub();

            component.saveTask();

            expect(finishTaskSpy).toHaveBeenCalled();
        });
    });

    describe(`Zooming`, () => {
        it(`should emit a zoom event when 'onZoomIn' is called`, async () => {
            const zoomEventSpy = spyOn<any>(component.zoomEvent, 'emit').and.stub();

            component.onZoomIn();

            expect(zoomEventSpy).toHaveBeenCalled();
        });

        it(`should emit a zoom event when 'onZoomOut' is called`, async () => {
            const zoomEventSpy = spyOn<any>(component.zoomEvent, 'emit').and.stub();

            component.onZoomOut();

            expect(zoomEventSpy).toHaveBeenCalled();
        });

        it(`should emit a fitToScreenEvent event when 'onFitToScreen' is called`, async () => {
            const fitToScreenEventSpy = spyOn<any>(component.fitToScreenEvent, 'emit').and.stub();

            component.onFitToScreen();

            expect(fitToScreenEventSpy).toHaveBeenCalled();
        });

        it(`should not zoom to selected mask after question is clicked if megazord does not exist`, async () => {
            const items = fixture.debugElement.queryAll(By.css('.question-summary'));
            items[1].triggerEventHandler('click', null);
            const directive: MegazordCanvasDirective = TestBed.get(MegazordCanvasDirective);
            const moveFrustumToSpy = spyOn<any>(directive, 'moveFrustumTo').and.stub();
            component.megazordCanvas = null;

            items[2].triggerEventHandler('click', null);

            expect(moveFrustumToSpy).not.toHaveBeenCalled();
        });
    });

    describe(`onAnswer`, () => {
        it(`should clear selected question`, async () => {
            const clearSelectedQuestionSpy = spyOn<any>(component, 'clearSelectedQuestion').and.callThrough();
            const items = fixture.debugElement.queryAll(By.css('.question-summary'));
            items[1].triggerEventHandler('click', null);
            for (let i = 0; i < component.task.questions.length; i++) {
                component.task.questions[i].answer = null;
            }
            fixture.detectChanges();

            triggerKeyPress('n');
            fixture.detectChanges();

            expect(clearSelectedQuestionSpy).toHaveBeenCalled();
        });

        it(`should not clear selected question if there are no more questions to answer`, async () => {
            const clearSelectedQuestionSpy = spyOn<any>(component, 'clearSelectedQuestion').and.callThrough();
            const items = fixture.debugElement.queryAll(By.css('.question-summary'));
            items[1].triggerEventHandler('click', null);
            for (let i = 0; i < component.task.questions.length; i++) {
                component.task.questions[i].answer = 'no';
            }
            fixture.detectChanges();

            triggerKeyPress('n');
            fixture.detectChanges();

            expect(clearSelectedQuestionSpy).not.toHaveBeenCalled();
        });
    });

    describe(`getQuestionTypeText`, () => {
        it(`should return 'Yes / No' when given 'YesNo' question type`, async () => {
            const wantedAnswer = 'Yes / No';

            const actualAnswer = component.getQuestionTypeText('YesNo');

            expect(actualAnswer).toEqual(wantedAnswer);
        });

        it(`should return empty string when given unknown question type`, async () => {
            const wantedAnswer = '';

            const actualAnswer = component.getQuestionTypeText('what is this');

            expect(actualAnswer).toEqual(wantedAnswer);
        });
    });

    describe(`allQuestionsAnswered`, () => {
        it(`should return true when there is no task`, async () => {
            const wantedAnswer = true;
            component.task = null;

            const actualAnswer = component.allQuestionsAnswered();

            expect(actualAnswer).toEqual(wantedAnswer);
        });

        it(`should return true when all questions answered`, async () => {
            const wantedAnswer = true;
            for (let i = 0; i < component.task.questions.length; i++) {
                component.task.questions[i].answer = 'yes';
            }

            const actualAnswer = component.allQuestionsAnswered();

            expect(actualAnswer).toEqual(wantedAnswer);
        });

        it(`should return false when not all questions answered`, async () => {
            const wantedAnswer = false;
            for (let i = 0; i < component.task.questions.length; i++) {
                component.task.questions[i].answer = null;
            }

            const actualAnswer = component.allQuestionsAnswered();

            expect(actualAnswer).toEqual(wantedAnswer);
        });
    });

    describe(`onClickBackButton`, () => {
        it(`should open dialog when there is a task`, async () => {
            const dialog: MatDialog = TestBed.get(MatDialog);
            const dialogOpenSpy = spyOn<any>(dialog, 'open').and
                .returnValue({afterClosed: () => of({cancel: true})});

            while (typeof component.activeUser == 'undefined') {
                await wait(10);
            }
            component.onClickBackButton();

            expect(dialogOpenSpy).toHaveBeenCalled();
        });

        it(`should cancel task when dialog closes with cancel true`, async () => {
            const service: ValidateManagerService = TestBed.get(ValidateManagerService);
            const cancelTaskSpy = spyOn<any>(service, 'cancelTask').and.callThrough();
            const dialog: MatDialog = TestBed.get(MatDialog);
            const dialogOpenSpy = spyOn<any>(dialog, 'open').and
                .returnValue({afterClosed: () => of({cancel: true})});

            while (typeof component.activeUser == 'undefined') {
                await wait(10);
            }
            component.onClickBackButton();

            expect(cancelTaskSpy).toHaveBeenCalled();
        });

        it(`should not cancel task when dialog closes with cancel false`, async () => {
            const service: ValidateManagerService = TestBed.get(ValidateManagerService);
            const cancelTaskSpy = spyOn<any>(service, 'cancelTask').and.callThrough();
            const dialog: MatDialog = TestBed.get(MatDialog);
            const dialogOpenSpy = spyOn<any>(dialog, 'open').and
                .returnValue({afterClosed: () => of({cancel: false})});

            while (typeof component.activeUser == 'undefined') {
                await wait(10);
            }
            component.onClickBackButton();

            expect(cancelTaskSpy).not.toHaveBeenCalled();
        });
    });

    it(`should have the given path on 'selectedProductImageUrl' when 'onSelectImage' is called`, async () => {
        const path = 'test/path';

        component.onSelectImage(path);

        expect(component.selectedProductImageUrl).toBe(path);
    });

    it(`should open lightbox when 'zooming' is called`, async () => {
        const service: Lightbox = TestBed.get(Lightbox);
        const lightboxOpenSpy = spyOn<any>(service, 'open').and.callThrough();
        const items = fixture.debugElement.queryAll(By.css('.question-summary'));
        items[1].triggerEventHandler('click', null);
        fixture.detectChanges();

        component.zooming();

        expect(lightboxOpenSpy).toHaveBeenCalled();
    });

});

describe(`probe task`, () => {

    let component: ValidateComponent;
    let fixture: ComponentFixture<ValidateComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ValidateComponent,
                HeaderTitleComponent,
                TransitionGroupComponent,
                MegazordCanvasDirective,
                RelativeStopwatchComponent,
                YesNoCantTellComponent,
                YesNoComponent,
                HotkeyDirective
            ],
            imports: [
                BrowserModule,
                AppRoutingModule,
                HttpClientModule,
                BrowserAnimationsModule,
                MatDialogModule,
                FormsModule,
                DragDropModule,
                LightboxModule,
                MatRadioModule,
                MatInputModule
            ],
            providers: [CookieService,
                MegazordCanvasDirective,
                {
                    provide: ElementRef,
                    useClass: MockElementRef
                },
                {
                    provide: UserService,
                    useClass: UserServiceMock
                },
                {
                    provide: ActivatedRoute,
                    useClass: ActivatedRouteMock
                },
                {
                    provide: ValidateManagerService,
                    useClass: ValidateManagerMock
                },
                {
                    provide: MatDialog,
                    useClass: MatDialogMock
                }
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ValidateComponent);
        component = fixture.componentInstance;
        const rowTask = cloneDeep(ROWTASK);
        rowTask.sceneData = undefined;
        rowTask.probeData  =  {
                                    probeUrl: "http://traxus.s3.amazonaws.com/shufersalil/Probe_Images/20200911/4/20200911100050-caf5e074-1c0a-4f17-8186-f23e26af7ff1",
                                    creationTime: "2019-09-04 12:09:51",
                                    localImageTime: "2019-09-04 12:03:06",
                                    probeId: 2187121,
                                    originalWidth: 2592,
                                    originalHeight: 1944
                             };

        component.task = new Task(rowTask);
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('`should show task`', async () => {
        const items = fixture.debugElement.queryAll(By.css('.question-summary'));

        expect(items.length).toBeGreaterThan(0);
    });

    it('should call ValidateManagerService getQueueCount every 10 seconds', fakeAsync(() => {
        const service: ValidateManagerService = TestBed.get(ValidateManagerService);
        const getQueueCountSpy = spyOn<any>(service, 'getQueueCount').and.callThrough();
        fixture = TestBed.createComponent(ValidateComponent);
        component = fixture.componentInstance;
        component.task = task;
        fixture.detectChanges();

        expect(getQueueCountSpy).toHaveBeenCalledTimes(1);
        tick(10000);
        expect(getQueueCountSpy).toHaveBeenCalledTimes(2);
        tick(20000);
        expect(getQueueCountSpy).toHaveBeenCalledTimes(4);
        tick(100000);
        expect(getQueueCountSpy).toHaveBeenCalledTimes(14);

        // need this to clear the setInterval tasks
        discardPeriodicTasks();
        tick(1000);
        discardPeriodicTasks();
    }));

    it('selectedLastSeenUrl should return the correct url', async () => {
        const items = fixture.debugElement.queryAll(By.css('.question-summary'));
        items[1].triggerEventHandler('click', null);
        const wantedUrl =
            `${environment.config.xsuite}${component.task.projectName}/explore/scene/${component.selectedQuestion.lastSeenSceneId}?product=${component.selectedQuestion.productId}`;

        const actualUrl = component.selectedLastSeenUrl;

        expect(actualUrl).toEqual(wantedUrl);
    });

    describe('Pressing hotkey', () => {
        describe(`'y'`, () => {
            it(`should trigger onAnswer 'yes'`, async () => {
                const spy = spyOn<any>(component, 'onAnswer').and.callThrough();
                const wantedArg = 'yes';
                const items = fixture.debugElement.queryAll(By.css('.question-summary'));
                items[1].triggerEventHandler('click', null);
                fixture.detectChanges();
                triggerKeyPress('y');
                fixture.detectChanges();

                expect(spy).toHaveBeenCalledWith(wantedArg);
            });
        });

        describe(`'n'`, () => {
            it(`should trigger onAnswer 'no'`, async () => {
                const spy = spyOn<any>(component, 'onAnswer').and.callThrough();
                const wantedArg = 'no';
                const items = fixture.debugElement.queryAll(By.css('.question-summary'));
                items[1].triggerEventHandler('click', null);
                fixture.detectChanges();
                triggerKeyPress('n');
                fixture.detectChanges();

                expect(spy).toHaveBeenCalledWith(wantedArg);
            });
        });

        describe(`'s'`, () => {
            it(`should trigger onChangeProbeSceneMode 'scene'`, async () => {
                const spy = spyOn<any>(component, 'onChangeProbeSceneMode').and.callThrough();
                const wantedArg = 'probes';
                const items = fixture.debugElement.queryAll(By.css('.question-summary'));
                items[1].triggerEventHandler('click', null);
                fixture.detectChanges();

                triggerKeyPress('s');
                fixture.detectChanges();

                expect(spy).toHaveBeenCalledWith(wantedArg);
            });
        });

        describe(`'p'`, () => {
            it(`should trigger onChangeProbeSceneMode 'probes'`, async () => {
                const spy = spyOn<any>(component, 'onChangeProbeSceneMode').and.callThrough();
                const wantedArg = 'probes';
                const items = fixture.debugElement.queryAll(By.css('.question-summary'));
                items[0].triggerEventHandler('click', null);
                fixture.detectChanges();

                triggerKeyPress('p');
                fixture.detectChanges();

                expect(spy).toHaveBeenCalledWith(wantedArg);
            });
        });

        describe(`'leftArrow'`, () => {
            it(`should change 'arrowKeysAffectedList' from 'probes' to 'questions'`, async () => {
                component.arrowKeysAffectedList = 'probes';
                const wantedValue = 'questions';
                const items = fixture.debugElement.queryAll(By.css('.question-summary'));
                items[1].triggerEventHandler('click', null);
                fixture.detectChanges();
                triggerKeyPress('ArrowLeft');
                fixture.detectChanges();

                expect(component.arrowKeysAffectedList).toEqual(wantedValue);
            });

            it(`should not change 'arrowKeysAffectedList' if it was already set to 'questions'`, async () => {
                component.arrowKeysAffectedList = 'questions';
                const wantedValue = 'questions';
                const items = fixture.debugElement.queryAll(By.css('.question-summary'));
                items[1].triggerEventHandler('click', null);
                fixture.detectChanges();
                triggerKeyPress('ArrowLeft');
                fixture.detectChanges();

                expect(component.arrowKeysAffectedList).toEqual(wantedValue);
            });
        });

        describe(`'rightArrow'`, () => {
            it(`should change 'arrowKeysAffectedList' from 'questions' to 'probes'`, async () => {
                component.arrowKeysAffectedList = 'questions';
                const wantedValue = 'probes';
                const items = fixture.debugElement.queryAll(By.css('.question-summary'));
                items[1].triggerEventHandler('click', null);
                fixture.detectChanges();
                triggerKeyPress('ArrowRight');
                fixture.detectChanges();

                expect(component.arrowKeysAffectedList).toEqual(wantedValue);
            });

            it(`should not change 'arrowKeysAffectedList' if it was already set to 'probes'`, async () => {
                component.arrowKeysAffectedList = 'probes';
                const wantedValue = 'probes';
                const items = fixture.debugElement.queryAll(By.css('.question-summary'));
                items[1].triggerEventHandler('click', null);
                fixture.detectChanges();
                triggerKeyPress('ArrowRight');

                fixture.detectChanges();

                expect(component.arrowKeysAffectedList).toEqual(wantedValue);
            });
        });

        describe(`'upArrow'`, () => {
            it(`should not call 'questionOnClick' nor 'onChangeSelectedProbe' when there is no selected question`, async () => {
                const questionOnClickSpy = spyOn<any>(component, 'questionOnClick').and.callThrough();
                const onChangeSelectedProbeSpy = spyOn<any>(component, 'onChangeSelectedProbe').and.callThrough();
                component.clearSelectedQuestion();
                fixture.detectChanges();
                triggerKeyPress('ArrowUp');
                fixture.detectChanges();

                expect(questionOnClickSpy).not.toHaveBeenCalled();
                expect(onChangeSelectedProbeSpy).not.toHaveBeenCalled();
            });

            it(`should call 'questionOnClick' but not 'onChangeSelectedProbe' when 'arrowKeysAffectedList' is set to 'questions'`, async () => {
                triggerKeyPress('s');
                await wait(100);
                fixture.detectChanges();
                const items = fixture.debugElement.queryAll(By.css('.question-summary'));
                items[1].triggerEventHandler('click', null);
                fixture.detectChanges();
                const questionOnClickSpy = spyOn<any>(component, 'questionOnClick').and.callThrough();
                const onChangeSelectedProbeSpy = spyOn<any>(component, 'onChangeSelectedProbe').and.callThrough();
                triggerKeyPress('ArrowUp');
                fixture.detectChanges();

                expect(questionOnClickSpy).toHaveBeenCalledTimes(1);
                expect(onChangeSelectedProbeSpy).not.toHaveBeenCalled();
            });

            it(`should not call 'questionOnClick' nor 'onChangeSelectedProbe' when the first question is selected and 'arrowKeysAffectedList' is set to 'questions'`, async () => {
                triggerKeyPress('s');
                await wait(100);
                fixture.detectChanges();
                const items = fixture.debugElement.queryAll(By.css('.question-summary'));
                items[0].triggerEventHandler('click', null);
                fixture.detectChanges();
                const questionOnClickSpy = spyOn<any>(component, 'questionOnClick').and.callThrough();
                const onChangeSelectedProbeSpy = spyOn<any>(component, 'onChangeSelectedProbe').and.callThrough();
                triggerKeyPress('ArrowUp');
                fixture.detectChanges();

                expect(questionOnClickSpy).not.toHaveBeenCalled();
                expect(onChangeSelectedProbeSpy).not.toHaveBeenCalled();
            });

            it(`should call 'onChangeSelectedProbe' but not 'questionOnClick' when 'arrowKeysAffectedList' is set to 'probes'`, async () => {
                const items = fixture.debugElement.queryAll(By.css('.question-summary'));
                items[1].triggerEventHandler('click', null);
                fixture.detectChanges();
                triggerKeyPress('p');
                await wait(100);
                fixture.detectChanges();
                triggerKeyPress('ArrowDown');
                await wait(100);
                fixture.detectChanges();
                const questionOnClickSpy = spyOn<any>(component, 'questionOnClick').and.callThrough();
                const onChangeSelectedProbeSpy = spyOn<any>(component, 'onChangeSelectedProbe').and.callThrough();
                triggerKeyPress('ArrowUp');
                fixture.detectChanges();

                expect(questionOnClickSpy).not.toHaveBeenCalled();
                expect(onChangeSelectedProbeSpy).not.toHaveBeenCalledTimes(1);
            });

            it(`should not call 'questionOnClick' nor 'onChangeSelectedProbe' when the first probe is selected and 'arrowKeysAffectedList' is set to 'probes'`, async () => {
                const items = fixture.debugElement.queryAll(By.css('.question-summary'));
                items[1].triggerEventHandler('click', null);
                fixture.detectChanges();
                triggerKeyPress('p');
                await wait(100);
                fixture.detectChanges();
                const questionOnClickSpy = spyOn<any>(component, 'questionOnClick').and.callThrough();
                const onChangeSelectedProbeSpy = spyOn<any>(component, 'onChangeSelectedProbe').and.callThrough();
                triggerKeyPress('ArrowUp');
                fixture.detectChanges();

                expect(questionOnClickSpy).not.toHaveBeenCalled();
                expect(onChangeSelectedProbeSpy).not.toHaveBeenCalled();
            });
        });

        describe(`'downArrow'`, () => {
            it(`should not call 'questionOnClick' nor 'onChangeSelectedProbe' when there is no selected question`, async () => {
                const questionOnClickSpy = spyOn<any>(component, 'questionOnClick').and.callThrough();
                const onChangeSelectedProbeSpy = spyOn<any>(component, 'onChangeSelectedProbe').and.callThrough();
                component.clearSelectedQuestion();
                fixture.detectChanges();
                triggerKeyPress('ArrowDown');
                fixture.detectChanges();

                expect(questionOnClickSpy).not.toHaveBeenCalled();
                expect(onChangeSelectedProbeSpy).not.toHaveBeenCalled();
            });

            it(`should call 'questionOnClick' but not 'onChangeSelectedProbe' when 'arrowKeysAffectedList' is set to 'questions'`, async () => {
                triggerKeyPress('s');
                await wait(100);
                fixture.detectChanges();
                const items = fixture.debugElement.queryAll(By.css('.question-summary'));
                items[1].triggerEventHandler('click', null);
                fixture.detectChanges();
                const questionOnClickSpy = spyOn<any>(component, 'questionOnClick').and.callThrough();
                const onChangeSelectedProbeSpy = spyOn<any>(component, 'onChangeSelectedProbe').and.callThrough();

                triggerKeyPress('ArrowDown');
                fixture.detectChanges();

                expect(questionOnClickSpy).toHaveBeenCalledTimes(1);
                expect(onChangeSelectedProbeSpy).not.toHaveBeenCalled();
            });

            it(`should not call 'questionOnClick' nor 'onChangeSelectedProbe' when the last question is selected and 'arrowKeysAffectedList' is set to 'questions'`, async () => {
                triggerKeyPress('s');
                await wait(100);
                fixture.detectChanges();
                const items = fixture.debugElement.queryAll(By.css('.question-summary'));
                items[items.length - 1].triggerEventHandler('click', null);
                fixture.detectChanges();
                const questionOnClickSpy = spyOn<any>(component, 'questionOnClick').and.callThrough();
                const onChangeSelectedProbeSpy = spyOn<any>(component, 'onChangeSelectedProbe').and.callThrough();
                triggerKeyPress('ArrowDown');
                fixture.detectChanges();

                expect(questionOnClickSpy).not.toHaveBeenCalled();
                expect(onChangeSelectedProbeSpy).not.toHaveBeenCalled();
            });

            it(`should not call 'questionOnClick' nor 'onChangeSelectedProbe' when the last probe is selected and 'arrowKeysAffectedList' is set to 'probes'`, async () => {
                const items = fixture.debugElement.queryAll(By.css('.question-summary'));
                items[1].triggerEventHandler('click', null);
                fixture.detectChanges();
                triggerKeyPress('p');
                await wait(100);
                fixture.detectChanges();
                component.onChangeSelectedProbe(component.task.probesMegazordData[component.task.probesMegazordData.length - 1]);
                await wait(100);
                fixture.detectChanges();
                const questionOnClickSpy = spyOn<any>(component, 'questionOnClick').and.callThrough();
                const onChangeSelectedProbeSpy = spyOn<any>(component, 'onChangeSelectedProbe').and.callThrough();
                triggerKeyPress('ArrowDown');
                fixture.detectChanges();
                expect(questionOnClickSpy).not.toHaveBeenCalled();
                expect(onChangeSelectedProbeSpy).not.toHaveBeenCalled();
            });
        });
    });

    describe(`'finishTask'`, () => {
        it(`should clear data`, async () => {
            const clearSelectedQuestionSpy = spyOn<any>(component, 'clearSelectedQuestion').and.callThrough();
            const openBetweenItemsDialogSpy = spyOn<any>(component, 'openBetweenItemsDialog').and.stub();

            component.finishTask();

            expect(component.task).toBe(null);
            expect(component.selectedProbe).toBe(null);
            expect(component.initialScale).toBe(null);
            expect(component.selectedProbeMasks.length).toBe(0);
            expect(clearSelectedQuestionSpy).toHaveBeenCalled();
        });

        it(`should stop inactivity timer`, async () => {
            const service: InactivityTimerService = TestBed.get(InactivityTimerService);
            const inactivityTimerStopSpy = spyOn<any>(service, 'stop').and.stub();
            const openBetweenItemsDialogSpy = spyOn<any>(component, 'openBetweenItemsDialog').and.stub();

            component.finishTask();

            expect(inactivityTimerStopSpy).toHaveBeenCalled();
        });

        it(`should open the next item dialog after`, async () => {
            const openBetweenItemsDialogSpy = spyOn<any>(component, 'openBetweenItemsDialog').and.stub();

            component.finishTask();

            expect(openBetweenItemsDialogSpy).toHaveBeenCalled();
        });
    });

    describe(`'cancelTask'`, () => {
        it(`should call 'cancelTask' from ValidateManagerService`, async () => {
            const service: ValidateManagerService = TestBed.get(ValidateManagerService);
            const cancelTaskSpy = spyOn<any>(service, 'cancelTask').and.callThrough();
            const openBetweenItemsDialogSpy = spyOn<any>(component, 'openBetweenItemsDialog').and.stub();

            component.cancelTask('test');

            expect(cancelTaskSpy).toHaveBeenCalled();
        });

        it(`should call 'finishTask'`, async () => {
            const finishTaskSpy = spyOn<any>(component, 'finishTask').and.callThrough();
            const openBetweenItemsDialogSpy = spyOn<any>(component, 'openBetweenItemsDialog').and.stub();

            component.cancelTask('test');

            expect(finishTaskSpy).toHaveBeenCalled();
        });
    });

    describe(`'saveTask'`, () => {
        it(`should call 'saveTask' from ValidateManagerService`, async () => {
            const service: ValidateManagerService = TestBed.get(ValidateManagerService);
            const saveTaskSpy = spyOn<any>(service, 'saveTask').and.callThrough();
            const openBetweenItemsDialogSpy = spyOn<any>(component, 'openBetweenItemsDialog').and.stub();

            component.saveTask();

            expect(saveTaskSpy).toHaveBeenCalled();
        });

        it(`should call 'finishTask'`, async () => {
            const finishTaskSpy = spyOn<any>(component, 'finishTask').and.callThrough();
            const openBetweenItemsDialogSpy = spyOn<any>(component, 'openBetweenItemsDialog').and.stub();

            component.saveTask();

            expect(finishTaskSpy).toHaveBeenCalled();
        });
    });

    describe(`Zooming`, () => {
        it(`should emit a zoom event when 'onZoomIn' is called`, async () => {
            const zoomEventSpy = spyOn<any>(component.zoomEvent, 'emit').and.stub();

            component.onZoomIn();

            expect(zoomEventSpy).toHaveBeenCalled();
        });

        it(`should emit a zoom event when 'onZoomOut' is called`, async () => {
            const zoomEventSpy = spyOn<any>(component.zoomEvent, 'emit').and.stub();

            component.onZoomOut();

            expect(zoomEventSpy).toHaveBeenCalled();
        });

        it(`should emit a fitToScreenEvent event when 'onFitToScreen' is called`, async () => {
            const fitToScreenEventSpy = spyOn<any>(component.fitToScreenEvent, 'emit').and.stub();

            component.onFitToScreen();

            expect(fitToScreenEventSpy).toHaveBeenCalled();
        });

        it(`should not zoom to selected mask after question is clicked if megazord does not exist`, async () => {
            const items = fixture.debugElement.queryAll(By.css('.question-summary'));
            items[1].triggerEventHandler('click', null);
            const directive: MegazordCanvasDirective = TestBed.get(MegazordCanvasDirective);
            const moveFrustumToSpy = spyOn<any>(directive, 'moveFrustumTo').and.stub();
            component.megazordCanvas = null;

            items[2].triggerEventHandler('click', null);

            expect(moveFrustumToSpy).not.toHaveBeenCalled();
        });
    });

    describe(`onAnswer`, () => {
        it(`should clear selected question`, async () => {
            const clearSelectedQuestionSpy = spyOn<any>(component, 'clearSelectedQuestion').and.callThrough();
            const items = fixture.debugElement.queryAll(By.css('.question-summary'));
            items[1].triggerEventHandler('click', null);
            for (let i = 0; i < component.task.questions.length; i++) {
                component.task.questions[i].answer = null;
            }
            fixture.detectChanges();

            triggerKeyPress('n');
            fixture.detectChanges();

            expect(clearSelectedQuestionSpy).toHaveBeenCalled();
        });

        it(`should not clear selected question if there are no more questions to answer`, async () => {
            const clearSelectedQuestionSpy = spyOn<any>(component, 'clearSelectedQuestion').and.callThrough();
            const items = fixture.debugElement.queryAll(By.css('.question-summary'));
            items[1].triggerEventHandler('click', null);
            for (let i = 0; i < component.task.questions.length; i++) {
                component.task.questions[i].answer = 'no';
            }
            fixture.detectChanges();

            triggerKeyPress('n');
            fixture.detectChanges();

            expect(clearSelectedQuestionSpy).not.toHaveBeenCalled();
        });
    });

    describe(`getQuestionTypeText`, () => {
        it(`should return 'Yes / No' when given 'YesNo' question type`, async () => {
            const wantedAnswer = 'Yes / No';

            const actualAnswer = component.getQuestionTypeText('YesNo');

            expect(actualAnswer).toEqual(wantedAnswer);
        });

        it(`should return empty string when given unknown question type`, async () => {
            const wantedAnswer = '';

            const actualAnswer = component.getQuestionTypeText('what is this');

            expect(actualAnswer).toEqual(wantedAnswer);
        });
    });

    describe(`allQuestionsAnswered`, () => {
        it(`should return true when there is no task`, async () => {
            const wantedAnswer = true;
            component.task = null;

            const actualAnswer = component.allQuestionsAnswered();

            expect(actualAnswer).toEqual(wantedAnswer);
        });

        it(`should return true when all questions answered`, async () => {
            const wantedAnswer = true;
            for (let i = 0; i < component.task.questions.length; i++) {
                component.task.questions[i].answer = 'yes';
            }

            const actualAnswer = component.allQuestionsAnswered();

            expect(actualAnswer).toEqual(wantedAnswer);
        });

        it(`should return false when not all questions answered`, async () => {
            const wantedAnswer = false;
            for (let i = 0; i < component.task.questions.length; i++) {
                component.task.questions[i].answer = null;
            }

            const actualAnswer = component.allQuestionsAnswered();

            expect(actualAnswer).toEqual(wantedAnswer);
        });
    });

    describe(`onClickBackButton`, () => {
        it(`should open dialog when there is a task`, async () => {
            const dialog: MatDialog = TestBed.get(MatDialog);
            const dialogOpenSpy = spyOn<any>(dialog, 'open').and
                .returnValue({afterClosed: () => of({cancel: true})});

            while (typeof component.activeUser == 'undefined') {
                await wait(10);
            }
            component.onClickBackButton();

            expect(dialogOpenSpy).toHaveBeenCalled();
        });

        it(`should cancel task when dialog closes with cancel true`, async () => {
            const service: ValidateManagerService = TestBed.get(ValidateManagerService);
            const cancelTaskSpy = spyOn<any>(service, 'cancelTask').and.callThrough();
            const dialog: MatDialog = TestBed.get(MatDialog);
            const dialogOpenSpy = spyOn<any>(dialog, 'open').and
                .returnValue({afterClosed: () => of({cancel: true})});

            while (typeof component.activeUser == 'undefined') {
                await wait(10);
            }
            component.onClickBackButton();

            expect(cancelTaskSpy).toHaveBeenCalled();
        });

        it(`should not cancel task when dialog closes with cancel false`, async () => {
            const service: ValidateManagerService = TestBed.get(ValidateManagerService);
            const cancelTaskSpy = spyOn<any>(service, 'cancelTask').and.callThrough();
            const dialog: MatDialog = TestBed.get(MatDialog);
            const dialogOpenSpy = spyOn<any>(dialog, 'open').and
                .returnValue({afterClosed: () => of({cancel: false})});

            while (typeof component.activeUser == 'undefined') {
                await wait(10);
            }
            component.onClickBackButton();

            expect(cancelTaskSpy).not.toHaveBeenCalled();
        });
    });

    it(`should have the given path on 'selectedProductImageUrl' when 'onSelectImage' is called`, async () => {
        const path = 'test/path';

        component.onSelectImage(path);

        expect(component.selectedProductImageUrl).toBe(path);
    });

    it(`should open lightbox when 'zooming' is called`, async () => {
        const service: Lightbox = TestBed.get(Lightbox);
        const lightboxOpenSpy = spyOn<any>(service, 'open').and.callThrough();
        const items = fixture.debugElement.queryAll(By.css('.question-summary'));
        items[1].triggerEventHandler('click', null);
        fixture.detectChanges();

        component.zooming();

        expect(lightboxOpenSpy).toHaveBeenCalled();
    });


    it(`should not show probe preview`, async () => {
        const items = fixture.debugElement.queryAll(By.css('.probes-preview-panel-summary'));
        expect(items.length).toEqual(0);
    });

    it(`should not show scene/probe switching`, async () => {
        const items = fixture.debugElement.queryAll(By.css('.scene-probes-switch'));
        expect(items.length).toEqual(0);
    });
});

describe('ValidatePriceComponent', () => {
    let component: ValidateComponent;
    let fixture: ComponentFixture<ValidateComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ValidateComponent,
                HeaderTitleComponent,
                TransitionGroupComponent,
                MegazordCanvasDirective,
                RelativeStopwatchComponent,
                YesNoCantTellComponent,
                YesNoComponent,
                HotkeyDirective
            ],
            imports: [
                BrowserModule,
                AppRoutingModule,
                HttpClientModule,
                BrowserAnimationsModule,
                MatDialogModule,
                FormsModule,
                DragDropModule,
                LightboxModule,
                MatRadioModule,
                MatInputModule
            ],
            providers: [CookieService,
                MegazordCanvasDirective,
                {
                    provide: ElementRef,
                    useClass: MockElementRef
                },
                {
                    provide: UserService,
                    useClass: UserServiceMock
                },
                {
                    provide: ActivatedRoute,
                    useClass: ActivatedRouteMock
                },
                {
                    provide: ValidateManagerService,
                    useClass: ValidateManagerMock
                },
                {
                    provide: MatDialog,
                    useClass: MatDialogMock
                }
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ValidateComponent);
        component = fixture.componentInstance;
        const rowTask = cloneDeep(ROWTASK);
        rowTask.questions[0].question_type = 'YesNoCantTell';
        rowTask.questions[0].value = '1.1';
        rowTask.questions[0].question_category = 'PriceApproval';

        rowTask.questions[1].question_type = 'YesNoCantTell';
        rowTask.questions[1].question_category = 'PriceApproval';
        rowTask.questions[1].value = '1.2';
        rowTask.questions[1].return_fixed_value = true;

        rowTask.questions[2].value = '1.3';
        rowTask.questions[2].question_type = 'YesNo';
        rowTask.questions[2].question_category = 'PriceApproval';
        rowTask.questions[2].return_fixed_value = true;
        component.task = new Task(rowTask);
        fixture.detectChanges();
    });

    it('should show task yes no cant tell without return value', () => {
        const items = fixture.debugElement.queryAll(By.css('.question-summary'));
        items[0].triggerEventHandler('click', null);
        fixture.detectChanges();
        const buttons = fixture.debugElement.queryAll(By.directive(YesNoCantTellComponent));
        expect(buttons.length).toEqual(1);
        const radioButtons = fixture.debugElement.queryAll(By.directive(MatRadioButton));
        expect(radioButtons.length).toEqual(3);
        expect(radioButtons[0].nativeElement.textContent).toEqual("Yes, the price is $ 1.1");
        expect(radioButtons[1].nativeElement.textContent).toEqual("There is no price tag");
        expect(radioButtons[2].nativeElement.textContent).toEqual("No, the price is incorrect");
    });

    it('should show task yes no cant tell with return value', () => {
        const items = fixture.debugElement.queryAll(By.css('.question-summary'));
        items[1].triggerEventHandler('click', null);
        fixture.detectChanges();
        const buttons = fixture.debugElement.queryAll(By.directive(YesNoCantTellComponent));
        expect(buttons.length).toEqual(1);
        const radioButtons = fixture.debugElement.queryAll(By.directive(MatRadioButton));
        expect(radioButtons.length).toEqual(3);
        expect(radioButtons[0].nativeElement.textContent).toEqual("Yes, the price is $ 1.2");
        expect(radioButtons[1].nativeElement.textContent).toEqual("There is no price tag");
        expect(radioButtons[2].nativeElement.textContent).toEqual('No, the price is');
    });

    it('should show task yes no with return value', () => {
        const items = fixture.debugElement.queryAll(By.css('.question-summary'));
        items[2].triggerEventHandler('click', null);
        fixture.detectChanges();
        const buttons = fixture.debugElement.queryAll(By.directive(YesNoCantTellComponent));
        expect(buttons.length).toEqual(1);
        const radioButtons = fixture.debugElement.queryAll(By.directive(MatRadioButton));
        expect(radioButtons.length).toEqual(2);
        expect(radioButtons[0].nativeElement.textContent).toEqual("Yes, the price is $ 1.3");
        expect(radioButtons[1].nativeElement.textContent).toEqual('No, the price is');
    });
});

describe('Validate LowFacing question', () => {
    let component: ValidateComponent;
    let fixture: ComponentFixture<ValidateComponent>;
    const productName = 'productName';

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ValidateComponent,
                HeaderTitleComponent,
                TransitionGroupComponent,
                MegazordCanvasDirective,
                RelativeStopwatchComponent,
                YesNoCantTellComponent,
                YesNoComponent,
                HotkeyDirective
            ],
            imports: [
                BrowserModule,
                AppRoutingModule,
                HttpClientModule,
                BrowserAnimationsModule,
                MatDialogModule,
                FormsModule,
                DragDropModule,
                LightboxModule,
                MatRadioModule,
                MatInputModule
            ],
            providers: [CookieService,
                MegazordCanvasDirective,
                {
                    provide: ElementRef,
                    useClass: MockElementRef
                },
                {
                    provide: UserService,
                    useClass: UserServiceMock
                },
                {
                    provide: ActivatedRoute,
                    useClass: ActivatedRouteMock
                },
                {
                    provide: ValidateManagerService,
                    useClass: ValidateManagerMock
                },
                {
                    provide: MatDialog,
                    useClass: MatDialogMock
                }
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ValidateComponent);
        component = fixture.componentInstance;
        const rowTask = cloneDeep(ROWTASK);
        rowTask.questions[0].question_type = 'YesNoCantTell';
        rowTask.questions[0].value = '11';
        rowTask.questions[0].question_category = 'LowFacingApproval';

        rowTask.questions[1].question_type = 'YesNoCantTell';
        rowTask.questions[1].question_category = 'LowFacingApproval';
        rowTask.questions[1].value = '12';
        rowTask.questions[1].return_fixed_value = true;

        rowTask.questions[2].value = '13';
        rowTask.questions[2].question_type = 'YesNo';
        rowTask.questions[2].question_category = 'LowFacingApproval';
        rowTask.questions[2].return_fixed_value = true;
        component.task = new Task(rowTask);
        fixture.detectChanges();
    });

    it('should show task yes no cant tell without return value', () => {
        const lowFacingYesNo = new LowFacingApprovalYesNoCantTell(productName, "11");
        const items = fixture.debugElement.queryAll(By.css('.question-summary'));
        items[0].triggerEventHandler('click', null);
        fixture.detectChanges();
        const buttons = fixture.debugElement.queryAll(By.directive(YesNoCantTellComponent));
        expect(buttons.length).toEqual(1);
        const radioButtons = fixture.debugElement.queryAll(By.directive(MatRadioButton));
        expect(radioButtons.length).toEqual(3);
        expect(radioButtons[0].nativeElement.textContent).toEqual(lowFacingYesNo.yesAnswer);
        expect(radioButtons[1].nativeElement.textContent).toEqual(lowFacingYesNo.cantTellAnswer);
        expect(radioButtons[2].nativeElement.textContent).toEqual(lowFacingYesNo.noAnswer);
    });

    it('should show task yes no cant tell with return value', () => {
        const lowFacingYesNo = new LowFacingApprovalYesNoCantTell(productName, "12");
        const items = fixture.debugElement.queryAll(By.css('.question-summary'));
        items[1].triggerEventHandler('click', null);
        fixture.detectChanges();
        const buttons = fixture.debugElement.queryAll(By.directive(YesNoCantTellComponent));
        expect(buttons.length).toEqual(1);
        const radioButtons = fixture.debugElement.queryAll(By.directive(MatRadioButton));
        expect(radioButtons.length).toEqual(3);
        expect(radioButtons[0].nativeElement.textContent).toEqual(lowFacingYesNo.yesAnswer);
        expect(radioButtons[1].nativeElement.textContent).toEqual(lowFacingYesNo.cantTellAnswer);
        expect(radioButtons[2].nativeElement.textContent).toEqual(lowFacingYesNo.noValueAnswer);
    });

    it('should show task yes no with return value', () => {
        const lowFacingYesNo = new LowFacingApprovalYesNo(productName, "13");
        const items = fixture.debugElement.queryAll(By.css('.question-summary'));
        items[2].triggerEventHandler('click', null);
        fixture.detectChanges();
        const buttons = fixture.debugElement.queryAll(By.directive(YesNoCantTellComponent));
        expect(buttons.length).toEqual(1);
        const radioButtons = fixture.debugElement.queryAll(By.directive(MatRadioButton));
        expect(radioButtons.length).toEqual(2);
        expect(radioButtons[0].nativeElement.textContent).toEqual(lowFacingYesNo.yesAnswer);
        expect(radioButtons[1].nativeElement.textContent).toEqual(lowFacingYesNo.noValueAnswer);
    });
});
