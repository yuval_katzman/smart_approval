import {
    Component,
    EventEmitter,
    Input,
    Output,
    OnInit,
    ViewChild,
    ChangeDetectorRef,
    OnDestroy,
    AfterViewInit
} from '@angular/core';
import {YesNoCantTellQuestion, YesNoQuestion} from "../../../models/task-details/interface";
import {Answer, AnswerOptions} from 'src/app/models/task';
import {MatInput} from "@angular/material/input";
import {fromEvent, Subscription} from "rxjs";
import {MatRadioButton} from "@angular/material/radio";

@Component({
    selector: 'app-radio-button-answer',
    templateUrl: './yes-no-cant-tell.component.html',
    styleUrls: ['./yes-no-cant-tell.component.scss']
})
export class YesNoCantTellComponent implements OnInit, OnDestroy, AfterViewInit {
    AnswerOptions = AnswerOptions;
    @Input() question: YesNoCantTellQuestion & YesNoQuestion;
    @Input() answer: Answer;
    @Output() onAnswer = new EventEmitter<Answer>();
    @ViewChild(MatInput, {static: false}) inputEl: MatInput;
    @ViewChild('withInputRadioButton', {static: false}) withInputRadioButton: MatRadioButton;

    constructor(private ref: ChangeDetectorRef) {}

    subscription: Subscription;

    ngOnInit(): void {
        if (!this.answer) {
            this.answer = {};
        }

    }

    ngAfterViewInit(){
        if (this.withInputRadioButton) {
            this.subscription = fromEvent(this.withInputRadioButton._inputElement.nativeElement, 'click').subscribe(
                res => {
                    this.inputEl.focus();
                    this.ref.detectChanges();
                });
        }
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
}
