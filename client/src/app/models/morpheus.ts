import {mat4, vec4} from "gl-matrix";
import {forEach, isEqual} from "lodash";

export class Morpheus {
    static unitMatrix = Morpheus.unit();

    static unit() {
        return mat4.create();
    }

    static moveTo(originMoleculeMatrix, x, y) {
        const transformSeparated = Morpheus.separateTranslationFromTransform(originMoleculeMatrix);

        const inverseOfOriginalMatrix = Morpheus.inverse(transformSeparated.translationOnlyTransform);

        const translatedMatrix = Morpheus.translation(x, y);

        return Morpheus.multiply(translatedMatrix, inverseOfOriginalMatrix, originMoleculeMatrix);
    }

    static separateTranslationFromTransform(inputMatrix) {
        let translation;
        let transformWithoutTranslation;
        translation = Morpheus.translation(inputMatrix[12], inputMatrix[13]);
        transformWithoutTranslation = Morpheus.multiply(Morpheus.inverse(translation), inputMatrix);

        return {translationOnlyTransform: translation, transformWithoutTranslation};
    }

    static inverse(A) {
        const AInverse = mat4.create();
        mat4.invert(AInverse, A);
        return AInverse;
    }

    static translation(x, y) {
        const result = mat4.create();
        const translationVector = vec4.fromValues(x, y, 0, 0);
        // @ts-ignore
        mat4.translate(result, result, translationVector);
        return result;
    }

    static multiply(...args) {
        const result = Morpheus.unit();

        forEach(args, transform => {
            mat4.multiply(result, result, transform);
        });

        return result;
    }

    static isUnit(matrix) {
        if (!Morpheus.unitMatrix) {
            Morpheus.unitMatrix = Morpheus.unit();
        }
        return isEqual(matrix, Morpheus.unitMatrix);
    }

    static toClient(CVM) {
        if (!CVM) {
            return Morpheus.unit();
        }

        const arrayCVM = [
            CVM[0][0], CVM[1][0], 0, CVM[2][0],
            CVM[0][1], CVM[1][1], 0, CVM[2][1],
            0, 0, 1, 0,
            CVM[0][2], CVM[1][2], 0, CVM[2][2]
        ];

        return Morpheus.multiply(Morpheus.unit(), arrayCVM);
    }
}

