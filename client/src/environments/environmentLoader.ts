import { environment as defaultEnvironment } from './environment';

export const environmentLoader = new Promise<{
  production: boolean,
  config: {
    baseUrl: string,
    cloud: string,
    cloudsUrls: {[cloudName: string]: string},
    assetsPrefix: string
  }
}>((resolve, reject) => {
  const xmlhttp = new XMLHttpRequest(),
    method = 'GET',
    // environment.json is override by entrypoint.sh
    url = 'approvals/assets/environments/environment.json';

  xmlhttp.open(method, url, true);
  xmlhttp.onload = () => {
    // server lets client handle 404s so we will get 200 instead of 404
    if (xmlhttp.status === 200) {
      let parse;

      try {
        parse = JSON.parse(xmlhttp.responseText);
      } catch (err) {
        resolve(defaultEnvironment);
        return;
      }

      resolve(parse);
    } else {
      resolve(defaultEnvironment);
    }
  };
  xmlhttp.send();
});
