#!/usr/bin/env bash

MYSQLCMD="mysql -u root"
SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

case $TRAX_ENV in
    "DEV")
        ENV="dev"
        SUBNET="10.4.4.0/24"
        ;;
    "TEST")
        ENV="test"
        SUBNET="10.3.3.0/24"
        ;;
esac

set -e
#docker-compose -f ${SCRIPTPATH}/docker-compose.yml -f ${SCRIPTPATH}/docker-compose.${ENV}.yml down && \

echo "STARTED removing env"

if [ "$(docker ps -a -q -f name=${TRAX_ENV})" ]; then
    docker rm -f $(docker ps -a -q -f name=${TRAX_ENV})
fi

if [ "$(docker network ls | grep "trax_${ENV}")" ]; then
    ENDPOINTS=$(docker network inspect trax_${ENV} -f "{{json .Containers }}")
        if [ "$ENDPOINTS" = '{}' ]; then
            docker network rm trax_${ENV}
        fi
fi

if [ "$(docker volume ls -q -f dangling=true)" ]; then
    docker volume rm $(docker volume ls -q -f dangling=true)
fi

echo "FINISHED removing env"