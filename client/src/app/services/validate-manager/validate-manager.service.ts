import { Injectable } from '@angular/core';
import {map, orderBy} from "lodash";
import {API} from "../API/api.service";
import {Observable} from "rxjs";
import {Task, Question} from "../../models/task";
import {HttpParams} from "@angular/common/http";
import {CancelOnRefreshService} from "../cancelOnRefresh/cancel-on-refresh.service";
import {SaveTask} from "./interfaces";
import { ActivatedRoute } from "@angular/router";
@Injectable({
  providedIn: 'root'
})
export class ValidateManagerService {
  task: Task | null;
  private extendLockInterval: number;
  private taskReceiveTime;

  constructor(private api: API, private cancelOnRefresh: CancelOnRefreshService, private route: ActivatedRoute) { }

  getQueueCount(projectName: string): Observable<{count: number}> {
    const params = this.route.snapshot.queryParams;
    return this.api.get('/approvals/api/:projectName/queue-count', { pathParams: { projectName}, params});
  }

  getNextTask(projectName: string): Observable<Task | null> {
    const params = this.route.snapshot.queryParams;
    return new Observable((observer) => {
      this.api.get('/approvals/api/:projectName/next-task', {
        pathParams: {
          projectName
        },
        params
      }).subscribe((rawTask) => {
        if (!rawTask) {
          // no tasks in queue
          observer.next();
          return;
        }
        this.task = new Task(rawTask);
        this.onGettingTask();
        observer.next(this.task);
      });
    });
  }

  getTemplate(projectName: string): Observable<string>  {
    const templateParams = this.route.snapshot.queryParams;
    if (!templateParams) {return  new Observable(observer => observer.next('')); }
    if (templateParams.templateGroup) {return  new Observable(observer => observer.next(templateParams.templateGroup)); }
    if (!templateParams.templateId)  {return  new Observable(observer => observer.next('')); }
    const params = new HttpParams().append('id', templateParams.templateId);
    return new Observable((observer) => {
      this.api.get('/approvals/api/:projectName/template', {
        pathParams: {
          projectName
        },
        params
      }).subscribe(template => {
        if (!template) {
          window.location.href = ``;
        }
        observer.next(template.name);
      });
    });
  }

  cancelTask(projectName: string, selectorTaskId: string, sceneId: number, reason: string) {
    const params = new HttpParams().append('selectorTaskId', selectorTaskId);
    if (this.task) {
      // this function can be called from cancelFromRefresh and in this case there's no this.task
      // and no need to do the finishTask thing
      this.onFinishTask();
    }

    return new Observable((observer) => {
      this.api.put('/approvals/api/:projectName/cancel-task', {
        sceneId,
        reason,
        taskType: (this.task && this.task.taskType) || 'oos_approval'
      }, {
        pathParams: {
          projectName
        },
        params
      }).subscribe(() => {});
    });
  }

  getProductImages(product) {
    const result = [...product.images];
    const dvcsImages = orderBy(product.designChanges, d => new Date(d.startDate), 'desc')
        .reduce((p, c) => p.concat(c.images), []);

    return result.concat(dvcsImages).slice(0, 4);
  }

  saveTask() {
    const params = new HttpParams().append('selectorTaskId', this.task.selectorTaskId);
    const body: SaveTask = {
      originalTask: this.task.selectorTask,
      netDurationInSeconds: (Date.now() - this.taskReceiveTime) / 1000,
      queueDurationInSeconds: this.task.queueDurationInSeconds,
      wave_type: this.task.waveType,
      wave_uid: this.task.waveUid,
      project_name: this.task.projectName,
      job_type: this.task.jobType,
      session_uid: this.task.sessionUid,
      scene_uid: this.task.sceneUid,
      scene_id: this.task.sceneId,
      persistence: this.task.persistence,
      approval_id: this.task.approvalId,
      task_name: this.task.taskType,
      answers: map(this.task.questions, (question: Question) => {
        return {
          question_uid: question.questionUid,
          product_fk: question.productId,
          question_category: question.questionCategory,
          question_type: question.questionType,
          answer: question.answer,
          duration: question.duration
        };
      })
    };

    const paramsToAdd = this.route.snapshot.queryParams;
    Object.keys(paramsToAdd).forEach(key => {
      body[key] =  paramsToAdd[key];
    });
    const projectName = this.task.projectName;

    this.onFinishTask();

    return new Observable((observer) => {
      this.api.post('/approvals/api/:projectName/save-task', body, {
        pathParams: {
          projectName
        },
        params
      }).subscribe(() => {});
    }).subscribe(() => {});
  }


  private extendLock() {
    const params = new HttpParams().append('selectorTaskId', this.task.selectorTaskId);
    params.append("objectId", this.task.objectId.toString());
    this.api.put('/approvals/api/:projectName/extend-lock', {}, {
      pathParams: {
        projectName: this.task.projectName
      },
      params
    }).subscribe(() => {});
  }

  private onGettingTask() {
    this.cancelOnRefresh.setTaskToCancelOnRefresh(this.task);
    this.extendLockInterval = window.setInterval(this.extendLock.bind(this), 1000 * 60 * 3);
    this.taskReceiveTime = Date.now();
  }

  private onFinishTask() {
    this.cancelOnRefresh.dontCancelOnRefresh({
      persistence: null,
      projectName: this.task.projectName,
      selectorTaskId: this.task.selectorTaskId,
      taskType: this.task.taskType,
      queueDurationInSeconds: this.task.queueDurationInSeconds,
      objectId: this.task.objectId,
      jobType: this.task.jobType
    } as any);

    clearInterval(this.extendLockInterval);
    this.task = null;
  }
}
