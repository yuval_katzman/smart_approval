import {SmartUser} from "../defs";
import {format} from 'date-fns'

const nc = require('@trax-retail/node-core')('smart-approval');
const getEvent = function (user: SmartUser, shift_event_name: string, task_uuid?: string,
                           project_name?: string, task_type?: string, object_id?: number) {
    return {
        event_name: 'SMART_SHIFTS-MANAGER_EVENT',
        shift_event_name,
        project_name,
        user: user.staff_id,
        task_name: task_type,
        email: user.email,
        timestamp: format(new Date(), "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"),
        task: { task_type, task_uuid, project_name,  object_id }
    }
}
const sendMetric = function (eventName: string, message: any) {
    nc.cloud.topic.publish(message).catch(err => {
        nc.log.warn(`Problem to send metric event for ${eventName}. ${err}`)
    })
}

export function startTask(user: SmartUser, taskId: string, projectName: string, taskType: string, objectId: number) {
    const queryParams: any = getEvent(user, "start_task", taskId, projectName, taskType, objectId)
    queryParams.task.object_id = objectId
    queryParams.task.project_name = projectName
    sendMetric("start_task", queryParams)
}

export function finishTask(user: SmartUser, taskId, projectName: string, taskType: string, objectId: number) {
    const queryParams: any = getEvent(user, "finish_task", taskId, projectName, taskType, objectId)
    sendMetric("finish_task", queryParams)
}

export function cancelTask(user: SmartUser, reason, taskId, projectName: string, taskType: string, objectId: number) {
    const queryParams: any = getEvent(user, "cancel_task", taskId, projectName, taskType, objectId)
    queryParams.task.cancel_reason = reason
    sendMetric("cancel_task", queryParams)
}

export function breakTask(user: SmartUser, reason) {
    const queryParams: any = getEvent(user, "start_break")
    queryParams.break = {break_reason: reason}
    queryParams.task = undefined
    sendMetric("start_break", queryParams)
}

