import {
    AfterViewInit,
    Component,
    ElementRef,
    EventEmitter, OnDestroy,
    QueryList,
    ViewChildren
} from '@angular/core';
import {every, find, findIndex, forEach, orderBy} from 'lodash';
import {ActivatedRoute} from '@angular/router';
import {UserService} from '../../services/user/user.service';
import {User} from '../../models/user';
import {environment} from '../../../environments/environment';
import {ValidateManagerService} from '../../services/validate-manager/validate-manager.service';
import {MatDialog} from '@angular/material';
import {BetweenItemsComponent} from '../between-items/between-items.component';
import {combineLatest, fromEvent, Observable, Subject, Subscription} from 'rxjs';
import {Task, Probe, Question, Answer, AnswerOptions} from '../../models/task';
import {Product} from '../../models/product';
import {CancelTaskDialogComponent} from '../cancel-task-dialog/cancel-task-dialog.component';
import {InactivityTimerService} from '../../services/inactivityTimer/inactivity-timer.service';
import {first, share, takeUntil} from 'rxjs/operators';
import {animate, style, transition, trigger} from '@angular/animations';
import {ChangeDetectorRef} from '@angular/core';
import {MegazordCanvasDirective} from '../megazordCanvas/megazord-canvas.directive';
import {Lightbox} from 'ngx-lightbox';

@Component({
    selector: 'app-validate',
    templateUrl: './validate.component.html',
    styleUrls: ['./validate.component.scss'],
    animations: [
        trigger('openFromTop', [
            transition(':enter', [
                style({'max-height': '0vw'}),
                animate('0.2s ease-out', style({'max-height': '22vw'}))
            ]),
            transition(':leave', [
                style({'max-height': '22vw'}),
                animate('0.2s ease-out', style({'max-height': '0vw'}))
            ])
        ])
    ],
})
export class ValidateComponent implements OnDestroy, AfterViewInit {
    template: string;
    complete$ = new Subject<any>();
    AnswerOptions = AnswerOptions;

    get selectedImages() {
        return  this.manager.getProductImages(this.selectedProduct);
    }

    get selectedLastSeenUrl() {
        return environment.config.xsuite +
            `${this.task.projectName}/explore/scene/${this.selectedQuestion.lastSeenSceneId}?product=${this.selectedQuestion.productId}`;
    }

    constructor(private route: ActivatedRoute, private userService: UserService, private manager: ValidateManagerService,
                private dialog: MatDialog, private inactivityTimer: InactivityTimerService,
                private elem: ElementRef, private ref: ChangeDetectorRef, private lightbox: Lightbox) {
        this.homepageUrl = environment.config.baseUrl;
        this.assetsPrefix = environment.config.assetsPrefix;
        this.zoomEvent = new EventEmitter();
        this.fitToScreenEvent = new EventEmitter();
        this.probesSceneMode = 'scene';
        this.arrowKeysAffectedList = 'questions';
        this.route.params.subscribe((params) => {
            this.loading = false;
            this.activeProjectName = params.projectName;
            this.onLoad();
        });
        this.bindHotKeys();
    }

    activeProjectName: string;
    activeUser: User;
    homepageUrl: string;
    qCount: number;
    qCountObservable: Observable<number>;
    task: Task | null;
    selectedQuestion: Question | null;
    selectedProduct: Product | null;
    selectedProductImageUrl;

    masks: {
        topLeft: {
            x: number,
            y: number
        },
        bottomRight: {
            x: number,
            y: number
        }
    }[];
    loading: boolean;
    zoomEvent: EventEmitter<number>;
    fitToScreenEvent: EventEmitter<{}>;
    scale: number;
    initialScale: number;
    probesSceneMode: 'probes' | 'scene';
    selectedProbe: any;
    selectedProbeMasks: {
        topLeft: {
            x: number,
            y: number
        },
        bottomRight: {
            x: number,
            y: number
        }
    }[];
    arrowKeysAffectedList: 'questions' | 'probes';
    assetsPrefix: string;
    lastAnswerTime: number;
    canvasSubscription: any;

    @ViewChildren(MegazordCanvasDirective) megazordCanvas!: QueryList<MegazordCanvasDirective>;

    public ngAfterViewInit(): void {
        // Zoom to selected once megazord is ready
        this.canvasSubscription = this.megazordCanvas.changes.subscribe(megazord => {
            if (megazord && this.selectedQuestion) {
                this.canvasSubscription.unsubscribe();
                setTimeout(() => {
                    this.zoomToSelected();
                }, 300);
            }
        });
    }

    bindHotKeys() {
        fromEvent(document, 'keypress').pipe(
            takeUntil(this.complete$)
        ).subscribe((e: KeyboardEvent) => {
            if (!this.task.sceneData || !this.selectedQuestion) {
                return;
            }

            if (e.key === "s") {
                return this.onChangeProbeSceneMode('scene');
            }

            if (e.key === "p") {
                return this.onChangeProbeSceneMode('probes');
            }
        });

        fromEvent(document, 'keydown').pipe(
            takeUntil(this.complete$)
        ).subscribe((e: KeyboardEvent) => {
            if (e.key === "ArrowLeft") {
                if (this.arrowKeysAffectedList === 'probes') {
                    this.arrowKeysAffectedList = 'questions';
                }
                return;
            }

            if (e.key === "ArrowRight") {
                if (this.arrowKeysAffectedList === 'questions') {
                    this.arrowKeysAffectedList = 'probes';
                }
                return;
            }

            if (e.key === "ArrowUp") {
                if (!this.selectedQuestion) {
                    return;
                }
                try {
                    if (this.arrowKeysAffectedList === 'questions') {
                        this.elem.nativeElement.getElementsByClassName('questions-panel')[0].focus();
                        const selectedQuestionIndex = findIndex(this.task.questions, this.selectedQuestion);
                        if (selectedQuestionIndex !== 0) {
                            this.questionOnClick(this.task.questions[selectedQuestionIndex - 1]);
                        }
                    } else {
                        this.elem.nativeElement.getElementsByClassName('probes-preview-panel')[0].focus();
                        const selectedProbeIndex = findIndex(this.task.probesMegazordData, this.selectedProbe);
                        if (selectedProbeIndex !== 0) {
                            this.onChangeSelectedProbe(this.task.probesMegazordData[selectedProbeIndex - 1]);
                        }
                    }
                } catch (e) {
                    return;
                }
            }

            if (e.key === "ArrowDown") {
                if (!this.selectedQuestion) {
                    return;
                }
                try {
                    if (this.arrowKeysAffectedList === 'questions') {
                        this.elem.nativeElement.getElementsByClassName('questions-panel')[0].focus();
                        const selectedQuestionIndex = findIndex(this.task.questions, this.selectedQuestion);
                        if (selectedQuestionIndex !== this.task.questions.length - 1) {
                            this.questionOnClick(this.task.questions[selectedQuestionIndex + 1]);
                        }
                    } else {
                        this.elem.nativeElement.getElementsByClassName('probes-preview-panel')[0].focus();
                        const selectedProbeIndex = findIndex(this.task.probesMegazordData, this.selectedProbe);
                        if (selectedProbeIndex !== this.task.probesMegazordData.length - 1) {
                            this.onChangeSelectedProbe(this.task.probesMegazordData[selectedProbeIndex + 1]);
                        }
                    }
                } catch (e) {
                    return;
                }
            }
        });
    }

    onLoad() {
        this.qCountObservable = new Observable((observer) => {
            this.manager.getQueueCount(this.activeProjectName).subscribe((qCount) => {
                observer.next(qCount.count);
                this.qCount = qCount.count;
            });

            setInterval(() => {
                this.manager.getQueueCount(this.activeProjectName).subscribe((qCount) => {
                    observer.next(qCount.count);
                    this.qCount = qCount.count;
                });
            }, 10 * 1000);
        }).pipe(share<number>());

        // this line is required, without it we setInterval twice, I don't know why
        this.qCountObservable.subscribe(() => {
        });
        this.manager.getTemplate(this.activeProjectName).subscribe(template => {
            this.template = template;
            this.ref.markForCheck();
        });
        combineLatest([
            this.userService.getLoggedInUser(),
            this.qCountObservable]).pipe(first())
            .subscribe(([activeUser, qCount]) => {
                this.activeUser = activeUser;
                this.qCount = qCount;
                this.openBetweenItemsDialog();
            });
    }

    openBetweenItemsDialog() {
        const dialog = this.dialog.open(BetweenItemsComponent, {
            width: '600px',
            panelClass: 'between-items-dialog',
            disableClose: true,
            data: {
                projects: Object.keys(this.activeUser.projects),
                activeProjectName: this.activeProjectName,
                qCountObservable: this.qCountObservable,
                qCount: this.qCount
            }
        }).afterClosed().subscribe((res) => {
            if (res && res.getNextTask) {
                this.getNextTask(this.activeProjectName);
            }
        });
    }

    getNextTask(projectName: string) {
        this.loading = true;
        this.manager.getNextTask(projectName).subscribe((task?: Task) => {
            if (!task) {
                return this.openBetweenItemsDialog();
            }
            this.lastAnswerTime = +new Date();
            this.task = task;
            this.probesSceneMode = this.task.sceneData ? 'scene' : 'probes';
            this.questionOnClick(task.questions[0]);
            this.ref.markForCheck();
        });
    }

    onZoomIn() {
        this.zoomEvent.emit(1.2);
    }

    onZoomOut() {
        this.zoomEvent.emit(1 / 1.2);
    }

    onScaleChanged(scale: number) {
        if (!this.initialScale) {
            this.initialScale = scale;
        }
        this.scale = (scale / this.initialScale) * 100;
        this.ref.detectChanges();
    }

    onFitToScreen() {
        this.fitToScreenEvent.emit();
    }

    onRenderFinish() {
        this.loading = false;
        this.inactivityTimer.start(this.cancelTask.bind(this));
    }

    getQuestionTypeText(questionType: string) {
        switch (questionType) {
            case 'YesNo':
                return 'Yes / No';
            case 'YesNoCantTell':
                return "Yes / No / Can't tell";
            default:
                return '';
        }
    }

    onChangeProbeSceneMode(mode: 'probes' | 'scene') {
        this.probesSceneMode = mode;

        if (this.probesSceneMode === 'probes') {
            this.arrowKeysAffectedList = 'probes';
            this.onChangeSelectedProbe(this.task.probesMegazordData[0]);
        } else {
            this.arrowKeysAffectedList = 'questions';
            this.selectedProbe = null;
            this.selectedProbeMasks = null;
        }
        this.ref.detectChanges();
    }

    onChangeSelectedProbe(probe) {
        if (!probe) {
            return;
        }
        this.selectedProbe = null;
        setTimeout(() => {
            this.selectedProbeMasks = [];
            this.selectedProbe = probe;
            if (this.selectedQuestion) {
                const selectedProbeMasks = find<Probe>(this.selectedQuestion.location.probes, {pk: this.selectedProbe.probeId});
                if (selectedProbeMasks) {
                    this.selectedProbeMasks = selectedProbeMasks.masks;
                }
            }
            this.ref.markForCheck();
        }, 0);
    }

    questionOnClick(question: Question) {
        if (this.selectedProbe) {
            this.onChangeSelectedProbe(this.selectedProbe);
        }
        if (question === this.selectedQuestion) {
            this.clearSelectedQuestion();
            return;
        }
        this.selectedQuestion = question;
        this.selectedProduct = find(this.task.productData, {id: question.productId});
        this.masks = [];
        if (this.task.sceneData) {
            forEach(question.location.scene.masks, (mask) => {
                this.masks.push({
                    topLeft: {
                        x: mask.x,
                        y: mask.y
                    },
                    bottomRight: {
                        x: mask.x + mask.w,
                        y: mask.y + mask.h
                    }
                });
            });
        }

        const selectedImages = this.selectedImages;
        if (this.selectedProduct) {
            const productImage = find(selectedImages, {direction: 'Front'}) || selectedImages[0];
            this.selectedProductImageUrl = productImage ? productImage.path : null;
        }
        setTimeout(() => { // wait for megzord hack- first question
            this.zoomToSelected();
        }, 10);
        this.sortQuestions();
        if (!this.task.sceneData) {
            this.onChangeProbeSceneMode('probes');
        }
    }

    private zoomToSelected() {
        if (!this.megazordCanvas.first || !this.selectedQuestion) {
            return;
        }
        const mask = this.selectedQuestion.location.scene.masks[0];
        this.megazordCanvas.first.moveFrustumTo(
            {x: -mask.x + 100, y: -mask.y + 100},
            {x: -mask.x - mask.w - 100, y: -mask.y - mask.h - 100}
        );
    }

    private onAnswer(answer: string | Answer, clearSelectedQuestion = true) {
        this.selectedQuestion.answer = answer;
        if (!this.selectedQuestion.isValidAnswer) {
            return;
        }

        this.selectedQuestion.duration = +new Date() - this.lastAnswerTime;
        this.lastAnswerTime = +new Date();
        if (this.allQuestionsAnswered() || !clearSelectedQuestion) {
            return;
        }
        this.clearSelectedQuestion();
        this.sortQuestions();
        this.questionOnClick.bind(this)(this.task.questions[0]);
        this.onChangeSelectedProbe(this.selectedProbe);
        this.ref.markForCheck();
    }

    private sortQuestions() {
        this.task.questions = orderBy(this.task.questions, (question) => {
            return question.isValidAnswer;
        });
    }

    clearSelectedQuestion() {
        this.selectedQuestion = null;
        this.selectedProduct = null;
        this.masks = [];
    }

    finishTask() {
        this.clearSelectedQuestion();
        this.task = null;
        this.selectedProbe = null;
        this.selectedProbeMasks = [];
        this.initialScale = null;
        this.inactivityTimer.stop();
        this.openBetweenItemsDialog();
    }

    saveTask() {
        this.manager.saveTask();
        this.finishTask();
    }

    onClickBackButton() {
        if (!this.task) {
            window.location.href = this.homepageUrl;
            return;
        }

        const dialog = this.dialog.open(CancelTaskDialogComponent, {
            width: '600px',
            panelClass: 'cancel-task-dialog',
            disableClose: true,
            data: {
                sceneId: this.task.sceneId,
                userName: this.activeUser.firstName
            }
        }).afterClosed().subscribe((res) => {
            if (res.cancel) {
                this.cancelTask(res.reason);
            }
        });

    }

    cancelTask(reason) {
        this.manager.cancelTask(this.activeProjectName, this.task.selectorTaskId, this.task.objectId, reason).subscribe(() => {
        });
        this.finishTask();
    }

    allQuestionsAnswered() {
        if (!this.task) {
            return true;
        }
        return every(this.task.questions, (question) => question.isValidAnswer);
    }

    zooming() {
        const selectedImages = this.selectedImages.map(img => ({
            src: img.path + '/original',
            thumb: img.path + '/small'
        }));
        const selectedProductImageIndex = findIndex(this.selectedImages, img => img.path === this.selectedProductImageUrl);
        this.lightbox.open(selectedImages, selectedProductImageIndex);
    }

    onSelectImage(path: string) {
        this.selectedProductImageUrl = path;
    }

    ngOnDestroy(): void {
        this.complete$.next();
    }
}







