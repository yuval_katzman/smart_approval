import { Injectable } from '@angular/core';
import {HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {CookieService} from "ngx-cookie-service";

@Injectable({
    providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {
    constructor(private cookieService: CookieService) { }


    intercept(req: HttpRequest<any>, next: HttpHandler): any {
        const connectSid = this.cookieService.get('connect.sid');
        if (connectSid) {
            req = req.clone({headers: req.headers.set('connect.sid', connectSid.split(':').join('%3A').split('/').join('%2F'))});
        }
        return next.handle(req)
    }
}
