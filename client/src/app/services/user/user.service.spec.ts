import {TestBed} from '@angular/core/testing';

import {UserService} from './user.service';
import {CookieService} from 'ngx-cookie-service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {User} from '../../models/user';
import {Observable} from 'rxjs';


describe('UserService', () => {
    let user;
    let rawUser;
    let service: UserService;
    beforeEach(() => TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [CookieService]
    }));

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [CookieService]
        });

        rawUser = {
            address: '',
            birth_date: '',
            center: '',
            country: '',
            create_date: '',
            current_center: '',
            delete_date: null,
            deleted: false,
            email: 'user@traxretail.com',
            first_name: 'Test',
            gender: 'Female',
            is_active: true,
            is_staff: false,
            is_superuser: false,
            join_date: '',
            last_name: 'User',
            last_login: null,
            mobile: '',
            modules: [],
            parents_phone: '',
            projects: {
                test: {
                    validate: 5
                }
            },
            roles: ['QAT'],
            staff_id: null,
            stitching: 5,
            teamMembers: []
        };

        user = new User({
            address: rawUser.address,
            birthDate: rawUser.birth_date,
            center: rawUser.center,
            country: rawUser.country,
            createDate: rawUser.create_date,
            currentCenter: rawUser.current_center,
            deleteDate: rawUser.delete_date,
            deleted: rawUser.deleted,
            email: rawUser.email,
            firstName: rawUser.first_name,
            gender: rawUser.gender,
            isActive: rawUser.is_active,
            isStaff: rawUser.is_staff,
            isSuperuser: rawUser.is_superuser,
            joinDate: rawUser.join_date,
            lastName: rawUser.last_name,
            lastLogin: rawUser.last_login,
            mobile: rawUser.mobile,
            modules: rawUser.modules,
            parentsPhone: rawUser.parents_phone,
            projects: rawUser.projects,
            roles: rawUser.roles,
            staffId: rawUser.staff_id,
            stitching: rawUser.stitching,
            teamMembers: rawUser.teamMembers
        });

        service = TestBed.get(UserService);
    });


    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('getLoggedInUser when not unauthenticated should logout', () => {
        const httpMock = TestBed.get(HttpTestingController);
        const logoutSpy = spyOn(service, 'logout');

        service.getLoggedInUser().subscribe();

        // Cloud 1
        httpMock.expectOne('/api/users/profile').flush({}, {
            status: 401,
            statusText: 'unauthenticated'
        });

        // Cloud 2
        httpMock.expectOne('http://sheker/api/users/profile').flush({}, {
            status: 401,
            statusText: 'unauthenticated'
        });

        expect(logoutSpy).toHaveBeenCalled();
    });

    it('getLoggedInUser should return user from server', () => {
        const httpMock = TestBed.get(HttpTestingController);

        service.getLoggedInUser().subscribe((res) => {
            expect(res).toEqual(user);
        });

        httpMock.expectOne('/api/users/profile').flush(rawUser);

    });

    it('getLoggedInUser should return user from cache', () => {
        const httpMock = TestBed.get(HttpTestingController);

        service.getLoggedInUser().subscribe((res) => {
            expect(res).toEqual(user);

            service.getLoggedInUser().subscribe((res2) => {
                expect(res2).toEqual(user);
            });
        });

        httpMock.expectOne('/api/users/profile').flush(rawUser);

    });

    it('isAuthorized should reject if user dont have access to project', () => {
        const isAuthenticatedSpy = spyOn(service, 'isAuthenticated').and.returnValue(true);
        const getLoggedInUserSpy = spyOn(service, 'getLoggedInUser').and.returnValue(new Observable(observer => observer.next(user)));

        service.isAuthorized('topSecretProject', ['QAT']).subscribe((authorized => {
            expect(authorized).toBeFalsy();
        }));
    });

    it('isAuthorized should reject if user has access to project but not required role', () => {
        const isAuthenticatedSpy = spyOn(service, 'isAuthenticated').and.returnValue(true);
        const getLoggedInUserSpy = spyOn(service, 'getLoggedInUser').and.returnValue(new Observable(observer => observer.next(user)));

        service.isAuthorized('test', ['specialRole']).subscribe((authorized => {
            expect(authorized).toBeFalsy();
        }));
    });

    it('isAuthorized should resolve if user is ok', () => {
        const isAuthenticatedSpy = spyOn(service, 'isAuthenticated').and.returnValue(true);
        const getLoggedInUserSpy = spyOn(service, 'getLoggedInUser').and.returnValue(new Observable(observer => observer.next(user)));

        service.isAuthorized('test', ['QAT']).subscribe((authorized => {
            expect(authorized).toBeTruthy();
        }));
    });

    it('isAuthenticated should reject if no cookie', () => {
        expect(service.isAuthenticated()).toBeFalsy();
    });

    it('isAuthenticated should resolve if cookie', () => {
        const cookieService = TestBed.get(CookieService);

        const cookieGetSpy = spyOn(cookieService, 'get').and.returnValue('bla');

        expect(service.isAuthenticated()).toBeTruthy();
    });
});
