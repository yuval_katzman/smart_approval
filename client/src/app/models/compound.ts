import {cloneDeep} from "lodash";
import {Molecule} from "./molecule";

interface CompoundData {
    sceneId: number | string;
    probes: {
        original_height: number,
        original_width: number,
        probe_pk: string,
        tileId: {x: number, y: number},
        tileImagePartialPath: string,
        probe_url: string,
        numOfLevels: number,
        clipPath: string
    }[];
    isTiled?: boolean;
    molecules: {
        data: {
            probe_pk: string,
            height: number,
            width: number,
            tileId: {x: number, y: number},
            transform: [[1, 0, number],
            [0, 1, number], [0, 0, 1]],
            location: {top: number, left: number}
        }[]
    }[];
}

interface TiledSceneJson {
    number_of_tiles_x: number;
    number_of_tiles_y: number;
    tile_width: number;
    tile_height: number;
    number_of_levels: number;
}

export class Compound {
    molecule: {};
    sceneWidthInTiles: number;
    sceneHeightInTiles: number;
    tileWidth: number;
    tileHeight: number;
    numOfLevels: number;
    numberOfProbes: number;
    molecules: Molecule[];


    constructor(compoundData: CompoundData, tiledSceneJson: TiledSceneJson, sceneDirectory: string) {
        const clondedCompoundData = cloneDeep(compoundData);
        const sceneId = clondedCompoundData.sceneId;

        this.molecule = {};
        this.sceneWidthInTiles = Number(tiledSceneJson.number_of_tiles_x);
        this.sceneHeightInTiles = Number(tiledSceneJson.number_of_tiles_y);

        this.tileWidth = Number(tiledSceneJson.tile_width) || 4096;
        this.tileHeight = Number(tiledSceneJson.tile_height) || 4096;
        this.numOfLevels = Number(tiledSceneJson.number_of_levels) || 7;

        this.numberOfProbes = this.sceneWidthInTiles * this.sceneHeightInTiles;

        clondedCompoundData.probes = [];
        clondedCompoundData.molecules[0].data = [];

        for (let tileXIndex = 0; tileXIndex < this.sceneWidthInTiles; tileXIndex++) {
            for (let tileYIndex = 0; tileYIndex < this.sceneHeightInTiles; tileYIndex++) {
                let opacityCoordsArrays;
                let clipPathArray;
                opacityCoordsArrays = tiledSceneJson['tile_' + tileXIndex + '_' + tileYIndex];

                if (opacityCoordsArrays) {
                    clipPathArray = this.parseOpacityCoords(opacityCoordsArrays);
                }

                clondedCompoundData.probes.push({
                    original_height: this.tileHeight,
                    original_width: this.tileWidth,
                    probe_pk: tileXIndex.toString() + tileYIndex.toString(),
                    tileId: {x: tileXIndex, y: tileYIndex},
                    tileImagePartialPath: sceneDirectory + sceneId + '_tile_' +
                        tileXIndex + '_' + tileYIndex,
                    probe_url: sceneDirectory + sceneId + '_tile_' +
                        tileXIndex + '_' + tileYIndex + '_' + (this.numOfLevels - 1) + '.jpg',
                    numOfLevels: this.numOfLevels,
                    clipPath: clipPathArray
                });

                clondedCompoundData.molecules[0].data.push({
                    probe_pk: tileXIndex.toString() + tileYIndex.toString(),
                    height: this.tileHeight,
                    width: this.tileWidth,
                    tileId: {x: tileXIndex, y: tileYIndex},
                    transform: [[1, 0, this.tileWidth * tileXIndex],
                        [0, 1, this.tileHeight * tileYIndex], [0, 0, 1]],
                    location: {top: this.tileHeight * tileYIndex, left: this.tileWidth * tileXIndex}
                });
            }
        }

        // There's only one molecule in a tiled compound.
        clondedCompoundData.isTiled = true;
        this.molecules = [new Molecule(clondedCompoundData.molecules[0], clondedCompoundData)];

    }

    private parseOpacityCoords(opacityCoordsArrays) {
        const clipPathsArray = [];
        for (let index = 0; index < opacityCoordsArrays.x.length; index++) {
            clipPathsArray.push({
                x: Math.floor(opacityCoordsArrays.x[index]),
                y: Math.floor(opacityCoordsArrays.y[index])
            });
        }

        return clipPathsArray;
    }

}
