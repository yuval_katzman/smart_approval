import {Injectable} from '@angular/core';
import {forEach} from 'lodash';
import {HttpClient, HttpErrorResponse, HttpRequest} from '@angular/common/http';
import {forkJoin, Observable, of, throwError} from 'rxjs';
import {environment} from '../../../environments/environment';
import {catchError} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class API {
  private projectsClouds: {
    [projectName: string]: string
  } = {};
  private getProjectCloudFromServerPromise: Promise<string>;

  constructor(private http: HttpClient) {
  }

  request(method: 'get' | 'put' | 'post', url: string, body?: any, options?: any) {
    options = options || {};
    options.body = body;
    const projectName = options.pathParams && options.pathParams.projectName;
    const requestedCloud = options.requestedCloud;

    if (!projectName && !requestedCloud) {
      const generatedUrl = this.generateUrlWithPathParams(
          url, options.pathParams);
      return this.http.request(method, generatedUrl, options);
    }

    if (requestedCloud) {
      return new Observable((observer) => {
          const generatedUrl = this.generateCloudUrl(requestedCloud, url, options);
          this.observerRequest(observer, method, generatedUrl, options);
      });
    }

    return new Observable((observer) => {
      this.getProjectCloud(projectName).subscribe((projectCloud) => {
        const generatedUrl = this.generateCloudUrl(projectCloud, url, options);
        this.observerRequest(observer, method, generatedUrl, options);
      });
    });
  }

  private generateCloudUrl(projectCloud, url: string, options?: any) {
    if (projectCloud === environment.config.cloud) {
      return this.generateUrlWithPathParams(url, options.pathParams);
    }

    const baseUrl = environment.config.cloudsUrls[projectCloud];
    return this.generateUrlWithPathParams(`${baseUrl}${url}`, options.pathParams);
  }

  private observerRequest(observer, method: 'get' | 'put' | 'post', generatedUrl, options) {
    this.http.request(method, generatedUrl, options).pipe(catchError(err => {
      return throwError(err);
    })).subscribe((res) => {
      observer.next(res);
      observer.complete();
    }, (err) => {
      observer.error(err);
    });
  }

  get(url: string, options?: any): Observable<any> {
    return this.request('get', url, undefined, options);
  }

  put(url: string, body: any, options?: any): Observable<any> {
    return this.request('put', url, body, options);
  }

  post(url: string, body: any, options?: any): Observable<any> {
    return this.request('post', url, body, options);
  }

  private generateUrlWithPathParams(url: string, pathParams: { [key: string]: string }): string {
    let generatedUrl = url;

    forEach(pathParams, (value, key) => {
      generatedUrl = generatedUrl.replace(`:${key}`, value);
    });

    return generatedUrl;
  }

  private getProjectCloud(projectName): Observable<string> {
    return new Observable((observer) => {
      const projectCloudFromMemory = this.getProjectCloudFromMemory(projectName);
      if (projectCloudFromMemory) {
        observer.next(projectCloudFromMemory);
        observer.complete();
        return;
      }

      const projectCloudFromLocalStorage = this.getProjectCloudFromLocalStorage(projectName);
      if (projectCloudFromLocalStorage) {
        observer.next(projectCloudFromLocalStorage);
        observer.complete();
        return;
      }

      this.getProjectCloudFromServer(projectName).then((projectCloudFromServer) => {
        observer.next(projectCloudFromServer);
        observer.complete();
      });
    });
  }

  private getProjectCloudFromMemory(projectName): string | undefined {
    return this.projectsClouds[projectName];
  }


  private getProjectCloudFromLocalStorage(projectName): string | undefined {
    const projectsClouds = localStorage.getItem('projectsClouds');
    if (!projectsClouds) {
      return;
    }
    this.projectsClouds = JSON.parse(projectsClouds);
    return this.projectsClouds[projectName];
  }

  private getProjectCloudFromServer(projectName: string): Promise<string> {
    if (this.getProjectCloudFromServerPromise) {
      return this.getProjectCloudFromServerPromise;
    }

    this.getProjectCloudFromServerPromise = new Promise((resolve) => {
      this.projectsClouds = {};
      const availableClouds = Object.keys(environment.config.cloudsUrls);
      const cloudsRequests = availableClouds.map(requestedCloud =>
          this.get('/api/projects', {requestedCloud}).pipe(catchError(err => of([]))));

      forkJoin(cloudsRequests)
            .subscribe((res: { project_name: string, cloudProvider: string }[][]) => {
              this.getProjectCloudFromServerPromise = null;

              // Merge projects from all clouds
              const projectsData = [].concat(...res);
              forEach(projectsData, (projectData) => {
                this.projectsClouds[projectData.project_name] = projectData.cloudProvider;
              });

              resolve(this.projectsClouds[projectName]);
            });
    });

    return this.getProjectCloudFromServerPromise;
  }


}

