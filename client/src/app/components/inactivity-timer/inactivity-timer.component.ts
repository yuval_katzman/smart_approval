import {ChangeDetectorRef, Component, ElementRef, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";

@Component({
  selector: 'app-inactivity-timer',
  templateUrl: './inactivity-timer.component.html',
  styleUrls: ['./inactivity-timer.component.scss']
})
export class InactivityTimerComponent implements OnInit {
  private endTime: number;
  private circleAnimation;
  private interval: number;
  timer;
  private modalDuration = 15;
  numberOfTries: number;

  constructor(private dialogRef: MatDialogRef<InactivityTimerComponent>,
              private elem: ElementRef,
              @Inject(MAT_DIALOG_DATA) private data: any) {
    this.numberOfTries = data.numberOfTries;
  }

  ngOnInit() {
    this.endTime = Date.now() + this.modalDuration * 1000;
    this.interval = window.setInterval(() => {
      this.circleAnimation = this.circleAnimation || this.elem.nativeElement.querySelectorAll('.circle-animation')[0];
      this.timer = this.endTime - Date.now();
      if (this.timer <= 0) {
        clearInterval(this.interval);
        this.dialogRef.close('timeout');
      }
      // 377 is the perimeter of the circle when radius is ~60, have to explicitly write it in order to use stroke-dashoffset
      this.circleAnimation.style['stroke-dashoffset'] = ((1 - (this.timer / (this.modalDuration * 1000))) * 377);
    }, 20, true);
  }

  continue() {
    clearInterval(this.interval);
    this.dialogRef.close('continue');
  }

}
