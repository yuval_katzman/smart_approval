import {resolve} from "path";
import { map } from "lodash";
const nc = require('@trax-retail/node-core')('smart-approval');
nc.init(resolve('./scripts'), 'INT');

const scenes = [
    {
        scene_uid: "d8996d39-13b3-4b5d-b401-bf2b9bc379db",
        session_uid: "e2f953b1-b926-4d67-8786-02f5a37d1971",
        scene_id: 399174
    },
    {
        scene_uid: "177a2b6a-8301-4630-9521-1d2223480b33",
        session_uid: "28fe061e-858d-44fc-afb7-5c3fdb253670",
        scene_id: 399359
    },
    {
        scene_uid: "50d227a5-0579-4d1e-a2cb-be462d1fc70f",
        session_uid: "95474fb7-4515-48c4-95c4-5215cc3cad2e",
        scene_id: 399489
    }
]

const messages = map(scenes, scene => {
    return {
        "event_name" : "SMART-TASK_REQUESTED",
        "job_type": "Approvals",
        "sub_queue": "LowFacingApproval",
        "ttl_in_minutes" : 200,
        "queue_entity_id": String(scene.scene_id),
        "project_name": "integ5",
        "approval_id":1,
        "scene_id": scene.scene_id,
        "entity_id": scene.scene_id,
        "entity_type": 'scene',
        "scene_uid": scene.scene_uid,
        "session_uid": scene.session_uid,
        "questions": [
            {
                "value": "10",
                "location": { "scene": { "pk": 1,  "masks": [ { "h": 200, "w": 1533, "x": 2234, "y": 959 } ]}, "probes": [] },
                "product_fk": 2,
                "question_uid": "7a6dd1f8-5972-46a8-816a-aef036235026",
                "question_type": "YesNo",
                "question_category": "LowFacingApproval",
            },
            {
                "value": "5",
                "location": { "scene": { "pk": 1,  "masks": [ { "h": 200, "w": 1533, "x": 2234, "y": 959 } ]}, "probes": [] },
                "product_fk": 3,
                "question_uid": "7a6dd1f8-5972-46a8-816a-aef036235026",
                "question_type": "YesNoCantTell",
                "question_category": "LowFacingApproval",
            },
        ]
    }
});

messages.forEach(message => nc.cloud.topic.publish(message))
