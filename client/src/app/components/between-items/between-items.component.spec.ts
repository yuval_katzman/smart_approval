import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {Router} from '@angular/router';
import {BetweenItemsComponent} from './between-items.component';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {RouterTestingModule} from '@angular/router/testing';
import {Observable} from 'rxjs';
import {HttpClientModule} from "@angular/common/http";


describe('BetweenItemsComponent', () => {
    let component: BetweenItemsComponent;
    let fixture: ComponentFixture<BetweenItemsComponent>;
    let router: Router;
    const matData = {
        qCountObservable: new Observable(observer => {
            setTimeout(() => observer.next(5), 100);
        })
    };

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [BetweenItemsComponent],
            imports: [RouterTestingModule, HttpClientModule],
            providers: [{
                provide: MatDialogRef,
                useValue: {
                    close: () => {
                    }
                }
            },
                {
                    provide: MAT_DIALOG_DATA,
                    useValue: matData
                }]
        })
            .compileComponents();

    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BetweenItemsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 15000;
        router = TestBed.get(Router);
    });

    it('should create', fakeAsync(() => {
        expect(component).toBeTruthy();
    }));

    it('should set weAreWaitingForYou to true after timeout runs out', fakeAsync(() => {
        fixture = TestBed.createComponent(BetweenItemsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();

        tick(10000);

        expect(component.weAreWaitingForYou).toBeTruthy();
    }));

    it('should change project', () => {
        const navigateSpy = spyOn(router, 'navigate');
        component.onChangeProject('test');
        expect(navigateSpy).toHaveBeenCalledWith(['approvals', 'test', 'validate']);
    });

    it('should unsubscribe when calling continue', () => {
        const subscriptionSpy = spyOn(component.subscription, 'unsubscribe');

        component.continue();

        expect(subscriptionSpy).toHaveBeenCalled();
    });

    it('should close the dialog when calling continue', () => {
        const dialog: MatDialogRef<BetweenItemsComponent> = TestBed.get(MatDialogRef);
        const dialogOpenSpy = spyOn<any>(dialog, 'close');

        component.continue();

        expect(dialogOpenSpy).toHaveBeenCalled();
    });

});
