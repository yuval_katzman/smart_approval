import {TestBed} from '@angular/core/testing';

import {InactivityTimerService} from './inactivity-timer.service';
import {MatDialog, MatDialogModule} from '@angular/material/dialog';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {InactivityTimerComponent} from '../../components/inactivity-timer/inactivity-timer.component';
import {of} from 'rxjs';

describe('InactivityTimerService', () => {
    let service: InactivityTimerService;
    beforeEach(() => TestBed.configureTestingModule({
            declarations: [InactivityTimerComponent],
            imports: [MatDialogModule, BrowserAnimationsModule],
        }).overrideModule(BrowserAnimationsModule, {
            set: {
                entryComponents: [InactivityTimerComponent],
            }
        })
    );

    beforeEach(() => {
        service = TestBed.get(InactivityTimerService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should start', () => {
        service.start(() => {
        });
        // @ts-ignore
        expect(service.running).toBe(true);
    });

    it('should stop', () => {
        // @ts-ignore
        service.onTimeout();
        // @ts-ignore
        expect(service.running).toBe(false);
    });

    it('should trigger cancelTaskFn when dialog times out', () => {
        const dialog: MatDialog = TestBed.get(MatDialog);
        const dialogOpenSpy = spyOn<any>(dialog, 'open').and
            .returnValue({afterClosed: () => of('timeout')});
        service.start(() => {
        });
        const cancelTaskFnSpy = spyOn<any>(service, 'cancelTaskFn').and.callThrough();

        // @ts-ignore
        service.onTimeout();

        expect(cancelTaskFnSpy).toHaveBeenCalled();
    });

    it('should lower the number of tries left when dialog closes with continue', () => {
        const dialog: MatDialog = TestBed.get(MatDialog);
        const dialogOpenSpy = spyOn<any>(dialog, 'open').and
            .returnValue({afterClosed: () => of('continue')});
        service.start(() => {
        });
        // @ts-ignore
        const wantedResult = service.numberOfTries - 1;

        // @ts-ignore
        service.onTimeout();
        // @ts-ignore
        const actualResult = service.numberOfTries;

        expect(actualResult).toEqual(wantedResult);
    });

    it('should set running to true when dialog closes with continue', () => {
        const dialog: MatDialog = TestBed.get(MatDialog);
        const dialogOpenSpy = spyOn<any>(dialog, 'open').and
            .returnValue({afterClosed: () => of('continue')});
        service.start(() => {
        });

        // @ts-ignore
        service.onTimeout();

        // @ts-ignore
        expect(service.running).toBeTruthy();
    });
});
