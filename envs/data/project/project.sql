SET FOREIGN_KEY_CHECKS = 0;

TRUNCATE TABLE static.image_storage_path;
INSERT INTO static.image_storage_path (`pk`, `path`, `default`) VALUES
('1', 'http://traxus.s3.amazonaws.com/shufersalil/Probe_Images', '1');


TRUNCATE TABLE probedata.scene;
INSERT INTO probedata.scene (pk, type, status, scene_date, sales_rep_fk, store_fk, store_uid, cooler_fk, template_fk,
number_of_probes, first_probe_capture_time, last_probe_capture_time, creation_time, locked_by, lock_expiration_time,
delete_time, stitch_time, upload_complete_time, scene_complete_time, in_store_location_fk, stitched_by,
realogram_html_path, session_uid, scene_uid, external_scene_id, store_area_type, scene_attribute_1,
scene_attribute_2, scene_attribute_3, scene_attribute_4, scene_attribute_5, scene_attribute_6,
scene_attribute_7, scene_attribute_8, scene_attribute_9, scene_attribute_10, rotated_probe,
exclude_status_fk, review_status_fk, resolution_code_fk, action_code_fk, reviewer, review_time,
number_of_ignored_probes, connected_shelf_fk) VALUES ('1', '2', '6', '2019-09-04', NULL, '28',
 '60273f16-43c2-4aeb-9991-7230ac584678', NULL, '60', '4', '2019-09-04 12:02:32', '2019-09-04 12:04:03',
  '2019-09-04 12:09:48', NULL, NULL, NULL, '2019-09-04 12:10:51', '2019-09-04 12:10:14', '2019-09-04 12:20:01',
   NULL, 'copy_stitch',
    'https://traxus.s3.amazonaws.com/shufersalil/scene-images/914757/Realogram_Scene914757_2019090412195019S.html',
     'd121f345-c7f3-4de7-a900-be6b1205d745', 'f6631314-8f0d-449f-a3e6-038500cc6f35',
      'f6631314-8f0d-449f-a3e6-038500cc6f35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
      NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '68'
),
('2', '2', '6', '2019-09-04', NULL, '28',
'60273f16-43c2-4aeb-9991-7230ac584678', NULL, '60', '4', '2019-09-04 12:02:32', '2019-09-04 12:04:03',
'2019-09-04 12:09:48', NULL, NULL, NULL, '2019-09-04 12:10:51', '2019-09-04 12:10:14', '2019-09-04 12:20:01',
NULL, 'copy_stitch',
'https://traxus.s3.amazonaws.com/shufersalil/scene-images/914757/Realogram_Scene914757_2019090412195019S.html',
'd121f345-c7f3-4de7-a900-be6b1205d745', 'f6631314-8f0d-449f-a3e6-038500cc6f36',
'f6631314-8f0d-449f-a3e6-038500cc6f35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '68'
);

TRUNCATE TABLE probedata.probe;
INSERT INTO probedata.probe (pk, retries, template_fk, scene_fk, session_uid, scene_uid, creation_time,
 process_time, delete_time, store_fk, store_uid, cooler_fk, special_case, validation_time,
 local_image_time, upload_time, match_count, type, status, sales_rep_fk, image_tz_offset,
 path_fk, image_date, hash, in_store_location_fk, pos_lat, pos_long, pos_accuracy, pos_timestamp,
 original_width, original_height, medium_size_scale, analysis_send_time, analysis_duration, grid_x,
  grid_y, quality, stitch_grid_x, stitch_grid_y, image_uid, application_version, ignored, probe_order,
  gps_update_time, iot_device_fk) VALUES ('2187121', '0', '60', '1', 'd121f345-c7f3-4de7-a900-be6b1205d745', 'f6631314-8f0d-449f-a3e6-038500cc6f35', '2019-09-04 12:09:49', NULL, NULL, '28', '60273f16-43c2-4aeb-9991-7230ac584678', NULL, '0', '2019-09-04 12:19:04', '2019-09-04 12:02:32', '2019-09-04 12:09:49', '62', '2', '3', NULL, '180', '1', '2019-09-04', '681b9568-bc5c-4729-8a50-0a709874cd76', NULL, NULL, NULL, NULL, NULL, '2592', '1944', NULL, NULL, NULL, NULL, NULL, 'Unknown', NULL, NULL, '681b9568-bc5c-4729-8a50-0a709874cd76', NULL, '0', '1', NULL, '1284'),
('2187122', '0', '60', '1', 'd121f345-c7f3-4de7-a900-be6b1205d745', 'f6631314-8f0d-449f-a3e6-038500cc6f35', '2019-09-04 12:09:50', NULL, NULL, '28', '60273f16-43c2-4aeb-9991-7230ac584678', NULL, '0', '2019-09-04 12:18:20', '2019-09-04 12:04:03', '2019-09-04 12:09:50', '49', '2', '3', NULL, '180', '1', '2019-09-04', '32ad5a32-7a28-49da-92f9-2c31a75a343b', NULL, NULL, NULL, NULL, NULL, '2592', '1944', NULL, NULL, NULL, NULL, NULL, 'Unknown', NULL, NULL, '32ad5a32-7a28-49da-92f9-2c31a75a343b', NULL, '0', '2', NULL, '1185'),
('2187123', '0', '60', '1', 'd121f345-c7f3-4de7-a900-be6b1205d745', 'f6631314-8f0d-449f-a3e6-038500cc6f35', '2019-09-04 12:09:50', NULL, NULL, '28', '60273f16-43c2-4aeb-9991-7230ac584678', NULL, '0', '2019-09-04 12:18:17', '2019-09-04 12:03:33', '2019-09-04 12:09:50', '52', '2', '3', NULL, '180', '1', '2019-09-04', 'b83bbd5b-8585-4004-a89a-52981b03b3a5', NULL, NULL, NULL, NULL, NULL, '2592', '1944', NULL, NULL, NULL, NULL, NULL, 'Unknown', NULL, NULL, 'b83bbd5b-8585-4004-a89a-52981b03b3a5', NULL, '0', '3', NULL, '1283'),
('2187124', '0', '60', '1', 'd121f345-c7f3-4de7-a900-be6b1205d745', 'f6631314-8f0d-449f-a3e6-038500cc6f35', '2019-09-04 12:09:51', NULL, NULL, '28', '60273f16-43c2-4aeb-9991-7230ac584678', NULL, '0', '2019-09-04 12:18:37', '2019-09-04 12:03:06', '2019-09-04 12:09:51', '49', '2', '3', NULL, '180', '1', '2019-09-04', '5139aac9-8d16-49a1-98ee-439fadd128b8', NULL, NULL, NULL, NULL, NULL, '2592', '1944', NULL, NULL, NULL, NULL, NULL, 'Unknown', NULL, NULL, '5139aac9-8d16-49a1-98ee-439fadd128b8', NULL, '0', '4', NULL, '1285'),
('2187125', '0', '60', '2', 'd121f345-c7f3-4de7-a900-be6b1205d745', 'f6631314-8f0d-449f-a3e6-038500cc6f35', '2019-09-04 12:09:49', NULL, NULL, '28', '60273f16-43c2-4aeb-9991-7230ac584678', NULL, '0', '2019-09-04 12:19:04', '2019-09-04 12:02:32', '2019-09-04 12:09:49', '62', '2', '3', NULL, '180', '1', '2019-09-04', '681b9568-bc5c-4729-8a50-0a709874cd77', NULL, NULL, NULL, NULL, NULL, '2592', '1944', NULL, NULL, NULL, NULL, NULL, 'Unknown', NULL, NULL, '681b9568-bc5c-4729-8a50-0a709874cd77', NULL, '0', '1', NULL, '1284'),
('2187126', '0', '60', '2', 'd121f345-c7f3-4de7-a900-be6b1205d745', 'f6631314-8f0d-449f-a3e6-038500cc6f35', '2019-09-04 12:09:50', NULL, NULL, '28', '60273f16-43c2-4aeb-9991-7230ac584678', NULL, '0', '2019-09-04 12:18:20', '2019-09-04 12:04:03', '2019-09-04 12:09:50', '49', '2', '3', NULL, '180', '1', '2019-09-04', '32ad5a32-7a28-49da-92f9-2c31a75a343c', NULL, NULL, NULL, NULL, NULL, '2592', '1944', NULL, NULL, NULL, NULL, NULL, 'Unknown', NULL, NULL, '32ad5a32-7a28-49da-92f9-2c31a75a343c', NULL, '0', '2', NULL, '1185'),
('2187127', '0', '60', '2', 'd121f345-c7f3-4de7-a900-be6b1205d745', 'f6631314-8f0d-449f-a3e6-038500cc6f35', '2019-09-04 12:09:50', NULL, NULL, '28', '60273f16-43c2-4aeb-9991-7230ac584678', NULL, '0', '2019-09-04 12:18:17', '2019-09-04 12:03:33', '2019-09-04 12:09:50', '52', '2', '3', NULL, '180', '1', '2019-09-04', 'b83bbd5b-8585-4004-a89a-52981b03b3a6', NULL, NULL, NULL, NULL, NULL, '2592', '1944', NULL, NULL, NULL, NULL, NULL, 'Unknown', NULL, NULL, 'b83bbd5b-8585-4004-a89a-52981b03b3a6', NULL, '0', '3', NULL, '1283'),
('2187128', '0', '60', '2', 'd121f345-c7f3-4de7-a900-be6b1205d745', 'f6631314-8f0d-449f-a3e6-038500cc6f35', '2019-09-04 12:09:51', NULL, NULL, '28', '60273f16-43c2-4aeb-9991-7230ac584678', NULL, '0', '2019-09-04 12:18:37', '2019-09-04 12:03:06', '2019-09-04 12:09:51', '49', '2', '3', NULL, '180', '1', '2019-09-04', '5139aac9-8d16-49a1-98ee-439fadd128b9', NULL, NULL, NULL, NULL, NULL, '2592', '1944', NULL, NULL, NULL, NULL, NULL, 'Unknown', NULL, NULL, '5139aac9-8d16-49a1-98ee-439fadd128b9', NULL, '0', '4', NULL, '1285');



TRUNCATE TABLE probedata.stitching_scene_info;
INSERT INTO probedata.stitching_scene_info (pk, scene_fk, display_type_fk, user_fk, stitching_duration, creation_time,
delete_time, tiles_creation_time, loading_duration, mosaicing_duration, display_selection_duration, base_and_shelf_duration) VALUES
('121682', '1', '1', 'copy_stitch', NULL, '2019-09-04 12:10:50', NULL, '2019-09-04 12:19:24', NULL, NULL, NULL, NULL),
('121683', '2', '1', 'copy_stitch', NULL, '2019-09-04 12:10:50', NULL, '2019-09-04 12:19:24', NULL, NULL, NULL, NULL);

TRUNCATE TABLE probedata.stitching_probe_info;
INSERT INTO probedata.stitching_probe_info (pk, stitching_scene_info_fk, probe_fk, group_id, sub_group_id, zindex,
 transform_0_0, transform_0_1, transform_0_2, transform_1_0, transform_1_1, transform_1_2, transform_2_0,
  transform_2_1, transform_2_2) VALUES ('975915', '121682', '2187124', '0', '0', '0', '0.5091549277380019', '-0.12541454542758315', '707.7204201318028', '0.04636412888493394', '0.3455915782831826', '351.4837065844982', '0.00009087432193519817', '-0.0000998993193258421', '1'),
('975916', '121682', '2187121', '0', '0', '1', '0.5119771378773853', '0.06465050632929253', '766.2400775131013', '0.022263813827079433', '0.4401206285418588', '-224.70131268183695', '0.00005137320179831387', '0.00004831432200487323', '1'),
('975917', '121682', '2187123', '0', '0', '2', '0.30756612738223577', '0.08156831198306008', '1584.6182880650888', '0.003667066395440073', '0.397580567531726', '-226.67798240115604', '-0.000043238790527485414', '0.000046632446903496506', '1'),
('975918', '121682', '2187122', '0', '0', '3', '0.2826513427710345', '-0.0460582413603336', '1595.7186432627257', '-0.01774839456188139', '0.35892594409326906', '215.6920030451096', '-0.000043031952781952376', '-0.000023877339303100934', '1'),
('975919', '121683', '2187128', '0', '0', '0', '0.5091549277380019', '-0.12541454542758315', '707.7204201318028', '0.04636412888493394', '0.3455915782831826', '351.4837065844982', '0.00009087432193519817', '-0.0000998993193258421', '1'),
('975920', '121683', '2187125', '0', '0', '1', '0.5119771378773853', '0.06465050632929253', '766.2400775131013', '0.022263813827079433', '0.4401206285418588', '-224.70131268183695', '0.00005137320179831387', '0.00004831432200487323', '1'),
('975921', '121683', '2187126', '0', '0', '2', '0.30756612738223577', '0.08156831198306008', '1584.6182880650888', '0.003667066395440073', '0.397580567531726', '-226.67798240115604', '-0.000043238790527485414', '0.000046632446903496506', '1'),
('975922', '121683', '2187127', '0', '0', '3', '0.2826513427710345', '-0.0460582413603336', '1595.7186432627257', '-0.01774839456188139', '0.35892594409326906', '215.6920030451096', '-0.000043031952781952376', '-0.000023877339303100934', '1');

TRUNCATE TABLE static_new.product;
INSERT INTO static_new.product (pk, name, short_name, local_name, ean_code, uuid, brand_fk, category_fk, created_by, creation_date, delete_date, is_active, activation_update_time, type, form_factor, size, unit_of_measure, subpackages_num, unit_num, width, height, depth, smart_caption, smart_display_order, smart_display_group, price_recognition, alcohol_percent, fat_percent, labels, substitution_product_fk, item_code, item_code_type_fk, sub_category_fk, trax_category_fk, global_status_fk, client_status_fk, training_status_fk, global_code, customer_product_id) VALUES
('1', 'Kellogges Coco Pops Cereal Choco Chocolate Cardboard Box 500 g - 5050083559761', 'Kellogges Coco Pops', 'Kellogges Coco Pops', '5050083559761', '76c0eb90-1e5c-11e9-86dd-dbdadbd45d50', '395', '23', 'madawa@gssintl.biz', '2019-01-22 15:43:28', NULL, '1', NULL, 'SKU', 'cardboard box', '500', 'g', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '{\"att2\": \"5050083559761\", \"alt_code_1\": \"5050083559761\", \"alt_code_2\": \"620\", \"alt_code_3\": \"3\", \"Sub Group Local Name\": \"דגני בוקר לילדים\", \"Sub Group English Name\": \"Cereal For Children\"}', NULL, '5050083559761', '2', '1071', '20', '4', NULL, '2', NULL, NULL),
('2', 'Thelma Kariot Cereal Chocolada Cardboard Box 750 g - 2741077', 'Thelma Kariot Cereal', 'Thelma Kariot Cereal 2741077', '2741077', '77bc89a0-1e5c-11e9-86dd-dbdadbd45d50', '394', '23', 'madawa@gssintl.biz', '2019-01-22 15:43:30', NULL, '1', NULL, 'SKU', 'cardboard box', '750', 'g', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '{\"att2\": \"2741077\", \"alt_code_1\": \"2741077\", \"alt_code_2\": \"620\", \"alt_code_3\": \"8\", \"Sub Group Local Name\": \"דגנים ממולאים/כריות\", \"Sub Group English Name\": \"Stuffed Cereals / Pillows\"}', NULL, NULL, NULL, '1071', '20', '4', NULL, '2', NULL, NULL);

TRUNCATE TABLE static_new.product_image;
INSERT INTO static_new.product_image (pk, product_fk, creation_date, image_path, image_direction, face_count, dvc_fk) VALUES
('1', '1', '2019-01-22 15:43:28', 'https://traxus.s3.amazonaws.com/shufersalil/Product_Images/8864/Front20190122154328', 'Front', NULL, NULL),
('2', '1', '2019-01-22 15:43:28', '', 'Back', NULL, NULL),
('3', '1', '2019-01-22 15:43:28', 'https://traxus.s3.amazonaws.com/shufersalil/Product_Images/8864/Top20190425075456', 'Top', NULL, NULL),
('4', '1', '2019-01-22 15:43:28', 'https://traxus.s3.amazonaws.com/shufersalil/Product_Images/8864/Bottom20190425075456', 'Bottom', NULL, NULL),
('5', '1', '2019-01-22 15:43:28', 'https://traxus.s3.amazonaws.com/shufersalil/Product_Images/8864/Side120190422082304', 'Side1', NULL, NULL),
('6', '1', '2019-01-22 15:43:29', 'https://traxus.s3.amazonaws.com/shufersalil/Product_Images/8864/Side220190422082304', 'Side2', NULL, NULL),
('7', '2', '2019-01-22 15:43:30', 'https://traxus.s3.amazonaws.com/shufersalil/Product_Images/8921/Front20190122154330', 'Front', NULL, NULL),
('8', '2', '2019-01-22 15:43:30', '', 'Back', NULL, NULL),
('9', '2', '2019-01-22 15:43:30', '', 'Top', NULL, NULL),
('10', '2', '2019-01-22 15:43:30', '', 'Bottom', NULL, NULL),
('11', '2', '2019-01-22 15:43:30', 'https://traxus.s3.amazonaws.com/shufersalil/Product_Images/8921/Side120190422082305', 'Side1', NULL, NULL),
('12', '2', '2019-01-22 15:43:30', 'https://traxus.s3.amazonaws.com/shufersalil/Product_Images/8921/Side220190422082305', 'Side2', NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;