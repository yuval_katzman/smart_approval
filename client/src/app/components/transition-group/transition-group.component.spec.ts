import {async, ComponentFixture, TestBed, tick} from '@angular/core/testing';

import {TransitionGroupComponent, TransitionGroupItemDirective} from './transition-group.component';
import {Component} from '@angular/core';
import {RelativeStopwatchComponent} from '../relative-stopwatch/relative-stopwatch.component';
import {wait} from '../../../utils';

@Component({
    selector: 'test-component-wrapper',
    template: `
        <!--outer div-->
        <div [transition-group]="'flip-list'">
            <!--inner div-->
            <div appTransitionGroupItem *ngFor="let item of items">
                {{ item }}
            </div>
        </div>
    `
})
class TestComponentWrapper {
    items = [1, 2, 3, 4, 5, 6, 7, 8, 9];
}

describe('TransitionGroupComponent', () => {
    let wrapperComponent: TestComponentWrapper;
    let component: TransitionGroupComponent;
    let fixture: ComponentFixture<TestComponentWrapper>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                TestComponentWrapper,
                TransitionGroupComponent,
                TransitionGroupItemDirective
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TestComponentWrapper);
        wrapperComponent = fixture.componentInstance;
        component = fixture.debugElement.children[0].componentInstance;
        fixture.detectChanges();

    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it(`should change location of items when they change location in the items array`, async () => {
        wrapperComponent.items = [1, 2, 3, 4, 5, 6, 7, 8, 9];
        fixture.detectChanges();
        const items = component.items.toArray();
        const testItem = items[0];
        const prevPos = testItem.prevPos;

        wrapperComponent.items = [2, 4, 6, 8, 1, 3, 5, 7, 9];
        fixture.detectChanges();
        const newPos = testItem.newPos;

        expect((prevPos.x !== newPos.x) || (prevPos.y !== newPos.y)).toBeTruthy();
    });

    it(`should not change location of items when they do not change location in the items array`, async () => {
        wrapperComponent.items = [1, 2, 3, 4, 5, 6, 7, 8, 9];
        fixture.detectChanges();
        const items = component.items.toArray();
        const testItem = items[8];
        const prevPos = testItem.prevPos;

        wrapperComponent.items = [2, 4, 6, 8, 1, 3, 5, 7, 9];
        fixture.detectChanges();
        const newPos = testItem.newPos;

        expect(prevPos.x).toEqual(newPos.x);
        expect(prevPos.y).toEqual(newPos.y);
    });

    describe('inner items', () => {
        it(`should have '-move' class suffix when items array changes`, async () => {
            // children[0] - outer div
            // children[0].children[0] - first inner div
            const innerItemElem = fixture.debugElement.children[0].children[0].nativeElement;
            wrapperComponent.items = [1, 2, 3, 4, 5, 6, 7, 8, 9];
            fixture.detectChanges();

            wrapperComponent.items = [2, 4, 6, 8, 1, 3, 5, 7, 9];
            fixture.detectChanges();

            expect(innerItemElem).toHaveClass('flip-list-move');
        });

        it(`should not have '-move' class suffix after transitionend event fires`, async () => {
            // children[0] - outer div
            // children[0].children[0] - first inner div
            const innerItemElem = fixture.debugElement.children[0].children[0].nativeElement;
            wrapperComponent.items = [1, 2, 3, 4, 5, 6, 7, 8, 9];
            fixture.detectChanges();
            const event: any = document.createEvent('CustomEvent');
            event.propertyName = 'transform';
            event.initEvent('transitionend', true, true);

            wrapperComponent.items = [2, 4, 6, 8, 1, 3, 5, 7, 9];
            fixture.detectChanges();
            innerItemElem.dispatchEvent(event);
            fixture.detectChanges();

            expect(innerItemElem).not.toHaveClass('flip-list-move');
        });
    });

    it(`should not run callback if there is no callback when runCallback is called`, async () => {
        // children[0] - outer div
        // children[0].children[0] - first inner div
        const innerItemElem = fixture.debugElement.children[0].children[0].nativeElement;
        wrapperComponent.items = [1, 2, 3, 4, 5, 6, 7, 8, 9];
        fixture.detectChanges();
        const event: any = document.createEvent('CustomEvent');
        event.propertyName = 'transform';
        event.initEvent('transitionend', true, true);
        const items = component.items.toArray();
        const testItem = items[0];
        wrapperComponent.items = [2, 4, 6, 8, 1, 3, 5, 7, 9];
        fixture.detectChanges();
        const moveCallbackSpy = spyOn<any>(testItem, 'moveCallback').and.callThrough();
        innerItemElem.dispatchEvent(event);
        fixture.detectChanges();

        component.runCallback(testItem);

        expect(moveCallbackSpy).not.toHaveBeenCalled();
    });

    it(`should run callback if there is callback when runCallback is called`, async () => {
        wrapperComponent.items = [1, 2, 3, 4, 5, 6, 7, 8, 9];
        fixture.detectChanges();
        const items = component.items.toArray();
        const testItem = items[0];
        wrapperComponent.items = [2, 4, 6, 8, 1, 3, 5, 7, 9];
        fixture.detectChanges();
        const moveCallbackSpy = spyOn<any>(testItem, 'moveCallback').and.callThrough();

        component.runCallback(testItem);

        expect(moveCallbackSpy).toHaveBeenCalled();
    });
});
