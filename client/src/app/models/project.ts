import {map} from "lodash";

export class Project {
    activation: string;
    additionalAttributes: [];
    additionalAttributesStates: string[];
    categories: Category[];
    currency: string;
    customer: string;
    displayTypes: DisplayType[];
    generalEmpty: number;
    isAdditionalAttributesEnabled: boolean;
    isOnline: boolean;
    isProduction: boolean;
    isOffline: boolean;
    isPriceManualMatching: boolean;
    matchProductInProbeState: string[];
    name: string;
    pricing: boolean;
    projectName: string;
    s3Bucket: string;
    sla: number;
    updated: string;
    validationByTemplate: boolean;

    constructor(rawProject) {
        this.activation = rawProject.activation;
        this.additionalAttributes = rawProject.additional_attributes;
        this.additionalAttributesStates = rawProject.additional_attributes_states;
        this.categories = map(rawProject.categories, (rawCategory) => {
            return new Category(rawCategory);
        });
        this.currency = rawProject.currency;
        this.customer = rawProject.customer;
        this.displayTypes = map(rawProject.displayTypes, (rawDisplayType) => {
            return new DisplayType(rawDisplayType);
        });
        this.generalEmpty = rawProject.generalEmpty;
        this.isAdditionalAttributesEnabled = rawProject.isAddittionalAttributesEnabled;
        this.isOnline = rawProject.isOnline;
        this.isProduction = rawProject.isProduction;
        this.isOffline = rawProject.is_offline;
        this.isPriceManualMatching = rawProject.is_price_manual_matching;
        this.matchProductInProbeState = rawProject.match_product_in_probe_state;
        this.name = rawProject.name;
        this.pricing = rawProject.pricing;
        this.projectName = rawProject.project_name;
        this.s3Bucket = rawProject.s3Bucket;
        this.sla = rawProject.sla;
        this.updated = rawProject.updated;
        this.validationByTemplate = rawProject.validation_by_template;
    }
}

export class Category {
    brands: Brand[];
    displayOrder: number;
    englishName: string;
    id: number;
    name: string;
    constructor(rawCategory) {
        this.brands = map(rawCategory.brands, (rawBrand) => {
            return new Brand(rawBrand);
        });
        this.displayOrder = rawCategory.display_order;
        this.englishName = rawCategory.englishName;
        this.id = rawCategory.id;
        this.name = rawCategory.name;
    }
}

export class Brand {
    displayOrder: number;
    englishName: string;
    facing: string[];
    id: number;
    logo: string;
    name: string;
    products: Product[];
    recognitionLevel: string[];
    title: string;
    constructor(rawBrand) {
        this.displayOrder = rawBrand.display_order;
        this.englishName = rawBrand.englishName;
        this.facing = rawBrand.facing;
        this.id = rawBrand.id;
        this.logo = rawBrand.logo;
        this.name = rawBrand.name;
        this.products = map(rawBrand.products, (rawProduct) => {
            return new Product(rawProduct);
        });
        this.recognitionLevel = rawBrand.recognitionLevel;
        this.title = rawBrand.title;
    }
}

export class Product {
    creationDate: string;
    designChanges: [];
    displayOrder: number;
    englishName: string;
    facings: {
        id: number;
        imageDirection: string;
        imagePath: string;
    }[];
    group: number;
    hardMaximalPrice: number;
    hardMinimalPrice: number;
    id: number;
    imageId: number;
    imagePath: string;
    isActive: boolean;
    isNew: boolean;
    maximalPrice: number;
    minimalPrice: number;
    name: string;
    size: number;
    soonDeleted: boolean;
    title: string;
    type: string;
    unit: string;
    uuid: string;
    constructor(rawProduct) {
        this.creationDate = rawProduct.creationDate;
        this.designChanges = rawProduct.designChanges;
        this.displayOrder = rawProduct.displayOrder;
        this.englishName = rawProduct.englishName;
        this.facings = rawProduct.facings;
        this.group = rawProduct.group;
        this.hardMaximalPrice = rawProduct.hardMaximalPrice;
        this.hardMinimalPrice = rawProduct.hardMinimalPrice;
        this.id = rawProduct.id;
        this.imageId = rawProduct.imageId;
        this.imagePath = rawProduct.imagePath;
        this.isActive = rawProduct.isActive;
        this.isNew = rawProduct.isNew;
        this.maximalPrice = rawProduct.maximalPrice;
        this.minimalPrice = rawProduct.minimalPrice;
        this.name = rawProduct.name;
        this.size = rawProduct.size;
        this.soonDeleted = rawProduct.soonDeleted;
        this.title = rawProduct.title;
        this.type = rawProduct.type;
        this.unit = rawProduct.unit;
        this.uuid = rawProduct.uuid;
    }
}

export class DisplayType {
    displayBrands: DisplayBrand[];
    englishName: string;
    id: number;
    imagePath: string;
    constructor(rawDisplayType) {
        this.displayBrands = map(rawDisplayType.display_brands, (rawDisplayBrand) => {
            return new DisplayBrand(rawDisplayBrand);
        });
        this.englishName = rawDisplayType.englishName;
        this.id = rawDisplayType.id;
        this.imagePath = rawDisplayType.imagePath;
    }
}

export class DisplayBrand {
    displays: Display[];
    englishName: string;
    id: number;
    logo: string;
    constructor(rawDisplayBrand) {
        this.displays = map(rawDisplayBrand.displays, (rawDisplay) => {
            return new Display(rawDisplay);
        });
        this.englishName = rawDisplayBrand.englishName;
        this.id = rawDisplayBrand.id;
        this.logo = rawDisplayBrand.logo;
    }
}

export class Display {
    displayName: string;
    englishName: string;
    id: number;
    imagePath: string;
    constructor(rawDisplay) {
        this.displayName = rawDisplay.display_name;
        this.englishName = rawDisplay.englishName;
        this.id = rawDisplay.id;
        this.imagePath = rawDisplay.imagePath;
    }
}
