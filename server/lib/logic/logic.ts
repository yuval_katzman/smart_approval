import {map, uniq} from "lodash";
import {AuthenticatedRequest, SmartUser} from "../defs";
import {cancelTask, count, extendLock, finishTask, getNext} from "../dal/selector";
import {
    addUserStatistics,
    documentUserCancel, getTemplate,
    saveTaskToApprovalResult,
    updateProjectStats,
    getTemplateGroupIds,
    updateUserLoginInfo
} from '../dal/dal';
import { NextFunction, Response } from 'express';
import {EntityType, SaveTask, TaskType} from "../interfaces";
import * as metric from '../dal/metric'
import {Retryable} from "typescript-retry-decorator";
import {StsService} from "../rw-server-utls";

const nc = require('@trax-retail/node-core')('smart-approval');
const eventsType = {"oos_approval": "APPROVAL-TASK_FINISHED", "approval_voting" :"APPROVAL-TASK_SINGLE_VOTED",
                    "approvals": "APPROVAL-TASK_FINISHED", "approvals_voting" :"APPROVAL-TASK_SINGLE_VOTED"};
export class Logic {
    async getQueueCount(user: SmartUser, projectName: string, templateId?:number, templateGroup?:string, subQueue?: string, traxCategoryUuid?: string) {
        // return 1;
        let ApprovalCount, approvalVotingCount;
        if (!subQueue){
            [ApprovalCount, approvalVotingCount] = await Promise.all([TaskType.oosApproval, TaskType.approvalVoting].map(type => count(user, type, projectName, templateId, templateGroup, undefined, traxCategoryUuid)));
        } else {
            [ApprovalCount, approvalVotingCount]
                = await Promise.all([TaskType.approvals, TaskType.approvalsVoting].map(type => count(user, type, projectName, templateId, templateGroup, subQueue, traxCategoryUuid)));

        }
        return (ApprovalCount || 0) + (approvalVotingCount || 0);
    }

    @Retryable({ maxAttempts: 3 })
    async getNextTask(user: SmartUser, projectName: string, templateId?:number, templateGroup?:string, subQueue?: string, traxCategoryUuid?: string) {
        let oosTask,
            votingTask,
            templateIds;

        if (templateGroup) {
            templateIds = await getTemplateGroupIds(projectName, templateGroup)
        }

        if (!subQueue){
            [oosTask, votingTask] = await Promise.all(
                [TaskType.oosApproval, TaskType.approvalVoting].map(type => getNext(user, type, projectName, templateId, templateIds, templateGroup, subQueue, traxCategoryUuid)));
        } else{
            [oosTask, votingTask] = await Promise.all(
                [TaskType.approvals, TaskType.approvalsVoting].map(type => getNext(user, type, projectName, templateId, templateIds, templateGroup, subQueue, traxCategoryUuid)));
        }

        const task = oosTask || votingTask;

        if(!task) {
            return;
        }

        // return  require('../mocktask.json')
        try {

            metric.startTask(user, task.selectorTaskId, projectName, task.taskType, task.scene_id);
            task.sceneId = task.sceneId || task.scene_id;
            const productIds = map(task.questions, 'product_fk');
            const isSceneTask =  !task.entity_type || task.entity_type === EntityType.scene;
            const [sceneOrProbeData, productData] = await Promise.all([
                isSceneTask ?
                    this.getSceneData(projectName, task.sceneId || task.entity_id) :
                    this.getProbeData(projectName, task.entity_id),
                this.getProductData(projectName, productIds)
            ]);
            if (isSceneTask){
                task.sceneData = sceneOrProbeData;
            } else{
                task.probeData = sceneOrProbeData;
            }

            task.productData = productData;

            task.questions = task.questions.filter(q => task.productData.find(p => p.id === q.product_fk));

            nc.logger.info({
                job_type: task.job_type,
                task_type: task.taskType,
                method_name:'getNextTask',
                session_uid: task.session_uid,
                scene_uid: task.scene_uid,
                scene_id: task.scene_id,
                selector_task_id: task.selectorTaskId,
                user_name:user.staff_id,
                email: user.email,
                project_name: projectName,
                sub_queue: subQueue,
                template_id: templateId,
                template_group: templateGroup,
                trax_category_uuid: traxCategoryUuid,
                approval_id: task.selectorTask.additionalInfo.approval_uid,
                approval_uid: task.approval_uid,
            }, 'Received message');
            return task as any;
        } catch (e) {
            await cancelTask(user, task.selectorTaskId, projectName,true);
            throw e;
        }

    }

    async cancelTaskAndLog(user: SmartUser, sessionId: string, projectName: string,
                           selectorTaskId: string, sceneId: number, reason: string, taskType:string) {
        metric.cancelTask(user, reason, selectorTaskId, projectName, taskType, sceneId);
        return Promise.all([
            new Promise((resolve, reject) => {
                // can throw error if cancelled after losing lock in the event of cancelOnRefresh
                cancelTask(user, selectorTaskId, projectName).then(resolve).catch(resolve);
            }),
            documentUserCancel(sessionId, projectName, taskType, sceneId, 'scene', reason)
        ]);
    }

    async breakTask(user: SmartUser, reason: string) {
        metric.breakTask(user, reason);
    }

    async extendLockAndStuff(user: SmartUser, projectName: string, selectorTaskId: string) {
        return extendLock(user, selectorTaskId, 1000 * 60 * 5, projectName);
    }

    async saveTask(user: SmartUser, sessionId: string, projectName: string, selectorTaskId: string, body: SaveTask) {
        try {
            const eventName = eventsType[body.task_name];
            const message = {
                netDurationInSeconds: body.netDurationInSeconds,
                processing_time: body.netDurationInSeconds,
                queue_duration: body.queueDurationInSeconds,
                wave_type: body.wave_type,
                wave_uid: body.wave_uid,
                project_name: body.project_name,
                persistence: body.persistence,
                job_type: body.job_type,
                session_uid: body.session_uid,
                scene_uid: body.scene_uid,
                scene_id: body.scene_id,
                entity_id: body.originalTask.additionalInfo.entity_id,
                entity_type: body.originalTask.additionalInfo.entity_type,
                sub_queue: body.originalTask.additionalInfo.sub_queue,
                answers: body.answers,
                event_name: eventName,
                user: user.staff_id,
                email: user.email,
                approval_id: body.approval_id,
                approval_uid: body.originalTask.additionalInfo.approval_uid,
                task_name: body.task_name,
                number_of_tasks: body.answers?.length || 0,
            };
            nc.logger.info({
                job_type: body.job_type,
                method_name:'saveTask',
                session_uid: body.session_uid,
                scene_uid: body.scene_uid,
                scene_id: body.scene_id,
                selector_task_id: selectorTaskId,
                user_name:user.staff_id,
                email: user.email,
                project_name: body.project_name,
                net_duration_in_seconds: body.netDurationInSeconds,
                sub_queue: body.originalTask.additionalInfo.sub_queue,
                template_id: body.templateId,
                template_group: body.templateGroup,
                trax_category_uuid: body.traxCategoryUuid,
                approval_id: body.approval_id,
                approval_uid: body.originalTask.additionalInfo.approval_uid
            }, 'Starting save approval task');

            metric.finishTask(user, selectorTaskId, projectName, body.task_name, body.scene_id);
            await Promise.all([
                saveTaskToApprovalResult(user, body),
                updateUserLoginInfo(projectName, sessionId, body.netDurationInSeconds),
                updateProjectStats(projectName),
                addUserStatistics(user, projectName, sessionId, body),
                nc.cloud.topic.publish(message)

            ]);
            nc.logger.info(message,eventName + ' event fired');

            await finishTask(user, selectorTaskId, projectName);

        }
        catch (err) {
            nc.logger.error({
                err
            }, 'Error finish task');

            throw err;
        }
    }

    private async getProductData(projectName: string, productIds: number[]) {
        const url = `${nc.config.pnbUrl}/pnb/api/projects/${projectName}/products?filter[id]=${uniq(productIds).join(',')}`;
        const productData = await StsService.get<{data: any}>(projectName, url);
        if (!productData?.data?.length){
            nc.logger.error({
                productIds
            }, 'P&B do not have data for require products');
            throw new Error('P&B do not have data for require products');
        }

        return productData.data;
    }

    private async getSceneData(projectName: string, sceneId: number) {
        return StsService.get(projectName, `${nc.config.xSuiteUrl}/trax-one/api/projects/${projectName}/scene/${sceneId}`);
    }

    private async getProbeData(projectName: string, probeId: number) {
        const url = `${nc.config.xSuiteUrl}/trax-one/api/projects/${projectName}/probe/${probeId}/readonly`;
        const probeData = await StsService.get<{probe_id: number, probe_image_path: string, original_width: string, original_height: string}>(projectName, url);
        return {probeId: probeData.probe_id,  probeUrl: probeData.probe_image_path.replace('/original', ''), originalWidth: probeData.original_width, originalHeight: probeData.original_height};
    }

    // should be used on every route that indicated that the user interacted with the app
    // for example: getNextProbe, but not getQueueCount, because getQueueCount happens without user interaction
    resetCookie(req: AuthenticatedRequest, res: Response, next: () => any) {
        req.session.touch();
        // we change the session object to cause a set cookie header on the response
        // express sessions calculates hashes of the original session and the current session
        // to understand if set cookie header is needed.... they compare the cookie object =(
        // this is a known issue
        req.session.rolling = !req.session.rolling;
        next();
    }

    async getTemplateById(projectName:string, templateId: number){
        return  getTemplate(projectName, templateId)
    }
}
