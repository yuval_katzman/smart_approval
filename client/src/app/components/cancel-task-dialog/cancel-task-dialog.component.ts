import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";

@Component({
  selector: 'app-cancel-task-dialog',
  templateUrl: './cancel-task-dialog.component.html',
  styleUrls: ['./cancel-task-dialog.component.scss']
})
export class CancelTaskDialogComponent {

  reason: string;
  reasonText: "";
  options: {value: string, content: string}[] = [
    {value: 'technicalIssue', content: ' Technical issue'},
    {value: 'iAmNotAQat', content: ' I am not a QAT'},
    {value: 'missingImage', content: ' Missing image'},
    {value: 'noQuestion', content: ' No question'},
    {value: 'missingInformation', content: ' Missing information'},
    {value: 'other', content: ' Other - please explain'},
  ];

  constructor(public dialogRef: MatDialogRef<CancelTaskDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) { }
  cancel() {
    this.dialogRef.close({
      cancel: true,
      reason: this.reason === 'other' ? this.reasonText : this.reason
    });
  }
  doNotCancel() {
    this.dialogRef.close({
      cancel: false
    });
  }
}
