### Developing in INT environment

### Run Smart Approvals
1. Make sure your node version is 12.16.2 or similar using nvm
2. run npm install in project root folder
3. run start:INT in client's package.json
4. run 'npx tsc' in server folder in order to build typescript 
5. run start:int in server's package.json
6. open http://localhost:4200/login?redirectUrl=http://localhost:4200/approvals/integ5/validate in browser

follow [this link](https://confluence.trax-cloud.com/display/RW/2021.01.04+Create+Approval+Tasks+on+INT) how to import tasks from production or add custom task with ```npm run gen_task```
open http://localhost:8081/login?redirectUrl=http://localhost:4200/approvals/integ38/validate?subQueue=LowFacingApproval

### Mock task with production data
- locate the original task in ```server/lib/mock/msg.json```
- run ```npm run prepare-mock-data```
- uncomment the relevant lines in ```server/lib/logic/logic.ts```
- run with TREX_ENV=DEV


