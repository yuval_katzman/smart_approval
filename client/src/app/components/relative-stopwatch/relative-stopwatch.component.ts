import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import * as moment from "moment";
@Component({
  selector: 'app-relative-stopwatch',
  templateUrl: './relative-stopwatch.component.html',
  styleUrls: ['./relative-stopwatch.component.scss']
})
export class RelativeStopwatchComponent implements OnInit, OnChanges {
  @Input() restartOn: boolean;
  @Input() stopper: boolean;

  INTERVAL = 500;
  displayValue: string;
  interval: number;

  constructor() { }

  private watch = {
    scope: {
      refTime: 0,
      timing: 0,
      running: false
    },

    reset: () => {
      this.watch.scope.refTime = Date.now();
      this.watch.scope.timing = 0;
      this.updateDisplay();
    },

    stop: () => {
      clearInterval(this.interval);
      this.interval = undefined;
      this.watch.scope.running = false;
    },

    continue: () => {
      this.watch.scope.running = true;
      this.watch.start();
    },

    start: () => {
      if (!this.interval) {
        this.watch.scope.refTime = Date.now() - this.watch.scope.timing;

        this.interval = window.setInterval(() => {
          this.watch.scope.timing = Date.now() - this.watch.scope.refTime;
          this.updateDisplay();
        }, this.INTERVAL, 0, false);
      }
    },

    restart: () => {
      this.watch.reset();
      this.watch.start();
    }
  };

  private updateDisplay() {
    this.displayValue = moment()
        .hour(0)
        .minute(0)
        .second(0)
        .millisecond(this.watch.scope.timing).format('mm:ss');
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.restartOn && changes.restartOn.currentValue) {
      this.watch.restart();
    }
    if (changes.stopper && !changes.stopper.currentValue) {
      this.watch.stop();
    }
    if (changes.stopper && changes.stopper.currentValue) {
      this.watch.start();
    }
  }

}
