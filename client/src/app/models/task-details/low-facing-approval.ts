import {YesNoCantTellQuestion, YesNoQuestion} from "./interface";

export class LowFacingApprovalYesNo implements YesNoQuestion {
    constructor(public productName: string, public value: string, public returnValue = false) {}
    readonly questionText = `Do you see at least ${this.value} facings of ${this.productName} on shelf?`;

    readonly valueType = 'number';
    readonly noAnswer: string = `No, there are less then ${this.value} facing`;
    readonly yesAnswer: string = `Yes, there are ${this.value} or more facing`;

    // this is relevant only when returnValue is true
    readonly valuePlaceHolder = '0';
    readonly noValueAnswer: string = "No, i see ";
}

export class LowFacingApprovalYesNoCantTell extends LowFacingApprovalYesNo implements YesNoCantTellQuestion {
    constructor(public productName: string, public value: string, public returnValue = false) {
        super(productName, value);
    }

    readonly cantTellAnswer: string = "This is not the right product";
}


