import {Injectable} from '@angular/core';

const INDEXED_DB_NAME = 'SmartDB';

@Injectable({
  providedIn: 'root'
})
export class IndexedDbService {

  constructor() {
  }

  public createDB(objectStoreKeys: string[]): Promise<null> {
    return new Promise((resolve, reject) => {
      let dbExists = true;
      const request = indexedDB.open(INDEXED_DB_NAME, 1);
      let db: IDBDatabase;
      request.onupgradeneeded = (event: any) => {
        dbExists = false;
        db = event.target.result;
        objectStoreKeys.forEach((key) => {
          db.createObjectStore(key);
        });
      };
      request.onsuccess = () => {
        if (!dbExists) {
          db.close();
        }
        resolve();
      };
      request.onerror = (err) => {
        reject(err);
      };
    });
  }

  public add(storeKey: string, key: string, item: {}) {
    const openDBRequest: IDBRequest = indexedDB.open(INDEXED_DB_NAME, 1);
    openDBRequest.onsuccess = (event: any) => {
      const db = event.target.result;
      const addRequest: IDBRequest = db.transaction(storeKey, 'readwrite')
          .objectStore(storeKey)
          .add(item, key);
      addRequest.onsuccess = () => {
        db.close();
      };
      addRequest.onerror = () => {
      };
    };
    openDBRequest.onerror = () => {
    };
  }

  read<T>(storeKey: string, key: string): Promise<T> {
    return new Promise((resolve, reject) => {
      const openDBRequest: IDBRequest = indexedDB.open(INDEXED_DB_NAME, 1);
      openDBRequest.onsuccess = (event: any) => {
        const db = event.target.result;
        let readRequest: IDBRequest;
        if (db.objectStoreNames.contains(storeKey)) {
          readRequest = db.transaction(storeKey, 'readwrite')
              .objectStore(storeKey)
              .get(key);
        } else {
          reject(new Error('Invalid store name' + key));
        }

        readRequest.onsuccess = () => {
          if (readRequest.result) {
            resolve(readRequest.result);
          } else {
            // In case there's no data, returns empty.
            resolve(null);
          }
          db.close();
        };
        readRequest.onerror = () => {
        };
      };
      openDBRequest.onerror = () => {
      };
    });
  }

  checkIfDBExists(): Promise<boolean> {
    return new Promise((resolve) => {
      const openDBRequest = indexedDB.open(INDEXED_DB_NAME, 1);
      openDBRequest.onupgradeneeded = (event: any) => {
        event.target.transaction.abort();
        resolve(false);
      };
      openDBRequest.onsuccess = () => {
        resolve(true);
      };
    });
  }

  deleteDB(): Promise<null | Error> {
    return new Promise((resolve, reject) => {
      const DBDeleteRequest = window.indexedDB.deleteDatabase(INDEXED_DB_NAME);

      DBDeleteRequest.onerror = (event) => {
        reject(new Error('Failed to delete database'));
      };

      DBDeleteRequest.onsuccess = (event) => {
        resolve();
      };
    });
  }
}

