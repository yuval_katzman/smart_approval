import {TestBed} from '@angular/core/testing';

import {CancelOnRefreshService} from './cancel-on-refresh.service';
import {Task} from '../../models/task';
import {HttpClientModule} from '@angular/common/http';
import {ActivatedRoute} from "@angular/router";

const createNewTask = (selectorTaskId: string,
                       projectName: string,
                       objectId: number,
                       persistence: any,
                       queueDurationInSeconds: number,
                       jobType?: string,
                       taskType = 'oos_approval'
                       ) => {
    return {taskType, selectorTaskId, projectName, objectId, persistence, queueDurationInSeconds, jobType} as Task;
};

describe('CancelOnRefreshService', () => {

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientModule],
            providers: [{
                provide: ActivatedRoute,
                useValue: {snapshot: {queryParams: {}}}
            }]
        });

        window.localStorage.clear();
    });

    it('should be created', () => {
        const service: CancelOnRefreshService = TestBed.get(CancelOnRefreshService);
        expect(service).toBeTruthy();
    });

    describe('store in localStorage', () => {
        it('should store when setting task to cancel on refresh', () => {
            const service: CancelOnRefreshService = TestBed.get(CancelOnRefreshService);
            const task = createNewTask('1111-1111', 'test', 1, {}, 10);
            const task2 = createNewTask('2222-2222', 'test', 2, {}, 10);
            service.setTaskToCancelOnRefresh(task);
            service.setTaskToCancelOnRefresh(task2);

            expect(JSON.parse(window.localStorage.getItem('pendingoos_approval'))).toEqual([
                {
                    taskType: task.taskType,
                    selectorTaskId: task.selectorTaskId,
                    projectName: task.projectName,
                    objectId: task.objectId
                },
                {
                    taskType: task2.taskType,
                    selectorTaskId: task2.selectorTaskId,
                    projectName: task2.projectName,
                    objectId: task2.objectId
                }
            ]);
        });


        it('should not store when item in localstorage is not an array', () => {
            const service: CancelOnRefreshService = TestBed.get(CancelOnRefreshService);
            const task = createNewTask('1111-1111', 'test', 1, {}, 10);
            const dummyJson = {
                pet: 'dog'
            };
            window.localStorage.setItem(`pending${task.taskType}`, JSON.stringify(dummyJson));

            service.setTaskToCancelOnRefresh(task);

            expect(JSON.parse(window.localStorage.getItem('pendingoos_approval'))).toEqual(dummyJson);
        });
    });

    describe('remove from localStorage', () => {
        it('should remove when setting task to cancel on refresh, setting task no to cancel on refresh then refresh', () => {
            const service: CancelOnRefreshService = TestBed.get(CancelOnRefreshService);
            const task = createNewTask('1111-1111', 'test', 1, {}, 10);
            const task2 = createNewTask('2222-2222', 'test', 2, {}, 10);
            service.setTaskToCancelOnRefresh(task);
            service.setTaskToCancelOnRefresh(task2);

            service.dontCancelOnRefresh(task2);

            expect(JSON.parse(window.localStorage.getItem('pendingoos_approval'))).toEqual([
                {
                    taskType: task.taskType,
                    selectorTaskId: task.selectorTaskId,
                    projectName: task.projectName,
                    objectId: task.objectId
                }
            ]);
        });

        it('should not remove when item in localstorage is not an array', () => {
            const service: CancelOnRefreshService = TestBed.get(CancelOnRefreshService);
            const task = createNewTask('1111-1111', 'test', 1, {}, 10);
            const taskJson = {
                taskType: task.taskType,
                selectorTaskId: task.selectorTaskId,
                projectName: task.projectName,
                objectId: task.objectId
            };
            window.localStorage.setItem(`pending${task.taskType}`, JSON.stringify(taskJson));
            service.setTaskToCancelOnRefresh(task);

            service.dontCancelOnRefresh(task);

            expect(JSON.parse(window.localStorage.getItem('pendingoos_approval'))).toEqual(taskJson);
        });
    });
});
