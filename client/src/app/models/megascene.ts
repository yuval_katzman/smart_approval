import {forEach} from "lodash";

export interface RawMegaScene {
    sceneId?: number;
    scene: {
        scene_id: number,
        project_name: string;
    };
    stitchingData: {
        displayTypeId: number,
        molecules: any;
        probes: any;
        dividers: any;
        edges: any;
    };
    projectName?: string;
    lock: any;
}

export class Megascene {
    sceneId: number | string;
    displayTypeId: number;
    projectName: string;
    molecules: any;
    probes: any;
    lines: MegaLine[];
    edges: any;
    messageLock: any;

    constructor(rawMegaScene: RawMegaScene) {
        this.sceneId = rawMegaScene.sceneId || rawMegaScene.scene.scene_id || 'No scene id';

        this.displayTypeId = rawMegaScene.stitchingData.displayTypeId;

        this.projectName = rawMegaScene.projectName || rawMegaScene.scene.project_name || 'No project name';

        this.molecules = rawMegaScene.stitchingData.molecules;

        this.probes = rawMegaScene.stitchingData.probes;

        this.lines = ((() => {
            const lines = [];
            forEach(rawMegaScene.stitchingData.dividers, divider => {
                lines.push(new MegaLine(divider));
            });
            return lines;
        }))();

        this.edges = rawMegaScene.stitchingData.edges;

        this.messageLock = rawMegaScene.lock;
        for (let moleculeIndex = 0; moleculeIndex < this.molecules.length; moleculeIndex++) {
            this.molecules[moleculeIndex].index = moleculeIndex;
        }

        return this;
    }
}

class MegaLine {
    line: {
        start: {
            x: number,
            y: number
        },
        end: {
            x: number,
            y: number
        },
        type: string
    };
    constructor(data: {
        x1: number,
        y1: number,
        x2: number,
        y2: number,
        type: string
    }) {
        this.line = {
            start: {
                x: Number(data.x1),
                y: Number(data.y1)
            },
            end: {
                x: Number(data.x2),
                y: Number(data.y2)
            },
            type: data.type
        };
    }
}
