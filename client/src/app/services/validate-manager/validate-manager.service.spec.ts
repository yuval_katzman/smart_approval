import {discardPeriodicTasks, fakeAsync, TestBed, tick} from '@angular/core/testing';
import { ValidateManagerService } from './validate-manager.service';
import {map} from "lodash";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {API} from "../API/api.service";
import {Observable, of} from "rxjs";
import {CancelOnRefreshService} from "../cancelOnRefresh/cancel-on-refresh.service";
import {HttpParams} from "@angular/common/http";
import {ActivatedRoute} from "@angular/router";
import {Task} from "../../models/task";
const rawTask = require('./test/rawTask.json');

describe('ValidateManagerService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [{
      provide: ActivatedRoute,
      useValue: {snapshot: {queryParams: {}}}
    }]
  }));

  it('should be created', () => {
    const service: ValidateManagerService = TestBed.get(ValidateManagerService);
    expect(service).toBeTruthy();
  });

  it('getQueueCount should work', () => {
    const service: ValidateManagerService = TestBed.get(ValidateManagerService);
    const apiMock = TestBed.get(API);

    const apiGetSpy = spyOn(apiMock, 'get');

    apiGetSpy.and.returnValue(new Observable(observer => observer.next({count: 666})));

    service.getQueueCount('test').subscribe((res) => {
      expect(res.count).toBe(666);
    });

    expect(apiGetSpy).toHaveBeenCalledWith('/approvals/api/:projectName/queue-count', {
      pathParams: {
        projectName: 'test'
      },
      params: {}
    });
  });

  it('getNextTask should return task', () => {
    const service: ValidateManagerService = TestBed.get(ValidateManagerService);
    const apiMock = TestBed.get(API);

    const apiGetSpy = spyOn(apiMock, 'get');

    const task = new Task(rawTask);

    apiGetSpy.and.returnValue(new Observable(observer => observer.next(rawTask)));

    service.getNextTask('test').subscribe((res) => {
      // stringifying because the next line fails when trying to compare functions on the objects
      // functions are equal if their memory address is equal
      expect(JSON.stringify(res)).toEqual(JSON.stringify(task));
    });

    expect(apiGetSpy).toHaveBeenCalledWith('/approvals/api/:projectName/next-task', {
      pathParams: {
        projectName: 'test'
      }, params: {}
    });

  });

  it('getNextTask should return nothing if no task', () => {
    const service: ValidateManagerService = TestBed.get(ValidateManagerService);
    const apiMock = TestBed.get(API);

    const apiGetSpy = spyOn(apiMock, 'get');

    apiGetSpy.and.returnValue(new Observable(observer => observer.next()));

    service.getNextTask('test').subscribe((res) => {
      expect(res).toBeFalsy();
    });

    expect(apiGetSpy).toHaveBeenCalledWith('/approvals/api/:projectName/next-task', {
      pathParams: {
        projectName: 'test'
      }, params: {}
    });
  });

  it('getNextTask should set task in cancelOrRefresh', () => {
    const service: ValidateManagerService = TestBed.get(ValidateManagerService);
    const apiMock = TestBed.get(API);
    const cancelOnRefreshMock = TestBed.get(CancelOnRefreshService);

    const setTaskToCancelOnRefreshMock = spyOn(cancelOnRefreshMock, 'setTaskToCancelOnRefresh').and.returnValue(null);
    const apiGetSpy = spyOn(apiMock, 'get');

    apiGetSpy.and.returnValue(new Observable(observer => observer.next(rawTask)));

    service.getNextTask('test').subscribe();

    expect(apiGetSpy).toHaveBeenCalledWith('/approvals/api/:projectName/next-task', {
      pathParams: {
        projectName: 'test'
      }, params: {}
    });

    expect(setTaskToCancelOnRefreshMock).toHaveBeenCalled();
  });

  it('getNextTask should start extendLock interval', fakeAsync(() => {
    const service: ValidateManagerService = TestBed.get(ValidateManagerService);
    const apiMock = TestBed.get(API);

    const apiGetSpy = spyOn(apiMock, 'get');
    const apiPutMock = spyOn(apiMock, 'put');

    apiGetSpy.and.returnValue(new Observable(observer => observer.next(rawTask)));
    apiPutMock.and.returnValue(new Observable(observer => observer.next()));

    service.getNextTask('test').subscribe();

    tick(1000 * 60 * 3);

    expect(apiPutMock).toHaveBeenCalledWith('/approvals/api/:projectName/extend-lock', {}, {
      pathParams: {
        projectName: 'test'
      },
      params: new HttpParams().append('selectorTaskId', rawTask.selectorTaskId)
    });

    discardPeriodicTasks();

  }));

  it('cancelTask should cancel task', () => {
    const service: ValidateManagerService = TestBed.get(ValidateManagerService);
    const apiMock = TestBed.get(API);

    const apiPutSpy = spyOn(apiMock, 'put').and.returnValue(new Observable(observer => observer.next()));
    service.cancelTask('test', '1111-1111', 1, `Yo mama`).subscribe();

    expect(apiPutSpy).toHaveBeenCalledWith('/approvals/api/:projectName/cancel-task', {
      sceneId: 1,
      reason: 'Yo mama',
      taskType: 'oos_approval'
    }, {
      pathParams: {
        projectName: 'test'
      },
      params: new HttpParams().append('selectorTaskId', '1111-1111')
    });
  });

  it('cancelTask should cancel extendLock interval', fakeAsync(() => {
    const service: ValidateManagerService = TestBed.get(ValidateManagerService);
    const apiMock = TestBed.get(API);

    const apiGetSpy = spyOn(apiMock, 'get');
    const apiPutSpy = spyOn(apiMock, 'put');

    apiGetSpy.and.returnValue(new Observable(observer => observer.next(rawTask)));
    apiPutSpy.and.returnValue(new Observable(observer => observer.next()));

    service.getNextTask('test').subscribe();

    service.cancelTask(rawTask.projectName, rawTask.selectorTaskId, rawTask.sceneId, 'because').subscribe();

    tick(1000 * 60 * 3);

    expect(apiPutSpy).not.toHaveBeenCalledWith('/approvals/api/:projectName/extend-lock', {}, {
      pathParams: {
        projectName: 'test'
      },
      params: new HttpParams().append('selectorTaskId', rawTask.selectorTaskId)
    });

  }));

  it('cancelTask should remove task from cancelOnRefresh', () => {
    const service: ValidateManagerService = TestBed.get(ValidateManagerService);
    const apiMock = TestBed.get(API);
    const cancelOnRefreshMock = TestBed.get(CancelOnRefreshService);

    const apiGetSpy = spyOn(apiMock, 'get');
    const apiPutSpy = spyOn(apiMock, 'put');
    const dontCancelOnRefreshSpy = spyOn(cancelOnRefreshMock, 'dontCancelOnRefresh');

    apiGetSpy.and.returnValue(new Observable(observer => observer.next(rawTask)));
    apiPutSpy.and.returnValue(new Observable(observer => observer.next()));

    service.getNextTask('test').subscribe();

    service.cancelTask(rawTask.projectName, rawTask.selectorTaskId, rawTask.sceneId, 'because').subscribe();

    expect(dontCancelOnRefreshSpy).toHaveBeenCalled();
  });

  it('saveTask should save task', () => {
    const service: ValidateManagerService = TestBed.get(ValidateManagerService);
    const apiMock = TestBed.get(API);

    const apiGetSpy = spyOn(apiMock, 'get');
    const apiPostSpy = spyOn(apiMock, 'post');

    apiGetSpy.and.returnValue(new Observable(observer => observer.next(rawTask)));
    apiPostSpy.and.returnValue(new Observable(observer => observer.next()));

    service.getNextTask('test').subscribe();

    service.saveTask();

    expect(apiPostSpy).toHaveBeenCalledWith('/approvals/api/:projectName/save-task', jasmine.objectContaining({
      wave_type: rawTask.waveType,
      wave_uid: rawTask.waveUid,
      project_name: rawTask.projectName,
      job_type: rawTask.job_type,
      session_uid: rawTask.session_uid,
      scene_uid: rawTask.scene_uid,
      scene_id: rawTask.sceneId,
      persistence: rawTask.persistence,
      answers: map(rawTask.questions, (question) => {
        return {
          question_uid: question.question_uid,
          product_fk: question.product_fk,
          question_category: question.question_category,
          question_type: question.question_type,
          answer: null,
          duration: question.duration
        };
      })
    }), {
      pathParams: {
        projectName: rawTask.projectName
      },
      params: new HttpParams().append('selectorTaskId', rawTask.selectorTaskId)
    });
  });

  it('saveTask should cancel extendLock interval', fakeAsync(() => {
    const service: ValidateManagerService = TestBed.get(ValidateManagerService);
    const apiMock = TestBed.get(API);

    const apiGetSpy = spyOn(apiMock, 'get');
    const apiPostSpy = spyOn(apiMock, 'post');

    apiGetSpy.and.returnValue(new Observable(observer => observer.next(rawTask)));
    apiPostSpy.and.returnValue(new Observable(observer => observer.next()));

    service.getNextTask('test').subscribe();

    service.saveTask();

    tick(1000 * 60 * 3);

    expect(apiPostSpy).not.toHaveBeenCalledWith('/approvals/api/:projectName/extend-lock', {}, {
      pathParams: {
        projectName: 'test'
      },
      params: new HttpParams().append('selectorTaskId', rawTask.selectorTaskId)
    });
  }));

  it('saveTask should remove task from cancelOnRefresh', () => {
    const service: ValidateManagerService = TestBed.get(ValidateManagerService);
    const apiMock = TestBed.get(API);
    const cancelOnRefreshMock = TestBed.get(CancelOnRefreshService);

    const apiGetSpy = spyOn(apiMock, 'get');
    const apiPostSpy = spyOn(apiMock, 'post');
    const dontCancelOnRefreshSpy = spyOn(cancelOnRefreshMock, 'dontCancelOnRefresh');

    apiGetSpy.and.returnValue(new Observable(observer => observer.next(rawTask)));
    apiPostSpy.and.returnValue(new Observable(observer => observer.next()));

    service.getNextTask('test').subscribe();

    service.saveTask();

    expect(dontCancelOnRefreshSpy).toHaveBeenCalled();
  });

  describe("get product images", async  () => {
    it("sanity test", () => {
      const service: ValidateManagerService = TestBed.get(ValidateManagerService);
      const product = {
        images: [
          {
            id: 477593,
            path: "img1",
            direction: "Front"
          }
        ],
        designChanges: [
          {
            id: 2602,
            name: "4008429105036",
            startDate: "2020-01-09T00:00:00.000Z",
            endDate: null,
            displayInSmartValidationPallet: true,
            images: [
              {
                id: 555944,
                path: "img2",
                direction: "Front"
              }
            ]
          }
        ]
      };
      const images = service.getProductImages(product);
      expect(images).toEqual([{ id: 477593, path: 'img1', direction: 'Front' }, { id: 555944, path: 'img2', direction: 'Front' }]);
    });

    it("should return only 4 images - all from product images", () => {
      const service: ValidateManagerService = TestBed.get(ValidateManagerService);
      const pImages = Array(4).fill({ id: 477593, path: 'img1', direction: 'Front' });
      const product = {
        images: pImages,
        designChanges: [
          {
            id: 2602,
            name: "4008429105036",
            startDate: "2020-01-09T00:00:00.000Z",
            endDate: null,
            displayInSmartValidationPallet: true,
            images: [
              {
                id: 555944,
                path: "img2",
                direction: "Front"
              }
            ]
          }
        ]
      };
      const images = service.getProductImages(product);
      expect(images).toEqual(pImages);
    });

    it("should return only 4 images - take the most relevance images", () => {
      const service: ValidateManagerService = TestBed.get(ValidateManagerService);
      const product = {
        images: [{ id: 477593, path: 'img1', direction: 'Front' }],
        designChanges: [
          {
            id: 2602,
            name: "4008429105036",
            startDate: "2021-01-09T00:00:00.000Z",
            endDate: null,
            displayInSmartValidationPallet: true,
            images: [
              {
                id: 555944,
                path: "img2",
                direction: "Front"
              },
              {
                id: 555944,
                path: "img3",
                direction: "Front"
              }
            ]
          },
          {
            id: 2602,
            name: "4008429105036",
            startDate: "2019-01-09T00:00:00.000Z",
            endDate: null,
            displayInSmartValidationPallet: true,
            images: [
              {
                id: 555944,
                path: "img3",
                direction: "Front"
              },
              {
                id: 555944,
                path: "img4",
                direction: "Front"
              }
            ]
          },
          {
            id: 2602,
            name: "4008429105036",
            startDate: "2022-01-09T00:00:00.000Z",
            endDate: null,
            displayInSmartValidationPallet: true,
            images: [
              {
                id: 555944,
                path: "img5",
                direction: "Front"
              },
              {
                id: 555944,
                path: "img6",
                direction: "Front"
              },
              {
                id: 555944,
                path: "img7",
                direction: "Front"
              }
            ]
          },
        ]
      };
      const images = service.getProductImages(product);
      expect(images).toEqual([
        {
          id: 477593,
          path: "img1",
          direction: "Front"
        },
        {
          id: 555944,
          path: "img5",
          direction: "Front"
        },
        {
          id: 555944,
          path: "img6",
          direction: "Front"
        },
        {
          id: 555944,
          path: "img7",
          direction: "Front"
        }
      ]);
    });
  });
});

