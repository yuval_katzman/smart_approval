import { promisify } from 'util';
const nc = require('@trax-retail/node-core')('smart-approval');

export class StsService {
    static async get<T>(projectName: string, url: string): Promise<T> {
        try {
            const reqSettings = {
                method: 'GET',
                url: url,
                headers: { 'Content-Type': 'application/json' },
                json: true
            };
            const res = await promisify(nc.utils.serverToServerHttpRequest)(nc.config.server_to_server_app, projectName, reqSettings);
            return JSON.parse(res.httpRequest.body);
        } catch (err) {
            nc.logger.error({ err }, 'Error getting data from ' + url);
            throw err;
        }
    }
}
