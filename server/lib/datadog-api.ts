const nc = require('@trax-retail/node-core')('smart-approval');
const trax_node_vault = require('@trax-retail/node-core-vault');
const dogApi  = require('dogapi');
import {promisify} from "util";

export async function initialize() {
    const vault = trax_node_vault({
        env: ['EDGE', 'PROD', 'GCP_EDGE', 'GCP_PROD'].includes(process.env.TRAX_ENV as string) ? 'PROD' : 'INT',
        app: 'smart-approval',
        cloud: nc.cloud.config.type
    });

    const api_key = (await vault.getSecret('datadog_iot')).additional_fields.api_key;
    dogApi.initialize({api_key});
}

export function heartbeat(check: string, hostName: string) {
    const tags=[`cloud:${nc.config.cloud.type}`,`env:${nc.config.cloud.env}`];

    setInterval(() =>{
        promisify(dogApi.serviceCheck.check)(check, hostName, dogApi.OK, {tags}).catch((err: any) => {
            nc.logger.error({
                err
            }, `Error sending check to datadog - ${check}:${hostName}`);
        })
    }, 30000);
}

export async function create(metric:string, value:number, tags: string[]) {
    dogApi.metric.send(metric, value, {host:"smart-approval", tags:tags});
}
