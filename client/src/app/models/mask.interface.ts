
export default interface Mask {
  topLeft: {
    x: number,
    y: number
  };
  bottomRight: {
    x: number,
    y: number
  };
  id: number;
  type: string;
  annotationText: string;
  annotationImageUrl: string;
  annotationNewImageFile?: File;
}
