import {find, forEach} from "lodash";
import {Morpheus} from "./morpheus";
import {mat4} from "gl-matrix";

export class Atom {
    transformation: {};
    location: {
        left: number,
        top: number
    };
    probe: {
        tileId: {
            x: number,
            y: number
        },
        probeOriginalWidth: number,
        probeOriginalHeight: number,
        clipImagedPath: string,
        availableLevels?: {
            s3ImagePath: string
        }[],
        probeId?: number,
        probeImageStoreUrl?: string,
        localImageTime?: Date,
        scaleTransform?: mat4,
        rectifications?: any[]
    };
    originalTransformation: mat4;
    isRectificationsAvailable: boolean;
    rendering: {
        tags: {
            canvasId: number
        }[];
        activeBuffer: {
            image: {
                onload: () => void,
                src: string;
            },
            level: number
        }
        backBuffer: {
            image: {
                onload: () => void,
                src: string;
            },
            level: number
        }
        completed: boolean,
        swapBuffers: () => void;
    };

    constructor(atomData, compoundData, isTiled) {
        if (isTiled) {
            this.TileAtom(atomData, compoundData);
        } else {
            this.ProbeAtom(atomData, compoundData);
        }
    }

    private TileAtom(atomData, compoundData) {
        this.location = atomData.location;
        this.probe = this.getTile(atomData.tileId, compoundData);
    }

    private ProbeAtom(atomData, compoundData) {
        this.probe = this.getProbe(atomData.probe_pk, compoundData);
        this.transformation = Morpheus.toClient(atomData.transform);
        this.originalTransformation = Morpheus.toClient(atomData.transform);
        this.isRectificationsAvailable = this.probe.rectifications && this.probe.rectifications.length > 0;
    }

    private getProbe(probePk, compoundData) {
        const probeData = this.findProbeOrTileDataInCompound(probePk, compoundData);
        const imageLevelsArray = this.createImageLevelsArrayForProbe(probeData);
        const probeRelevantData = {
            probeId: probeData.probe_pk,
            tileId: probeData.tileId,
            probeOriginalWidth: probeData.original_width,
            probeOriginalHeight: probeData.original_height,
            clipImagedPath: probeData.clipPath,
            probeImageStoreUrl: probeData.availableUrls.imageStoreMedium,
            availableLevels: imageLevelsArray,
            localImageTime: new Date(probeData.local_image_time),
            scaleTransform: Morpheus.toClient(probeData.scale_transform),
            rectifications: []
        };

        forEach(probeData.transforms, rectificationData => {
            probeRelevantData.rectifications.push(Morpheus.toClient(rectificationData.transform));
        });

        if (probeRelevantData.rectifications &&
            probeRelevantData.rectifications.length === 1 && Morpheus.isUnit(probeRelevantData.rectifications[0])) {
            probeRelevantData.rectifications = null;
        }

        return probeRelevantData;
    }

    private createImageLevelsArrayForProbe(imageData) {
        const imageLevelOptions = [];
        const imageLevelOptionMedium = {
            s3ImagePath: imageData.availableUrls.s3medium,
            imageStorePath: imageData.availableUrls.imageStoreMedium,
            width: 300,
            height: 400,
            scale: 300 / imageData.original_width
        };
        const imageLevelOptionOriginal = {
            s3ImagePath: imageData.availableUrls.s3original,
            imageStorePath: imageData.availableUrls.imageStoreOriginal,
            width: imageData.original_width,
            height: imageData.original_height,
            scale: 1
        };

        imageLevelOptions.push(imageLevelOptionMedium);
        imageLevelOptions.push(imageLevelOptionOriginal);

        return imageLevelOptions;
    }

    private getTile(tileId, compoundData) {
        const tileData = this.findProbeOrTileDataInCompound(tileId, compoundData);
        const tileRelevantData = {
            tileId: tileData.tileId,
            probeOriginalWidth: tileData.original_width,
            probeOriginalHeight: tileData.original_height,
            clipImagedPath: tileData.clipPath,
            availableLevels: this.createImageLevelsArray(tileData)
        };

        return tileRelevantData;
    }

    private createImageLevelsArray(imageData) {
        const imageLevelOptions = [];

        for (let levelIndex = 0; levelIndex < imageData.numOfLevels; levelIndex++) {
            const imagePath = imageData.tileImagePartialPath + '_' + levelIndex + '.jpg';

            const height = imageData.original_height / Math.pow(2, levelIndex);
            const width = imageData.original_width / Math.pow(2, levelIndex);

            imageLevelOptions.push({
                s3ImagePath: imagePath,
                imageStorePath: imagePath,
                width,
                height,
                scale: width / imageData.original_width
            });
        }

        return imageLevelOptions;
    }

    private findProbeOrTileDataInCompound(probeOrTileId, compoundData) {
        // Will get the probe by the tile id if it has one. If not, will find according to probe id
        const probeData = find(compoundData.probes, probe => {
            if (probe.tileId && probeOrTileId && (probeOrTileId.x || probeOrTileId.x === 0) && (probeOrTileId.y || probeOrTileId.y === 0)) {
                return probe.tileId.x === probeOrTileId.x && probe.tileId.y === probeOrTileId.y;
            }
            return probe.probe_pk === probeOrTileId;
        });

        return probeData;
    }
}



