import {fakeAsync, inject, TestBed, tick} from '@angular/core/testing';

import {API} from './api.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {of, throwError} from 'rxjs';
import {HttpClient} from '@angular/common/http';

describe('APIService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule]
        });
        window.localStorage.clear();
    });

    it('should be created', () => {
        const service: API = TestBed.get(API);
        expect(service).toBeTruthy();
    });

    describe('action without projectName', () => {
        it('should get', inject([HttpTestingController], (httpMock: HttpTestingController) => {
            const service: API = TestBed.get(API);

            service.get('/api/projects').subscribe();

            const req = httpMock.expectOne('/api/projects');
            expect(req.request.body).toBeNull();
            expect(req.request.method).toEqual('GET');
            httpMock.verify();
        }));

        it('should put', inject([HttpTestingController], (httpMock: HttpTestingController) => {
            const service: API = TestBed.get(API);
            const testItem = {
                pet: 'dog'
            };

            service.put('/api/projects', testItem).subscribe();

            const req = httpMock.expectOne('/api/projects');
            expect(req.request.body).toEqual(testItem);
            expect(req.request.method).toEqual('PUT');
            httpMock.verify();
        }));

        it('should post', inject([HttpTestingController], (httpMock: HttpTestingController) => {
            const service: API = TestBed.get(API);
            const testItem = {
                pet: 'dog'
            };

            service.post('/api/projects', testItem).subscribe();

            const req = httpMock.expectOne('/api/projects');
            expect(req.request.body).toEqual(testItem);
            expect(req.request.method).toEqual('POST');
            httpMock.verify();
        }));
    });

    describe('request respond', () => {
        it('should pass respond to the observer', inject([HttpTestingController], (httpMock: HttpTestingController) => {
            const service: API = TestBed.get(API);
            const client: HttpClient = TestBed.get(HttpClient);
            let actualResult = '';
            const wantedResult = 'test value';
            const httpRequestSpy = spyOn<any>(client, 'request').and
                .returnValue(of(wantedResult));
            window.localStorage.setItem('projectsClouds', JSON.stringify({
                test: 'TEST'
            }));

            service.get('/api/:projectName/bla', {
                pathParams: {
                    projectName: 'test'
                }
            }).subscribe((result) => {
                actualResult = result;
            });

            expect(actualResult).toEqual(wantedResult);
        }));
    });

    it('get with projectName - project cloud in localStorage', inject([HttpTestingController], (httpMock: HttpTestingController) => {
        const service: API = TestBed.get(API);

        window.localStorage.setItem('projectsClouds', JSON.stringify({
            test: 'TEST'
        }));

        service.get('/api/:projectName/bla', {
            pathParams: {
                projectName: 'test'
            }
        }).subscribe();

        const req = httpMock.expectOne('http://sheker/api/test/bla');
        expect(req.request.body).toBeNull();
        httpMock.verify();
    }));

    it('get with projectName - project cloud not in localStorage', inject([HttpTestingController], fakeAsync((httpMock: HttpTestingController) => {
        const service: API = TestBed.get(API);

        service.get('/api/:projectName/bla', {
            pathParams: {
                projectName: 'test'
            }
        }).subscribe();

        // Cloud 1
        httpMock.expectOne('/api/projects').flush([
            {
                project_name: 'test',
                cloudProvider: 'TEST'
            }
        ]);

        // Cloud 2
        httpMock.expectOne('http://sheker/api/projects').flush([
            {
                project_name: 'test2',
                cloudProvider: 'DEV'
            }
        ]);

        tick();

        const req = httpMock.expectOne('http://sheker/api/test/bla');
        expect(req.request.body).toBeNull();
        httpMock.verify();
    })));
});
