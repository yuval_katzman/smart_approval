#!/usr/bin/env bash

set -e

DOCKEREXEC="docker exec -i"
CONTAINER="$1"
shift
CMD="$@"

case $CONTAINER in
    "${TRAX_ENV}_proj_sql" | "${TRAX_ENV}_usr_mgmt_sql")
        GREP="port: 3306  MySQL Community Server (GPL)"
        ;;
    "${TRAX_ENV}_mongo")
        GREP="waiting for connections on port 27017"
        ;;
    "${TRAX_ENV}_pricing_mongo")
        GREP="waiting for connections on port 27017"
        ;;
esac

echo "Starting $CONTAINER"

COUNTER=0

until [[ ("`docker logs $CONTAINER 2>&1 | grep \"${GREP}\"`") || ${COUNTER} -ge 360 ]]; do
    sleep 2;
    let COUNTER+=2
    echo "Waiting for $CONTAINER to initialize... ($COUNTER seconds so far)"
done;

if [[ ${COUNTER} -ge 360 ]]
then
echo "$CONTAINER is not ready to use after 2 min" >&2
exit 1
fi

$DOCKEREXEC $CONTAINER $CMD