#!/bin/sh

MYSQLCMD="mysql -u root"
SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

case $TRAX_ENV in
    "DEV")
        MONGODB="SmartDev"
        PRICING_MONGODB="PricingEngineDev"
        ENV="dev"
        SUBNET="10.4.4.0/24"
        REDIS=10.4.4.4
        ;;
    "TEST")
        MONGODB="SmartTest"
        PRICING_MONGODB="PricingEngineTest"
        ENV="test"
        SUBNET="10.3.3.0/24"
        REDIS=10.3.3.4
        ;;
esac

COMPOSE_FILES=${SCRIPTPATH}/docker-compose.${ENV}.yml

set -e
$(aws ecr get-login --no-include-email --region us-east-1)
${SCRIPTPATH}/env-down.sh
docker network create --subnet=${SUBNET} trax_${ENV} 2>&1 || echo "trax_${ENV} network already exists"
docker-compose -f ${SCRIPTPATH}/docker-compose.yml pull || {
    echo "Please manually run the aws login command: \$(aws ecr get-login --no-include-email --region us-east-1)"
} && docker-compose -f ${SCRIPTPATH}/docker-compose.yml -f $COMPOSE_FILES -p ${ENV} up -d


#####
# Wait for containers to start and create users
#####
${SCRIPTPATH}/wait-for.sh ${TRAX_ENV}_usr_mgmt_sql "$MYSQLCMD" < ${SCRIPTPATH}/user-creation/createUserManagementMysqlUser.sql
${SCRIPTPATH}/wait-for.sh ${TRAX_ENV}_mongo "mongo $MONGODB" < ${SCRIPTPATH}/user-creation/mongo-user.js
${SCRIPTPATH}/wait-for.sh ${TRAX_ENV}_proj_sql "$MYSQLCMD" < ${SCRIPTPATH}/user-creation/sql-proj-user.sql

####
# Insert data
####
node ${SCRIPTPATH}/data/import-db-data.js
