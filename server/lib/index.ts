import * as path from 'path';
const nc = require('@trax-retail/node-core')('smart-approval');
import {SmartAuthService} from '@trax-retail/smart-auth';
import { get} from "lodash";
import {initialize, heartbeat} from "./datadog-api";

import { mergeWith } from 'lodash';
const env = process.env.TRAX_ENV || 'DEV';
const config = mergeWith(require(`../config/base.json`), require(`../config/${env}.json`));

export async function init (){
    await SmartAuthService.init({mongoConfig: config.mongo.users, corsAllowedOrigins: config.corsAllowedOrigins});
    nc.init(path.resolve('./'), process.env.TRAX_ENV || 'DEV');
    if (process.env.SEND_HEARTBEAT){
        const hostname = get(nc.config, "serviceCheck.hostName");
        let checkName = get(nc.config, "serviceCheck.serviceCheckName");
        if(hostname && checkName) {
            initialize();
            heartbeat(checkName, hostname);
        }
    }
}
