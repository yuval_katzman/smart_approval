import {Component, OnInit} from '@angular/core';
import {CancelOnRefreshCancellerService} from "./services/cancelOnRefresh/cancel-on-refresh-canceller-service";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    constructor(private cancelOnRefresh: CancelOnRefreshCancellerService) {
    }

    ngOnInit(): void {
        this.cancelOnRefresh.cancelTasks();
    }
}
