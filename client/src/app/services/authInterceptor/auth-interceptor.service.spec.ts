import { TestBed } from '@angular/core/testing';

import { AuthInterceptorService } from './auth-interceptor.service';
import {CookieService} from "ngx-cookie-service";

describe('AuthInterceptorService', () => {
    beforeEach(() => TestBed.configureTestingModule({
        providers: [CookieService]
    }));

    it('should be created', () => {
        const service: AuthInterceptorService = TestBed.get(AuthInterceptorService);
        expect(service).toBeTruthy();
    });
});
