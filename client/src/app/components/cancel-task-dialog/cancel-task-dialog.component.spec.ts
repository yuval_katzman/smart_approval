import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelTaskDialogComponent } from './cancel-task-dialog.component';
import {FormsModule} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {HttpClient} from "@angular/common/http";

describe('CancelTaskDialogComponent', () => {
  let component: CancelTaskDialogComponent;
  let fixture: ComponentFixture<CancelTaskDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule ],
      providers: [{
        provide: MatDialogRef,
        useValue: {close: () => {}},
      },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {}
        }],
      declarations: [ CancelTaskDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelTaskDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should cancel', () => {
    component.cancel();
  });

  it('should doNotCancel', () => {
    component.doNotCancel();
  });
});
