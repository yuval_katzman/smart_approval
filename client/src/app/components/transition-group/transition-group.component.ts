import {AfterContentInit, Component, ContentChildren, Directive, ElementRef, Input, QueryList} from '@angular/core';


/**
 * taken from https://next.plnkr.co/edit/i2hRcrTHLxTKA7mk?preview
 */

@Directive({
    selector: '[appTransitionGroupItem]'
})
export class TransitionGroupItemDirective {
    prevPos: any;

    newPos: any;

    el: HTMLElement;

    moved: boolean;

    moveCallback: any;

    constructor(elRef: ElementRef) {
        this.el = elRef.nativeElement;
    }
}


@Component({
    selector: '[transition-group]',
    templateUrl: './transition-group.component.html'
})
export class TransitionGroupComponent implements AfterContentInit {
    @Input('transition-group') class;

    @ContentChildren(TransitionGroupItemDirective) items: QueryList<TransitionGroupItemDirective>;

    ngAfterContentInit() {
        this.refreshPosition('prevPos');
        this.items.changes.subscribe(items => {
            items.forEach(item => {
                item.prevPos = item.newPos || item.prevPos;
            });

            items.forEach(this.runCallback);
            this.refreshPosition('newPos');
            items.forEach(this.applyTranslation);

            // force reflow to put everything in position
            const offSet = document.body.offsetHeight;
            this.items.forEach(this.runTransition.bind(this));
        });
    }

    runCallback(item: TransitionGroupItemDirective) {
        if (item.moveCallback) {
            item.moveCallback();
        }
    }

    runTransition(item: TransitionGroupItemDirective) {
        if (!item.moved) {
            return;
        }
        const cssClass = this.class + '-move';
        const el = item.el;
        const style: any = el.style;
        el.classList.add(cssClass);
        style.transform = style.WebkitTransform = style.transitionDuration = '';
        el.addEventListener('transitionend', item.moveCallback = (e: any) => {
            if (!e || /transform$/.test(e.propertyName)) {
                el.removeEventListener('transitionend', item.moveCallback);
                item.moveCallback = null;
                el.classList.remove(cssClass);
            }
        });
    }

    refreshPosition(prop: string) {
        this.items.forEach(item => {
            item[prop] = item.el.getBoundingClientRect();
        });
    }

    applyTranslation(item: TransitionGroupItemDirective) {
        item.moved = false;
        const dx = item.prevPos.left - item.newPos.left;
        const dy = item.prevPos.top - item.newPos.top;
        if (dx || dy) {
            item.moved = true;
            const style: any = item.el.style;
            style.transform = style.WebkitTransform = 'translate(' + dx + 'px,' + dy + 'px)';
            style.transitionDuration = '0s';
        }
    }
}
