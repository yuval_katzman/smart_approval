import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { InactivityTimerComponent } from './inactivity-timer.component';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
class MatDialogMock {
  static open() {

  }
  static close() {}
}
describe('InactivityTimerComponent', () => {
  let component: InactivityTimerComponent;
  let fixture: ComponentFixture<InactivityTimerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InactivityTimerComponent ],
      imports: [],
      providers: [{
        provide: MatDialogRef,
        useValue: MatDialogMock
      },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {}
        }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InactivityTimerComponent);
    component = fixture.componentInstance;
    // @ts-ignore
    component.modalDuration = 1;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should close dialog after timeout ', (done) => {
    const spy = spyOn(MatDialogMock, 'close');
    setTimeout(() => {
      expect(spy).toHaveBeenCalled();
      done();
    }, 2000);
  });

  it('should close dialog after continue ', fakeAsync(() => {
    const spy = spyOn(MatDialogMock, 'close');
    tick(100);
    component.continue();
    expect(spy).toHaveBeenCalled();
  }));
});
