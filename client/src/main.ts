import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import {environmentLoader} from "./environments/environmentLoader";

environmentLoader.then( env => {
  if (env.production) {
    enableProdMode();
  }

  environment.config = env.config;
  environment.production = env.production;

  platformBrowserDynamic().bootstrapModule(AppModule)
      .catch(err => console.log(err));
});
