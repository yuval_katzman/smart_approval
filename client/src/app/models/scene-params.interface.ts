export interface SceneParams {
  numOfTilesX: number;
  numOfTilesY: number;
  sceneHeight: number;
  sceneWidth: number;
  tileHeight: number;
  tileWidth: number;
}
