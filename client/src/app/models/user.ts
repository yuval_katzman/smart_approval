
export class User {
    address: string;
    birthDate: string;
    center: string;
    country: string;
    createDate: string;
    currentCenter: string;
    deleteDate: string;
    deleted: boolean;
    email: string;
    firstName: string;
    gender: string;
    isActive: boolean;
    isStaff: boolean;
    isSuperuser: boolean;
    joinDate: string;
    lastName: string;
    lastLogin: string;
    mobile: string;
    modules: string[];
    parentsPhone: string;
    projects: {
        [projectName: string]: {
            validate: number;
        }
    };
    roles: string[];
    staffId: string;
    stitching: number;
    teamMembers: string[];
    constructor({address, birthDate, center, country, createDate, currentCenter, deleteDate, deleted, email, firstName,
                gender, isActive, isStaff, isSuperuser, joinDate, lastName, lastLogin, mobile, modules, parentsPhone,
                projects, roles, staffId, stitching, teamMembers}) {
        this.address = address;
        this.birthDate = birthDate;
        this.center = center;
        this.country = country;
        this.createDate = createDate;
        this.currentCenter = currentCenter;
        this.deleteDate = deleteDate;
        this.deleted = deleted;
        this.email = email;
        this.firstName = firstName;
        this.gender = gender;
        this.isActive = isActive;
        this.isStaff = isStaff;
        this.isSuperuser = isSuperuser;
        this.joinDate = joinDate;
        this.lastName = lastName;
        this.lastLogin = lastLogin;
        this.mobile = mobile;
        this.modules = modules;
        this.parentsPhone = parentsPhone;
        this.projects = projects;
        this.roles = roles;
        this.staffId = staffId;
        this.stitching = stitching;
        this.teamMembers = teamMembers;
    }
}
