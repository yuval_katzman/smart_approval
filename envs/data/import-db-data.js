
const env = process.env.TRAX_ENV || "DEV"

const yaml = require('yaml'),
    path = require('path'),
    glob = require('glob'),
    {promisify} = require('util'),
    fs = require('fs'),
    docker = require('docker-composer-manager'),
    exec = require('child_process').exec,
    yamlFilePath = yaml.parse(path.resolve(__dirname, `../docker-compose.${env.toLowerCase()}.yml`)),
    yamlFile = fs.readFileSync(yamlFilePath, 'utf-8'),
    parsedYaml = yaml.parse(yamlFile),
    dbName = parsedYaml.services.mongo.environment.MONGODB_DATABASE,
    mongoUrl = parsedYaml.services.mongo.networks[env.toLowerCase()].ipv4_address;


async function importMongo() {
    const filePaths = glob.sync(`${__dirname}/mongo/*.json`);
    for(const filePath of filePaths) {
        await promisify(exec)(`mongoimport -h ${mongoUrl} --db ${dbName} --drop --file ${filePath}`)
    }
    console.log('finished importing mongo');
}

async function importSelector() {
    const filePaths = glob.sync(`${__dirname}/selector/*.sql`);
    for(const filePath of filePaths) {
        await promisify(exec)(`docker exec -i ${env}_selector_sql mysql -u root < ${path.resolve(filePath)}`).catch((err) => {
            console.log(err);
        });
    }
    console.log('finished importing selector');
}

async function importProject() {
    const filePaths = glob.sync(`${__dirname}/project/*.sql`);
    for(const filePath of filePaths) {
        await promisify(exec)(`docker exec -i ${env}_proj_sql mysql -u root < ${path.resolve(filePath)}`).catch((err) => {
            console.log(err);
        });
    }
    console.log('finished importing project');
}

async function importUserMgmt() {
    const filePaths = glob.sync(`${__dirname}/userMgmt/*.sql`);
    for(const filePath of filePaths) {
        await promisify(exec)(`docker exec -i ${env}_usr_mgmt_sql mysql -u root < ${path.resolve(filePath)}`).catch((err) => {
            console.log(err);
        });
    }
    console.log('finished importing user management');
}

async function restartSmart() {
    await promisify(exec)(`docker restart ${env}_smart`).catch((err) => {
        console.log(err);
    });
    console.log('finished restarting smart');
}

async function importAll() {
    await importMongo();
    await importSelector();
    await importProject();
    await importUserMgmt();
    await restartSmart();
    console.log('finished importing all data');
}

return importAll();
