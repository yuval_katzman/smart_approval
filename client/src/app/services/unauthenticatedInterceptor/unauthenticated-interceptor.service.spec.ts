import { TestBed } from '@angular/core/testing';

import { UnauthenticatedInterceptorService } from './unauthenticated-interceptor.service';
import {CookieService} from "ngx-cookie-service";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {HTTP_INTERCEPTORS, HttpClient} from "@angular/common/http";
import {UserService} from "../user/user.service";

describe('UnauthenticatedInterceptorService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
        CookieService,
      {
        provide: HTTP_INTERCEPTORS,
        useClass: UnauthenticatedInterceptorService,
        multi: true
      }
    ],
    imports: [HttpClientTestingModule]
  }));

  it('should be created', () => {
    const service: UnauthenticatedInterceptorService = TestBed.get(UnauthenticatedInterceptorService);
    expect(service).toBeTruthy();
  });

  it('response is fine, should return result', () => {
    const httpMock = TestBed.get(HttpTestingController);
    const httpClient = TestBed.get(HttpClient);

    httpClient.get('http://sheker').subscribe((res) => {
      expect(res).toEqual({
        great: 'success'
      });
    });

    httpMock.expectOne('http://sheker').flush({
      great: 'success'
    });
  });

  it('response is unauthenticated, should logout', () => {
    const httpMock = TestBed.get(HttpTestingController);
    const httpClient = TestBed.get(HttpClient);
    const userServiceSpy = TestBed.get(UserService);

    const logoutSpy = spyOn(userServiceSpy, 'logout').and.returnValue(true);

    httpClient.get('http://sheker').subscribe((res) => {
    }, (err) => {
      expect(err.error).toEqual({
        great: 'failure'
      });
    });

    httpMock.expectOne('http://sheker').flush({
      great: 'failure'
    }, {
      status: 401,
      statusText: 'unauthenticated'
    });

    expect(logoutSpy).toHaveBeenCalled();
  });
});
