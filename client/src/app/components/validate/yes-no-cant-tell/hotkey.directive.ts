import {AfterViewInit, OnDestroy, Directive, Input} from '@angular/core';
import {MatRadioButton} from "@angular/material/radio";
import {fromEvent, Subscription} from "rxjs";

@Directive({
    selector: '[appHotkey]'
})
export class HotkeyDirective implements AfterViewInit, OnDestroy {
    @Input() content: string;
    subscription: Subscription;
    constructor(private element: MatRadioButton) {}
    ngAfterViewInit(): void {
        const contentElement = this.element._inputElement.nativeElement.parentNode.parentNode.querySelector("span");
        contentElement.innerHTML = `<u>${this.content[0]}</u>${this.content.slice(1)}`;
        contentElement.style.display = "inline";
        this.subscription = fromEvent(document, 'keypress').subscribe((e: KeyboardEvent) => {
            if (e.key === this.content[0].toLowerCase()) {
                this.element._inputElement.nativeElement.dispatchEvent(new MouseEvent('click', {bubbles: true}));
            }
        });
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }
}
