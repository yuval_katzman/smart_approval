import {SmartUser} from "../defs";
import {EntityType} from "../interfaces";
import {getTemplateGroupIds} from "./dal";

const nc = require('@trax-retail/node-core')('smart-approval');
const  config = nc.config;
const  request = require('request');
const  lodash = require('lodash');
const  util = require('util');
const  baseSelectorUrl = config.selectorUrl;

interface SelectorGetNextResponse {
    additionalInfo: {
        [key: string]: any
    };
    objectId: number;
    objectType: string;
    projectName: string;
    taskType: string;
    uuid: string;
    waveType: string;
    waveUid: string;
    queueDurationInSeconds: number;
}

interface GetNextResponse {
    sceneId: number,
    entity_id: number,
    entity_type: EntityType,
    scene_id: number,
    project: string,
    projectName: string,
    project_name: string,
    taskType: string,
    selectorTaskId: string,
    waveType: string,
    waveUid: string,
    wave_type: string,
    wave_uid: string,
    current_queue_duration: number,
    voting_process_fk: string,
    [additionalInfo: string]: any,
    questions: {
        "location": {
            "scene": {
                "masks": [
                    {
                        "h": number,
                        "w": number,
                        "x": number,
                        "y": number
                    }
                ],
                "productId":number
            },
            "probes": [
                {
                    "pk": number,
                    "masks": [
                        {
                            "h": number,
                            "w": number,
                            "x": number,
                            "y": number
                        }
                    ]
                }
            ]
        },
        "product_fk": number,
        "dvc_fk": number,
        "question_uid": string,
        "question_type": string,
        "question_category": string
    }[]
}

export async function getNext(user: SmartUser, taskType: string, projectName?: string, templateId?: number, templateIds?: number[], templateGroup?: string, subQueue?: string, traxCategoryUuid?:string): Promise<GetNextResponse | void> {
    let bodyParams: {
        [bodyParam: string]: any
    } = {
        qatName: user.staff_id || user.email,
        taskType,
        projectName,
        templateId,
        templateIds,
        templateGroup,
        subQueue,
        traxCategoryUuid
    };

    // removing falsy values
    bodyParams = lodash.omitBy(bodyParams, (t: any) => {
        return !t;
    });

    let task,
        error;

    const res = await util.promisify(request.post)({uri: `${baseSelectorUrl}/next-task`, body: bodyParams, json: true});
    if(res.statusCode !== 200) {
        error = new Error(`${res.statusMessage}: ${res.body}`);
        throw error;
    }

    if (!error && res && res.body) {
        task = createTaskObject(res.body);
    }

    return task;
}

async function handleTemplateGroupQueue(projectName: string, templateGroup: string, qCount: any) {
    let sumQueuesCount = qCount.templateGroups ? qCount.templateGroups[templateGroup] || 0 : 0;

    if (qCount.templates) {
        let templateGroupIds = await getTemplateGroupIds(projectName, templateGroup);
        templateGroupIds?.forEach(templateIdInGroup => {
            sumQueuesCount += qCount.templates[templateIdInGroup] || 0;
        });
    }

    return sumQueuesCount;
}

export async function count(user: SmartUser, taskType: string, projectName: string, templateId?:number, templateGroup?:string, subQueue?: string, traxCategoryUuid?:string): Promise<number>{
    const queryParams = {
        qatName: user.staff_id || user.email,
        taskType,
        projectName
    };

    const res = await util.promisify(request.get)({uri: `${baseSelectorUrl}/count`, qs: queryParams, json: true});
    const error = res.statusCode !== 200 && res.statusMessage;

    if(error) {
        throw error;
    }
    const result = !subQueue ? res.body : ((res.body.subQueues && res.body.subQueues[subQueue]) || {count : 0})
    if (templateId) return result.templates ? result.templates[templateId] || 0 : 0;

    if (traxCategoryUuid) return result.traxCategories ? result.traxCategories[traxCategoryUuid] || 0 : 0;

    if (templateGroup) {
        return await handleTemplateGroupQueue(projectName, templateGroup, result);
    }

    return result.count;
}

export async function extendLock(user: SmartUser, taskId: string, durationMS: number, projectName:string) {
    const queryParams = {
        qatName: user.staff_id || user.email,
        uuid: taskId,
        duration: durationMS / 1000
    };

    const res = await util.promisify(request.put)({uri: `${baseSelectorUrl}/extend-lock`, qs: queryParams, json: true});
    const error = res.statusCode !== 200 && res.statusMessage;

    if (error) {
        throw error;
    }

    return res.body;
}

export async function cancelTask(user: SmartUser, taskId: string,projectName: string, shouldNotBeRedone = false) {
    const queryParams = {
        qatName: user.staff_id || user.email,
        uuid: taskId,
        shouldNotBeRedone
    };

    const res = await util.promisify(request.put)({uri: `${baseSelectorUrl}/cancel`, qs: queryParams, json: true});
    const error = res.statusCode !== 200 && res.statusMessage;

    if(error) {
        throw error;
    }

    return res.body;
}

export async function finishTask(user: SmartUser, taskId: string, projectName: string) {
    const queryParams = {
        qatName: user.staff_id || user.email,
        uuid: taskId
    };

    const res = await util.promisify(request.put)({uri: `${baseSelectorUrl}/finish`, qs: queryParams, json: true});
    const error = res.statusCode !== 200 && res.statusMessage;

    if(error) {
        throw error;
    }

    return res.body;
}

function createTaskObject(response: SelectorGetNextResponse) {
    const votingProcessId = (response.additionalInfo && response.additionalInfo.votingProcessId) ||
        (response.additionalInfo && response.additionalInfo.params && response.additionalInfo.params.voting_process_fk);

    const obj = {
        [`${response.objectType}Id`]: response.objectId,
        [`${response.objectType}_id`]: response.objectId,
        project: response.projectName,
        projectName: response.projectName,
        project_name: response.projectName,
        taskType: response.taskType,
        selectorTaskId: response.uuid,
        waveType: response.waveType,
        waveUid: response.waveUid,
        wave_type: response.waveType,
        wave_uid: response.waveUid,
        queueDurationInSeconds: response.queueDurationInSeconds,
        voting_process_fk: votingProcessId,
        selectorTask: response
    };

    return lodash.mergeWith(obj, response.additionalInfo);
}
