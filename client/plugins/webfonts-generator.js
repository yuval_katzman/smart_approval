'use strict';

const glob = require('glob');
const webfontsGenerator = require('webfonts-generator');

glob('./src/trax-icons/*.svg', (err, files) => {
  if (err) {
    return;
  }

  webfontsGenerator({
    files: files,
    dest: 'src/assets/trax-icons',
    fontName: 'trax-icons',
    templateOptions: {
      classPrefix: 'trax-icons-',
      baseSelector: '.trax-icons'
    }
  }, function (err, res) {

  });
});
