import { Injectable } from '@angular/core';
import { Project } from "../../models/project";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {IndexedDbService} from "../indexedDb/indexed-db-service.service";
import {API} from "../API/api.service";

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  private projects: {
      [projectName: string]: Project
  };
  private getProjectFromServerPromise: Promise<Project | undefined>;

  constructor(private http: HttpClient, private api: API, private indexedDbService: IndexedDbService) {
      this.projects = {};
  }

  getProjectByName(projectName: string): Observable<Project> {
    return new Observable((observer) => {

      const projectFromMemory = this.getProjectFromMemory(projectName);
      if (projectFromMemory) {
        observer.next(projectFromMemory);
        observer.complete();
        return;
      }

      this.getProjectFromIndexedDb(projectName).subscribe((projectFromIndexedDb) => {
        if (projectFromIndexedDb) {
          observer.next(projectFromIndexedDb);
          observer.complete();
          return;
        }
        this.getProjectFromServerAndSaveToIndexedDb(projectName).then((projectFromServer) => {
          observer.next(projectFromServer);
          observer.complete();
        });
      });
    });
  }

  private getProjectFromMemory(projectName): Project | undefined {
    return this.projects[projectName];
  }

  private getProjectFromIndexedDb(projectName: string): Observable<Project | undefined> {
    return new Observable((observer) => {
      this.indexedDbService.checkIfDBExists()
          .then((dbExists) => {
            if (!dbExists) {
              observer.next();
              observer.complete();
              return;
            }
            this.indexedDbService.read('projects', projectName).then((rawProject) => {
                if (!rawProject) {
                    observer.next();
                    observer.complete();
                    return;
                }
                this.projects[projectName] = new Project(rawProject);
                observer.next(this.projects[projectName]);
                observer.complete();
            });
          });
    });
  }

  private getProjectFromServerAndSaveToIndexedDb(projectName: string): Promise<Project | undefined> {
      if (this.getProjectFromServerPromise) {
          return this.getProjectFromServerPromise;
      }

      this.getProjectFromServerPromise = new Promise((resolve, reject) => {
          this.api.get('/api/projects/:projectName', {pathParams: {projectName}})
              .subscribe((rawProject) => {
                  this.getProjectFromServerPromise = null;
                  this.projects[projectName] = new Project(rawProject);
                  this.indexedDbService.checkIfDBExists()
                      .then((dbExists) => {
                          if (!dbExists) {
                              return this.indexedDbService.createDB(['projects']);
                          }
                      }).then(() => {
                          this.indexedDbService.add('projects', projectName, rawProject);
                      });
                  resolve(this.projects[projectName]);
              }, (err) => {
                  reject(err);
          });
      });

      return this.getProjectFromServerPromise;
  }
}
